//
//  main.swift
//  ChainOfResponsibilitySwiftDemo
//
//  Created by Водолазкий В.В. on 16.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

//
// Common ncestor with abstract methods
//
class AbstractBookTopic {
	
	func getTitle()->String {
		return "Abstract Class! Override getTitle()"
	}
	func getTopic()->String {
		return "Abstract Class! Override getTopic()"
	}
	func setTitle(title : String?) {
		
	}
}

class BookTopic : AbstractBookTopic {
	var localTitle : String? = nil;
	var localTopic : String? = nil;
	
	 init( topic : String?) {
		localTopic = topic
	}
	
	override func getTitle() -> String {
		if ((localTitle) != nil) {
			return localTitle!;
		} else {
			return "Title is not defined yet"
		}
	}
	
	override func getTopic() -> String {
		if ((localTopic) != nil) {
			return localTopic!;
		} else {
			return "Topic is not defined yet"
		}
	}
	
	override func setTitle(title: String?) {
		localTitle = title
	}
}

class BookSubTopic : AbstractBookTopic {
	var localTopic : String? = nil
	var localTitle : String? = nil
	var pTopic : BookTopic? = nil
	
	init( topic : String?, parentTopic : BookTopic) {
		localTopic = topic
		pTopic = parentTopic
		localTitle = nil		// just to be sure
	}
	
	override func getTopic() -> String {
		return localTopic!
	}
	
	override func getTitle() -> String {
		if ((localTitle) != nil) {
			return localTitle!
		} else {
			return pTopic!.getTitle()
		}
	}
	
	override func setTitle(title: String?) {
		localTitle = title
	}
	
	func getParentTopic() -> BookTopic {
		return pTopic!
	}
}


	print("BEGIN TESTING CHAIN OF RESPONSIBILITY PATTERN")
	print("")

	let main = BookTopic.init(topic:"PHP 5")
	print("bookTopic before title is set:")
	print("topic: ", main.getTopic())
	print("title: ", main.getTitle())
	print("")

	main.setTitle(title : "PHP 5 Recipes by Babin, Good, Kroman, and Stephens")
	print("bookTopic after title is set: ")
	print("topic: ", main.getTopic())
	print("title: ", main.getTitle())
	print("")

	let st = BookSubTopic.init(topic: "PHP 5 Patterns", parentTopic: main)

	print("bookSubTopic before title is set: ")
	print("topic ", st.getTopic() )
	print("title ", st.getTitle())
	print("")

	st.setTitle(title:"PHP 5 Objects Patterns and Practice by Zandstra")
	print("bookSubTopic after title is set: ")
	print("topic ", st.getTopic())
	print("title ", st.getTitle())
	print("")

	let st2 = BookSubTopic.init(topic: "PHP 5 Patterns for Cats", parentTopic: main)

	print("bookSubSubTopic with no title set: ")
	print("topic: ", st2.getTopic())
	print("title: ", st2.getTitle())
	print("")

	st2.setTitle(title:nil)
	print("bookSubSubTopic with no title for set for bookSubTopic either:")
	print("topic: ", st2.getTopic() )
	print("title: ", st2.getTitle())
	print("")

	print("END TESTING CHAIN OF RESPONSIBILITY PATTERN");
