//
//  main.swift
//  AbstractFactorySwiftDemo
//
//  Created by Водолазкий В.В. on 10.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

import Foundation


class Infantry  {
	func info() -> String {
		return "Infantry"
	}
}

class Archer {
	func info() -> String {
		return "Archer"
	}
}

class Horseman {
	func info() -> String {
		return "Horseman"
	}
}


class RomanInfantry : Infantry {
	
	override func info() -> String {
		return String.init(format: "Roman %@", super.info())
	}

}

class CarthageInfantry : Infantry {
	override func info() -> String {
		return String.init(format: "Carthage %@", super.info())
	}
}

class RomanArcher : Archer {
	override func info() -> String {
		return String.init(format: "Roman %@", super.info())
	}
	
}

class CarthageArcher : Archer {
	override func info() -> String {
		return String.init(format: "Carthage %@", super.info())
	}
}

class RomanHorseman : Horseman {
	override func info() -> String {
		return String.init(format: "Roman %@", super.info())
	}
}

class CarthageHorseman : Horseman {
	override func info() -> String {
		return String.init(format: "Carthage %@", super.info());
	}
}


class ArmyFactory {
	
	func createInfantry() -> Infantry? {
		return nil
	}
	func createArcher() -> Archer? {
		return nil
	}
	func createHorseman() -> Horseman? {
		return Horseman.init()
	}
}

class RomanArmyFactory : ArmyFactory {
	override func createInfantry() -> Infantry? {
		return RomanInfantry.init()
	}
	override func createArcher() -> Archer? {
		return RomanArcher.init()
	}
	override func createHorseman() -> Horseman? {
		return RomanHorseman.init()
	}
}

class CarthageArmyFactory : ArmyFactory {
	override func createInfantry() -> Infantry? {
		return CarthageInfantry.init()
	}
	override func createArcher() -> Archer? {
		return CarthageArcher.init()
	}
	override func createHorseman() -> Horseman? {
		return CarthageHorseman.init()
	}
}


class Army {
	var InfantryArray:	Array <Infantry> = []
	var ArcherArray:	Array <Archer> = []
	var HorsemanArray:	Array <Horseman> = []
	
	func printInfo() {
		for i in InfantryArray {
			print(i.info())
		}
		for a in ArcherArray {
			print(a.info())
		}
		for h in HorsemanArray {
			print(h.info)
		}
	}
}

class Game {
	func createArmyWithFactory(factory : ArmyFactory) -> Army {
		let  a = Army.init()
		a.InfantryArray.append(factory.createInfantry()!)
		a.ArcherArray.append(factory.createArcher()!)
		a.HorsemanArray.append(factory.createHorseman()!)
		return a
	}
}


	let game = Game.init()

	let raf = RomanArmyFactory.init()
	let caf = CarthageArmyFactory.init()

	let ra = game.createArmyWithFactory(factory: raf)
	let ca = game.createArmyWithFactory(factory: caf)

	print("Roman Army:")
	ra.printInfo()

	print("\nCarthage Army:")
	ca.printInfo()


