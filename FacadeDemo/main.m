//
//  main.m
//  FacadeDemo
//
//  Created by Водолазкий В.В. on 04.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MISStates) {
		MISReceived, MISDenyAllKnowledge, MISReferClientToFacilities,
		MISFacilitiesHasNotSentPaperwork, MISElectricianIsNotDone,
		MISElectricianDidItWrong, MISDispatchTechnician, MISSignedOff,
		MISDoesNotWork, MISFixElectriciansWiring, MISComplete
};


@interface MisDepartment : NSObject {
	MISStates state;
}

- (void) submitNetworkRequest;
- (BOOL) checkStatus;

@end

@implementation MisDepartment

- (instancetype) init {
	if (self = [super init]) {
		state = MISReceived;
	}
	return self;
}

- (void) submitNetworkRequest {
	state = MISReceived;
}

- (BOOL) checkStatus {
	state++;
	return (state == MISComplete);
}

@end

typedef NS_ENUM(NSInteger, ElectricState) {
	ELReceived, ELRejectTheForm, ELSizeTheJob, ELSmokeAndJokeBreak,
	ELWaitForAuthorization, ELDoTheWrongJob, ELBlameTheEngineer,
	ELWaitToPunchOut, ELDoHalfAJob, ELComplainToEngineer,
	ELGetClarification, ELCompleteTheJob, ELTurnInThePaperwork,
	ELComplete
};


@interface ElectricianUnion : NSObject {
	ElectricState  state;
}

- (void) submitNetworkRequest;
- (BOOL) checkStatus;

@end

@implementation ElectricianUnion

- (instancetype) init {
	if (self = [super init]) {
		state = ELReceived;
	}
	return self;
}

- (void) submitNetworkRequest {
	state = ELReceived;
}

- (BOOL) checkStatus {
	state++;
	return (state == ELComplete);
}

@end

typedef NS_ENUM(NSInteger, FacilityState) {
	FAReceived, FAAssignToEngineer, FAEngineerResearches,
	FARequestIsNotPossible, FAEngineerLeavesCompany,
	FAAssignToNewEngineer, FANewEngineerResearches,
	FAReassignEngineer, FAEngineerReturns,
	FAEngineerResearchesAgain, FAEngineerFillsOutPaperWork,
	FAComplete
};

@interface FacilityDepartment : NSObject {
	FacilityState state;
}

- (void) submitNetworkRequest;
- (BOOL) checkStatus;

@end

@implementation FacilityDepartment

- (instancetype) init {
	if (self = [super init]) {
		state = FAReceived;
	}
	return self;
}

- (void) submitNetworkRequest {
	state = FAReceived;
}

- (BOOL) checkStatus {
	state++;
	return (state == FAComplete);
}

@end

#pragma mark -

typedef NS_ENUM(NSInteger, State) {
	Received, SubmitToEngineer, SubmitToElectrician,
	SubmitToTechnician
};

@interface FacilitiesFacade : NSObject {
	State state;
	NSInteger count;
	FacilityDepartment *engineer;
	ElectricianUnion *electrician;
	MisDepartment *technician;
}
- (void) submitNetworkRequest;
- (BOOL) checkStatus;

@property (nonatomic, readonly) NSInteger numberOfCalls;

@end

@implementation FacilitiesFacade

- (instancetype) init {
	if (self = [super init]) {
		state = Received;
		engineer = [[FacilityDepartment alloc] init];
		electrician = [[ElectricianUnion alloc] init];
		technician = [[MisDepartment alloc] init];
	}
	return self;
}

- (void) submitNetworkRequest {
	state = Received;
}

- (BOOL) checkStatus {
	count++;
	/* If request is received */
	if (state == Received)	{
		state++;
		/* Redirect it to engineer */
		[engineer submitNetworkRequest];
		NSLog(@"Submit request to Facilities. %ld calls so far", count);
	}
	else if (state == SubmitToEngineer)	{
		/* If enginner's work is over,
		 redurect request to electrician */
		if ([engineer checkStatus]) {
			state++;
			[electrician submitNetworkRequest];
			NSLog(@"Submit request to Electrician. %ld calls so far", count);
		}
	}
	else if (state == SubmitToElectrician) {
		/* electric finished - redirect to technician */
		if ([electrician checkStatus]) {
			state++;
			[technician submitNetworkRequest];
			NSLog(@"Submit request to MIS. %ld calls so far", count);
		}
	}
	else if (state == SubmitToTechnician) {
		/* The whole sequence is finished */
		if ([technician checkStatus])
			return 1;
	}
	/* Request is not finished yet */
	return 0;

}


- (NSInteger) numberOfCalls {
	return count;
}

@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		FacilitiesFacade *f = [[FacilitiesFacade alloc] init];
		
		[f submitNetworkRequest];
		/* Monitor state until job is finished */
		while (! [f checkStatus])
			;
		NSLog(@"Job successfully finished after %ld calls", f.numberOfCalls);
	}
    return 0;
}
