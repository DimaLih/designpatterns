//
//  main.swift
//  ObserverSwiftDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class SignalEmitter {
	func emitSignal(message : String, user : Int) {
		let userName = String.init(format: "Signal%d", user)
		NotificationCenter.default.post(name: Notification.Name(userName), object: message)
	}
	func emitBroadcast(message: String) {
		NotificationCenter.default.post(name: Notification.Name("Broadcast"), object: message)
	}
}

class Receiver {
	var userId = 0
	
	convenience init(userId : Int) {
		self.init()
		self.userId = userId;
		let userName = String.init(format: "Signal%d", userId)
		let nc = NotificationCenter.default
		
		nc.addObserver(self, selector: #selector(messageReceiver(notification:)), name: NSNotification.Name(rawValue: userName), object: nil)
		
		nc.addObserver(self, selector: #selector(broadcastReceiver(notification:)), name: NSNotification.Name(rawValue: "Broadcast"), object: nil)
		
	}
	
	@objc func messageReceiver(notification: Notification) {
		let message : String = notification.object as! String
		print("<<< User ",userId, " got message  : ", message)
	}
	
	@objc func broadcastReceiver(notification: Notification) {
		let message : String = notification.object as! String
		print("<<< User ",userId, " got broadcast : ", message)
	}
	
}

let radio = SignalEmitter.init()

let users : NSMutableArray = []

for i in 0 ... 5  {
	let user = Receiver.init(userId: i)
	users.add(user)
}

let transmission : [Int : String] = [
	1 : "Zoo news",
	2 : "Sport",
	4 : "Zo,bie VS Plants - the movie",
	3 : "Dandy in New York",
]

for (key,value) in transmission {
		radio.emitSignal(message: value, user: key)
}

radio.emitBroadcast(message: "Nebiru planet is coming!!!")



