//
//  main.m
//  TemplateMethodDemo
//
//  Created by Водолазкий В.В. on 13.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Base : NSObject

- (void) a;
- (void) e;
- (void) c;
- (void) ph1;
- (void) ph2;

- (void) execute;

@end

@implementation Base

- (void) a {
	printf("a ");
}

- (void) e {
	printf("e ");
}

- (void) c {
	printf("c ");
}

- (void) ph1 {}

- (void) ph2 {}


- (void) execute
{
	[self a];
	[self ph1];
	[self c];
	[self ph2];
	[self e];
	printf("\n");
}

@end

@interface One : Base
@end

@implementation One

- (void) ph1 {
	printf("b ");
}

- (void) ph2 {
	printf("d ");
}

@end

@interface Two : Base
@end

@implementation Two

- (void) ph1; {
	printf("2 ");
}

- (void) ph2 {
	printf("4 ");
}

@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		One *one = [One new];
		[one execute];
		Two *two = [Two new];
		[two execute];
	}
	return 0;
}
