//
//  main.m
//  AbdtractFactoryDemo
//
//  Created by Водолазкий В.В. on 29.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfantryMan : NSObject
@property (nonatomic, retain) NSString *info;
@end

@implementation InfantryMan
@end

@interface Horseman : NSObject
@property (nonatomic, retain) NSString *info;
@end

@implementation Horseman
@end

@interface Archer : NSObject
@property (nonatomic, retain) NSString *info;
@end

@implementation Archer
@end

#pragma mark -

@interface RomanInfantry : InfantryMan
@end

@implementation RomanInfantry

- (NSString *)info
{
	return @"Roman infantry";
}

@end

#pragma mark -

@interface CarthageInfantry : InfantryMan
@end

@implementation CarthageInfantry

- (NSString *)info
{
	return @"Carthage infantry";
}

@end


#pragma mark -

@interface RomanArcher : Archer
@end

@implementation RomanArcher

- (NSString *)info
{
	return @"Roman Archer";
}

@end

#pragma mark -

@interface CarthageArcher : Archer
@end

@implementation CarthageArcher

- (NSString *)info
{
	return @"Carphage Archer";
}

@end

#pragma mark -

@interface RomanHorseman : Horseman
@end

@implementation RomanHorseman

- (NSString *)info
{
	return @"Roman Horseman";
}
@end

#pragma mark -

@interface CarthageHorseman : Horseman
@end

@implementation CarthageHorseman

- (NSString *)info
{
	return @"Carthage Horseman";
}

@end

#pragma mark -

@interface ArmyFactory : NSObject

- (InfantryMan *) createInfantryMan;
- (Archer *) createArcher;
- (Horseman *) createHorseman;


@end

@implementation ArmyFactory


- (InfantryMan *) createInfantryMan
{
	return nil;
}

- (Archer *) createArcher
{
	return nil;
}

- (Horseman *) createHorseman
{
	return nil;
}

@end


#pragma mark -

@interface RomanArmyFactory : ArmyFactory
@end

@implementation RomanArmyFactory

- (InfantryMan *) createInfantryMan
{
	RomanInfantry *infantry = [RomanInfantry new];
	return infantry;
}

- (Archer *) createArcher
{
	RomanArcher *archer = [RomanArcher new];
	return archer;
}

- (Horseman *) createHorseman
{
	RomanHorseman *horseman = [RomanHorseman new];
	return horseman;
}

@end

#pragma mark -

@interface CarthageArmyFactory : ArmyFactory
@end

@implementation CarthageArmyFactory

- (InfantryMan *) createInfantryMan
{
	CarthageInfantry *infantry = [CarthageInfantry new];
	return infantry;
}

- (Archer *) createArcher
{
	CarthageArcher *archer = [CarthageArcher new];
	return archer;
}

- (Horseman *) createHorseman
{
	CarthageHorseman *horseman = [CarthageHorseman new];
	return horseman;
}

@end

@interface Army : NSObject

@property (nonatomic, retain) NSMutableArray <InfantryMan *> *infantry;
@property (nonatomic, retain) NSMutableArray <Archer *> *archer;
@property (nonatomic, retain) NSMutableArray <Horseman *> *horseman;

- (void) printInfo;


@end

@implementation Army

- (instancetype) init
{
	if (self = [super init]) {
		self.infantry = [NSMutableArray new];
		self.archer = [NSMutableArray new];
		self.horseman = [NSMutableArray new];
	}
	return self;
}

- (void) printInfo
{
	for (InfantryMan *inf in self.infantry) {
		NSLog(@"%@",[inf info]);
	}
	for (Archer *ar in self.archer) {
		NSLog(@"%@", [ar info]);
	}
	for (Horseman *hr in self.horseman) {
		NSLog(@"%@", [hr info]);
	}
}

@end

#pragma mark -

@interface Game : NSObject

+ (Army *) createWithFactory:(ArmyFactory *)factory;

@end

@implementation Game


+ (Army *) createWithFactory:(ArmyFactory *)factory
{
	Army *army = [[Army alloc] init];
	[army.infantry addObject:[factory createInfantryMan]];
	[army.archer addObject:[factory createArcher]];
	[army.horseman addObject:[factory createHorseman]];
	return army;
}

@end



#pragma mark - -


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		RomanArmyFactory *raf = [RomanArmyFactory new];
		Army *romanArmy = [Game createWithFactory:raf];
		CarthageArmyFactory *caf = [CarthageArmyFactory new];
		Army *carthageArmy = [Game createWithFactory:caf];
		
		NSLog(@"Roman Army:");
		[romanArmy printInfo];
		NSLog(@"Carthage Army:");
		[carthageArmy printInfo];
		
	}
    return 0;
}
