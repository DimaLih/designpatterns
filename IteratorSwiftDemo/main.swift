//
//  main.swift
//  IteratorSwiftDemo
//
//  Created by Водолазкий В.В. on 10.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class Book {
	var title	: String = ""
	var author	: String = ""
	
	convenience init(author : String, title : String) {
		self.init()
		self.title = title
		self.author = author
	}
	
	func description()-> String {
		return String.init(format: "Book: %@ by %@", self.title, self.author)
	}

}

class BookList {
	var list = NSMutableArray.init()
	
	func count() -> Int {
		return self.list.count
	}
	
	func addBook(book : Book) {
		self.list.add(book)
	}
	
	func getBook(bookNumber : Int) -> Book? {
		if bookNumber >= 0 && bookNumber < self.list.count {
			return (list[bookNumber] as! Book)
		}
		return nil
	}
	
	func removeBook(book : Book) -> Int {
		for i in 0 ..< self.count() {
			let b : Book = self.list[i] as! Book
			if b.title == book.title && b.author == book.author {
				self.list.remove(b)
				continue
			}
		}
		return self.count()
	}
	
}

class BookListIterator {
	var cBook = 0	// index of current record in the list
	var list = BookList.init()
	
	convenience init(list : BookList) {
		self.init()
		self.list = list
	}
	
	func getCurrentBook() -> Book? {
		if (cBook >= 0 && cBook < self.list.count()) {
			return self.list.getBook(bookNumber: cBook)
		}
		return nil
	}
	
	func getNextBook() -> Book? {
		if self.hasNetBook() {
			cBook += 1
			return self.list.getBook(bookNumber: cBook)
		}
		return nil
	}
	
	func hasNetBook() -> Bool {
		return (cBook < (self.list.count() - 1))
	}
}

class BookListReverseIterator : BookListIterator {
	
	convenience init(list : BookList) {
		self.init()
		self.list = list
		self.cBook = self.list.count() - 1
	}
	
	override func hasNetBook() -> Bool {
		return self.cBook > 0
	}
	
	override func getNextBook() -> Book? {
		if self.hasNetBook() {
			self.cBook -= 1
			return self.getCurrentBook()
		}
		return nil
	}

}


let firstBook = Book.init(author: "Atkinson and Suraski",
                          title: "Core PHP Programming, Third Edition");

let secondBook = Book.init(author: "Converse and Park",
                           title: "PHP Bible");
let thirdBook = Book.init(author: "Gamma, Helm, Johnson, and Vlissides",
                           title: "Design Patterns");

let bookList = BookList.init()
bookList.addBook(book: firstBook)
bookList.addBook(book: secondBook)
bookList.addBook(book: thirdBook)

print("Testing the iterator")
let bit = BookListIterator.init(list: bookList)
while bit.hasNetBook() {
	let b = bit.getNextBook()
	print (b?.description() ?? "Missed from the shelf in a mystical way")
}
print("get current book")
var cb = bit.getCurrentBook()
print (cb?.description() ?? "Cannot get current book")

print("Testing the Reverse iterator")

let rit = BookListReverseIterator.init(list: bookList)
while rit.hasNetBook() {
	let b = rit.getNextBook()
	print (b?.description() ?? "Missed from the shelf in a mystical way")
}
print("get current book eith Reverse Iterator")
cb = rit.getCurrentBook()
print (cb?.description() ?? "Cannot get current book")




