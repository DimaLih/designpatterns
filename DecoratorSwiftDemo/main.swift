//
//  main.swift
//  DecoratorSwiftDemo
//
//  Created by Водолазкий В.В. on 04.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. 
//  All rights reserved.
//

import Foundation

protocol DrawWidget {
	func draw()
}

class Textfield : DrawWidget {

	var w  : Int = 0
	var h  : Int = 0

	required convenience init(width: Int, height : Int ) {
		self.init()
		self.w = width
		self.h = height
	}
	
	func draw() {
		print("TextField width",self.w, "height", self.h)
	}

}

class Decorator : DrawWidget {
	var w : DrawWidget? = nil
		
	convenience init(widget : DrawWidget) {
		self.init()
		self.w = widget
	}
	
	func draw() {
		w?.draw()
	}
}

class BorderDecorator : Decorator {
	
	override func draw() {
		super.draw()
		print("  BorderDecorator");
	}
}

class ScrollviewDecorator : Decorator {
	
	override func draw() {
		super.draw()
		print("    ScrollviewDecorator")
	}
}


	let tf = Textfield.init(width: 80, height: 24)
	let widget = ScrollviewDecorator.init(widget: BorderDecorator.init(widget: tf))

	widget.draw()

