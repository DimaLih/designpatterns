//
//  main.swift
//  FlyWeightSwiftDemo
//
//  Created by Водолазкий В.В. on 09.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

import Foundation


class Book {
	var author : String = ""
	var title  : String = ""
	
	convenience init(author : String, title : String) {
		self.init()
		self.author = author
		self.title = title
	}
	
	func showBook() {
		print("Author:", self.author, "Title :", self.title)
	}
}

class BookFactory {
	private var books  = [ String : Book]()
	
	init() {
		books.updateValue(self.makeBook1(), forKey: "Book1")
		books.updateValue(self.makeBook2(), forKey: "Book2")
		books.updateValue(self.makeBook3(), forKey: "Book3")
	}
	
	
	func bookforKey(key : String) -> Book {
		return books[key]!
	}
	
	private func makeBook1()-> Book {
		return Book.init(author: "Terry Pratchet", title: "Small Gods")
	}
	
	private func makeBook2()-> Book {
		return Book.init(author: "Terry Pratchet", title: "The Colour of Magic")
	}
	
	private func makeBook3()-> Book {
		return Book.init(author: "Terry Pratchet", title: "Guards! Guards!")
	}
}

class BookShelf {
	var books = [Book]()

	func addBook(book : Book) {
		books.append(book)
	}
	
	func showBooks() {
		for b in books {
			b.showBook()
		}
	}
}


let publisher = BookFactory.init()
let shelf1 = BookShelf.init()
let shelf2 = BookShelf.init()

shelf1.addBook(book: publisher.bookforKey(key: "Book1"))
shelf1.addBook(book: publisher.bookforKey(key: "Book2"))
shelf1.addBook(book: publisher.bookforKey(key: "Book1"))

shelf2.addBook(book: publisher.bookforKey(key: "Book1"))
shelf2.addBook(book: publisher.bookforKey(key: "Book3"))
shelf2.addBook(book: publisher.bookforKey(key: "Book2"))

print("Shelf 1")
shelf1.showBooks()
print("Shelf 2")
shelf2.showBooks()


