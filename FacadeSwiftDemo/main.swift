//
//  main.swift
//  FacadeSwiftDemo
//
//  Created by Водолазкий В.В. on 04.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

import Foundation

enum MISStates : Int {
	case MISReceived = 1
	case MISDenyAllKnowledge
	case MISReferClientToFacilities
	case MISFacilitiesHasNotSentPaperwork
	case MISElectricianIsNotDone
	case MISElectricianDidItWrong
	case MISDispatchTechnician
	case MISSignedOff
	case MISDoesNotWork
	case MISFixElectriciansWiring
	case MISComplete
};

class MISDepartment {
	var state : MISStates = MISStates.MISReceived
	
	func submitNetworkRequest() {
		state = MISStates.MISReceived
	}
	
	func checkStatus() -> Bool {
		state = MISStates(rawValue: state.rawValue + 1)!
		if (state == MISStates.MISComplete) {
			return true
		}
		return false
	}
}

enum ElectricianStates : Int {
	case ELReceived = 1
	case ELRejectTheForm, ELSizeTheJob, ELSmokeAndJokeBreak,
	ELWaitForAuthorization, ELDoTheWrongJob, ELBlameTheEngineer,
	ELWaitToPunchOut, ELDoHalfAJob, ELComplainToEngineer,
	ELGetClarification, ELCompleteTheJob, ELTurnInThePaperwork,
	ELComplete
}

class ElectricianDepartment {
	var state : ElectricianStates = ElectricianStates.ELReceived
	
	func submitNetworkRequest() {
		state = ElectricianStates.ELReceived
	}
	
	func checkStatus() -> Bool {
		state = ElectricianStates(rawValue: state.rawValue + 1)!
		return (state == ElectricianStates.ELComplete)
	}
}

enum FacilitiesStates : Int {
	case FAReceived = 1, FAAssignToEngineer, FAEngineerResearches,
	FARequestIsNotPossible, FAEngineerLeavesCompany,
	FAAssignToNewEngineer, FANewEngineerResearches,
	FAReassignEngineer, FAEngineerReturns,
	FAEngineerResearchesAgain, FAEngineerFillsOutPaperWork,
	FAComplete

}

class Facilities {
	var state : FacilitiesStates = FacilitiesStates.FAReceived
	
	func submitNetworkRequest() {
		state = FacilitiesStates.FAReceived
	}
	
	func checkStatus() -> Bool {
		state = FacilitiesStates(rawValue: state.rawValue + 1)!
		return (state == FacilitiesStates.FAComplete)
	}
}

enum MainStates : Int {
	case Received = 1,
	SubmitToEngineer, SubmitToElectrician,
	SubmitToTechnician

}

class FacilitiesFacade {
	let facility = Facilities.init()
	let electrician = ElectricianDepartment.init()
	let technician = MISDepartment.init()
	
	var state : MainStates = MainStates.Received
	var count = 0
	
	func submitNetworkRequest() {
		state = MainStates.Received
	}
	
	func incrementState() {
		state = MainStates(rawValue: state.rawValue + 1)!
	}
	
	func checkStatus() -> Bool {
		count += 1
		if (state == MainStates.Received) {
			self.incrementState()
			facility.submitNetworkRequest()
			print("Submit request to Facilities.", count, "calls so far")
		} else if (state == MainStates.SubmitToEngineer) {
			if (facility.checkStatus()) {
				self.incrementState()
				electrician.submitNetworkRequest()
				print("Submit request to Electrician.", count, "calls so far")
			}
		} else if (state == MainStates.SubmitToElectrician) {
			if (electrician.checkStatus()) {
				self.incrementState()
				technician.submitNetworkRequest()
				print("Submit request to Facilities.", count, "calls so far")
			}
		} else if (state == MainStates.SubmitToTechnician) {
			if (technician.checkStatus()) {
				return true
			}
		}
		return false
	}
}

let f = FacilitiesFacade.init()
f.submitNetworkRequest()

while !f.checkStatus() { }

print("Job successfully finished after", f.count, "calls")



