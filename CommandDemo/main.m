//
//  main.m
//  CommandDemo
//
//  Created by Водолазкий В.В. on 06.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Game : NSObject

- (void) create;
- (void) open:(NSString *) fileName;
- (void) save:(NSString *) fileName;
- (void) makeMove:(NSString *)move;

@end

@implementation Game

- (void) create {
	NSLog(@"Game created");
}

- (void) open:(NSString *)fileName
{
	NSLog(@"Game file opened - %@", fileName);
}

- (void) save:(NSString *)fileName
{
	NSLog(@"Game file saved - %@", fileName);
}

- (void) makeMove:(NSString *)move
{
	NSLog(@"next move - %@", move);
}

@end

@interface Command : NSObject
- (instancetype) initWithGame:(Game *)game;
- (void) execute;
@end

@interface Command ()
@property (nonatomic, retain) Game *g;
@end

@implementation Command

- (instancetype) initWithGame:(Game *)game
{
	if (self = [super init]) {
		self.g = game;
	}
	return self;
}

- (void) execute
{
	NSLog(@"Abstract implementation - Override me!");
}

@end

@interface CreateGameCommand : Command
@end

@implementation CreateGameCommand

- (void) execute
{
	[self.g create];
}
@end

@interface OpenGameCommand : Command
@end

@implementation OpenGameCommand

- (void) execute
{
	[self.g open:@"SAVE_FILE"];
}

@end

@interface SaveGameCommand : Command
@end

@implementation SaveGameCommand

- (void) execute
{
	[self.g save:@"SAVE_FILE"];
}

@end

@interface MakeMoveCommand : Command
{
	NSString *move;
}

- (instancetype) initWithGame:(Game *)game andMove:(NSString *)moveStr;

@end

@implementation MakeMoveCommand

- (instancetype) initWithGame:(Game *)game andMove:(NSString *)moveStr
{
	if (self = [super initWithGame:game]) {
		move = moveStr;
	}
	return self;
}

- (void) execute
{
	[self.g save:@"LAST_MOVE"];
	[self.g makeMove:move];
}

@end

@interface UndoLastMoveCommand : Command
@end

@implementation UndoLastMoveCommand

- (void) execute
{
	[self.g open:@"LAST_MOVE"];
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {

		Game *game = [[Game alloc] init];
		NSMutableArray <Command *> *actions = [NSMutableArray new];
		
		// create new game
		CreateGameCommand *newGame = [[CreateGameCommand alloc] initWithGame:game];
		[actions addObject:newGame];
		
		// start the game
		MakeMoveCommand *move1 = [[MakeMoveCommand alloc] initWithGame:game andMove:@"e2-e4"];
		[actions addObject:move1];
		MakeMoveCommand *move2 = [[MakeMoveCommand alloc] initWithGame:game andMove:@"h7-h5"];
		[actions addObject:move2];
		
		// undo last move
		UndoLastMoveCommand *undo = [[UndoLastMoveCommand alloc] initWithGame:game];
		[actions addObject:undo];
		
		// and save game
		SaveGameCommand *save = [[SaveGameCommand alloc] initWithGame:game];
		[actions addObject:save];
		
		// now we can apply all this sequence
		for (NSInteger i = 0; i < actions.count; i++) {
			Command *c = actions[i];
			[c execute];
		}
		
	
	
	}
	return 0;
}
