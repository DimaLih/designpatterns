//
//  main.m
//  CompositeDemo
//
//  Created by Водолазкий В.В. on 04.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Unit : NSObject


@property (nonatomic, readonly) NSInteger strength;

@end

@implementation Unit

- (NSInteger) strength
{
	return 0;
}

@end

@interface Archer : Unit
@end

@implementation Archer

- (NSInteger) strength
{
	return 1;
}

@end

@interface Infantry : Unit
@end

@implementation Infantry

- (NSInteger) strength
{
	return 2;
}

@end

@interface Horseman : Unit
@end

@implementation Horseman

- (NSInteger) strength {
	return 3;
}

@end

@interface CompositeUnit : NSObject {
	NSMutableArray *units;
}

@property (nonatomic, readonly) NSInteger strength;

- (void) addUnit:(id)aUnit;

@end

@implementation CompositeUnit

- (instancetype) init {
	if (self = [super init]) {
		units = [NSMutableArray new];
	}
	return self;
}

- (void) addUnit:(Unit *)aUnit {
	[units addObject:aUnit];
}

- (NSInteger) strength {
	NSInteger sum = 0;
	for (id u in units) {
		sum += [u strength];
	}
	return sum;
}


@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // Create Army which has 4 legions
		CompositeUnit *army = [CompositeUnit new];
		for (int lc = 0; lc < 4; lc++) {
			
			CompositeUnit *c = [CompositeUnit new];
			//Each legion contains 3000 heave infantry units
			for (int i = 0; i < 3000; i++) {
				[c addUnit:[Infantry new]];
			}
			//  1200 light infantry
			for (int i = 0; i < 1200; i++) {
				[c addUnit:[Archer new]];
			}
			// and 300 horsemen
			for (int i = 0; i < 300; i++) {
				[c addUnit:[Horseman new]];
			}
			
			[army addUnit:c];
		}
		NSLog(@"Total strength of army - %ld",army.strength);
		
	}
    return 0;
}
