//
//  main.swift
//  MultitonSwiftDemo
//
//  Created by Водолазкий В.В. on 26.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. 
//  All rights reserved.
//

import Foundation



class InfantryMan {
    static let sharedInstance = InfantryMan()
    
    private init() {
        // to prevent it direct call
    }
    
    func info() {
        print("InfantryMan")
    }
}

class Archer {
    static let sharedInstance = Archer()
    
    private init() {
        // to prevent it direct call
    }

    func info() {
        print("Archer")
    }
}

class Horseman {
    static let sharedInstance = Horseman()
    
    private init() {
        // to prevent it direct call
    }

    func info() {
        print("Horseman")
    }
}

class SharedUnit {
    
    
    private var sharedPool : Dictionary <String, Any>  = [ : ]
   
    func extracUnit( className : String) -> Any {
        
        var result = sharedPool[className]
        if (result == nil) {
            switch(className) {
                case "InfantryMan" :
                    result = InfantryMan.sharedInstance;
                break
                case "Archer" :
                    result = Archer.sharedInstance;
                break
                case "Horseman":
                    result = Horseman.sharedInstance;
                break
                default:
                    // unsupported class
                    return NSNull()
            }
            sharedPool[className] = result
        }
        return result as Any

    }
}

let barracks = SharedUnit()

let infantry = barracks.extracUnit(className: "InfantryMan") as! InfantryMan
infantry.info()
let archer = barracks.extracUnit(className: "Archer") as! Archer
archer.info()
let horseman = barracks.extracUnit(className: "Horseman") as! Horseman
horseman.info()


