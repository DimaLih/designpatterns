//
//  main.m
//  LazyInitDemo
//
//  Created by Водолазкий В.В. on 24.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Beer : NSObject

@property (nonatomic, retain) NSString *beerName;

+ (void) printBeerTypes;

@end

@implementation Beer

static NSMutableDictionary *beerTypes = nil;

- (NSString *)description
{
    return [NSString stringWithFormat:@"Beer: %@", self.beerName];
}

+ (void) printBeerTypes
{
    for (NSString *key in beerTypes) {
        NSLog(@"%@", beerTypes[key]);
    }
    NSLog(@"");
}

+ (Beer *) getBeerByName:(NSString *)aName
{
    if (!beerTypes) {
        beerTypes = [NSMutableDictionary new];
    }
    Beer *oldBeer = beerTypes[aName];
    if (oldBeer) {
        return oldBeer;
    }
    // Create new beer with supplied name
    Beer *newBeer = [[Beer alloc] init];
    newBeer.beerName = aName;
    beerTypes[aName] = newBeer;
    return newBeer;
}

@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {

        Beer *b = [Beer getBeerByName:@"Gambrinus"];
        [Beer printBeerTypes];
        
        b = [Beer getBeerByName:@"KarelIV"];
        [Beer printBeerTypes];
        
        b = [Beer getBeerByName:@"Gambrinus"];
        [Beer printBeerTypes];
    
    }
    return 0;
}
