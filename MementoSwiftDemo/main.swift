//
//  main.swift
//  MementoSwiftDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class BookReader {
	var page = 0
	var title = ""
	
	convenience init(title : String, page : Int) {
		self.init()
		self.page = page
		self.title = title
	}
}

class BookMark {
	var page = 0
	var title = ""
	
	convenience init(reader : BookReader) {
		self.init()
		self.page = reader.page
		self.title = reader.title
	}
	
	func setPage(reader : BookReader) {
		self.page = reader.page
	}
	
	func getPage(reader : BookReader) {
		reader.page = self.page
	}
	
	func setTitle(reader : BookReader) {
		self.title = reader.title
	}
	
	func getTitle(reader : BookReader) {
		reader.title = self.title
	}
}



print("BEGIN TESTING MEMENTO PATTERN")
print("")

let br = BookReader.init(title: "Core PHP Programming, Third Edition", page: 103)
let bm = BookMark.init(reader: br)


print("(at beginning) bookReader title:", br.title)
print("(at beginning) bookReader page: ", br.page)

br.page = 104
bm.setPage(reader: br)

print("(one page later) bookReader page: ", br.page)

br.page = 2005	 //oops! a typo

print("(after typo) bookReader page: ", br.page)

bm.getPage(reader: br)
print("(back to one page later) bookReader page: ", br.page)
print("")

print("END TESTING MEMENTO PATTERN")

