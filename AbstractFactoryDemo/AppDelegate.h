//
//  AppDelegate.h
//  AbstractFactoryDemo
//
//  Created by Водолазкий В.В. on 29.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

