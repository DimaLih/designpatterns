//
//  main.m
//  AbstractFactoryDemo
//
//  Created by Водолазкий В.В. on 29.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//
//  AbstractFactory.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 29.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "AbstractFactory.h"


@interface InfantryMan : NSObject
@property (nonatomic, retain) NSString *info;
@end

@interface Horseman : NSObject
@property (nonatomic, retain) NSString *info;
@end

@interface Archer : NSObject
@property (nonatomic, retain) NSString *info;
@end

#pragma mark -

@interface RomanInfantry : InfantryMan
@end

@implementation RomanInfantry

- (NSString *)info
{
	return @"Roman infantry";
}

@end

#pragma mark -

@interface CarthageInfantry : InfantryMan
@end

@implementation CarthageInfantry

- (NSString *)info
{
	return @"Carthage infantry";
}

@end


#pragma mark -

@interface RomanArcher : Archer
@end

@implementation RomanArcher

- (NSString *)info
{
	return @"Roman Archer";
}

@end

#pragma mark -

@interface CarthageArcher : Archer
@end

@implementation CarthageArcher

- (NSString *)info
{
	return @"Carphage Archer";
}

@end

#pragma mark -

@interface RomanHorseman : Horseman
@end

@implementation RomanHorseman

- (NSString *)info
{
	return @"Roman Horseman";
}
@end

#pragma mark -

@interface CarthageHorseman : Horseman
@end

@implementation CarthageHorseman

- (NSString *)info
{
	return @"Carthage Horseman";
}

@end

#pragma mark -

@interface ArmyFactory : NSObject

@property (nonatomic, retain) NSMutableArray < InfantryMan *> *infantry;
@property (nonatomic, retain) NSMutableArray < Archer *> *archer;
@property (nonatomic, retain) NSMutableArray < Horseman *> *horseman;

- (InfantryMan *) createInfantryMan;
- (Archer *) createArcher;
- (Horseman *) createHorseman;


@end

@implementation ArmyFactory

- (instancetype) init
{
	if (self = [super init]) {
		self.infantry = [NSMutableArray new];
		self.horseman = [NSMutableArray new];
		self.archer = [NSMutableArray new];
	}
	return self;
}

- (InfantryMan *) createInfantryMan
{
	return nil;
}

- (Archer *) createArcher
{
	return nil;
}

- (Horseman *) createHorseman
{
	return nil;
}

@end


#pragma mark -

@interface RomanArmyFactory : ArmyFactory
@end

@implementation RomanArmyFactory

- (InfantryMan *) createInfantryMan
{
	RomanInfantry *infantry = [RomanInfantry new];
	return infantry;
}

- (Archer *) createArcher
{
	RomanArcher *archer = [RomanArcher new];
	return archer;
}

- (Horseman *) createHorseman
{
	RomanHorseman *horseman = [RomanHorseman new];
	return horseman;
}

@end

#pragma mark -

@interface CarthageArmyFactory : ArmyFactory
@end

@implementation CarthageArmyFactory

- (InfantryMan *) createInfantryMan
{
	CarthageInfantry *infantry = [CarthageInfantry new];
	return infantry;
}

- (Archer *) createArcher
{
	CarthageArcher *archer = [CarthageArcher new];
	return archer;
}

- (Horseman *) createHorseman
{
	CarthageHorseman *horseman = [CarthageHorseman new];
	return horseman;
}

@end


#pragma mark - -


@implementation AbstractFactory

@end


int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
