//
//  main.swift
//  VisitorSwiftDemo
//
//  Created by Водолазкий В.В. on 13.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation


class Visitor {
	var note : String = ""
	
	func visitBook(book: BookVisitee) {}
	func visitSoftware(software : SoftwareVisistee) {}
	
}

class Visitee {
	func acceptVisitor(visitor: Visitor) {}
}

class BookVisitee : Visitee {
	var title : String = ""
	var author : String = ""
	
	convenience init(title : String, author : String) {
		self.init()
		self.title = title
		self.author = author
	}
	
	override func acceptVisitor(visitor : Visitor) {
		visitor .visitBook(book: self)
	}
}

class SoftwareVisistee :Visitee {
	var title : String = ""
	var company : String = ""
	var url : String = ""
	
	convenience init(title : String, company : String, url : String) {
		self.init()
		self.title = title
		self.company = company
		self.url = url
	}
	
	override func acceptVisitor(visitor : Visitor) {
		visitor .visitSoftware(software: self)
	}
}


class PlainDescriptorVisitor : Visitor {
	
	override func visitBook(book: BookVisitee) {
		self.note = String.init(format: "%@ written by %@",
		                        arguments: [book.title, book.author])
	}
	
	override func visitSoftware(software: SoftwareVisistee) {
		self.note = NSString(format:"%@ made by %@ company website : %@",
		software.title, software.company, software.url) as String
	}
}


class FancyDescriptionVisitor : Visitor {

	override func visitBook(book: BookVisitee) {
		self.note = NSString(format: "%@ ...!*@*! written !*! by !@! %@",
		                     book.title, book.author) as String
	}
	
	override func visitSoftware(software: SoftwareVisistee) {
		self.note = NSString(format:"%@ ...!!! made !*! by !@@!  %@ ...www website !**! at http:// : %@",
		                     software.title, software.company, software.url) as String
	}
}

//double dispatch any visitor and visitee objects

func acceptVisitor(visitee : Visitee, visitor : Visitor) {
	visitee.acceptVisitor(visitor :visitor);
}



print("BEGIN TESTING VISITOR PATTERN")
print("")

let book = BookVisitee.init(title: "Design Patterns",
                            author: "Gamma, Helm, Johnson, and Vlissides")

let soft = SoftwareVisistee.init(title: "Zend Studio",
                                 company: "Zend Technologies",
                                 url: "www.zend.com")


let plainVisitor = PlainDescriptorVisitor.init()

acceptVisitor(visitee : book, visitor : plainVisitor)

print("plain description of book: ", plainVisitor.note)

acceptVisitor(visitee : soft, visitor : plainVisitor)

print("plain description of software: ",plainVisitor.note)
print("");

let fancy = FancyDescriptionVisitor.init()

acceptVisitor(visitee : book, visitor : fancy)

print("fancy description of book: ", fancy.note)

acceptVisitor(visitee : soft, visitor : fancy)

print("fancy description of software: ",fancy.note)
print("")

print("END TESTING VISITOR PATTERN")


