//
//  main.m
//  PrototypeDemo
//
//  Created by Водолазкий В.В. on 25.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Warrior : NSObject <NSCopying>

@property (nonatomic, retain) NSString *info;

@end

@implementation Warrior

- (instancetype) init
{
    if (self = [super init]) {
        self.info = @"Abstract Warrior";
    }
    return self;
}

//
// NSCopying protocol is used to "clone" objects
//
- (id)copyWithZone:(nullable NSZone *)zone
{
    Warrior *newWarrior = [[Warrior alloc] init];
    newWarrior.info = self.info;
    return newWarrior;
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@", self.info];
}

@end

@interface InfantryMan : Warrior

@end

@implementation InfantryMan

- (instancetype) init
{
    if (self = [super init]) {
        self.info = @"InfantryMan";
    }
    return self;
}

@end

@interface Archer : Warrior
// To demonstrate additional properties in ancestors
@property (nonatomic,readwrite) NSInteger range;
@end

@implementation Archer

- (instancetype) init
{
    if (self = [super init]) {
        self.info = @"Archer";
        self.range = 3;
    }
    return self;
}

- (id) copyWithZone:(NSZone *)zone
{
    Archer *newArcher = [[Archer alloc] init];
    newArcher.info = self.info;
    newArcher.range = self.range;
    return newArcher;
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@ : Range %ld",
            self.info, self.range];
}

@end

@interface HorseMan : Warrior
@end

@implementation HorseMan

- (instancetype) init
{
    if (self = [super init]) {
        self.info = @"Horseman";
    }
    return self;
}

@end

#pragma mark -

@interface Barracks : NSObject

+ (InfantryMan *) createInfantryMan;
+ (Archer *) createArcher;
+ (HorseMan *) createHorseman;

@end

@implementation Barracks

+ (InfantryMan *) createInfantryMan
{
    static InfantryMan *_infantryManTemplate = nil;
    if (!_infantryManTemplate) {
        _infantryManTemplate = [[InfantryMan alloc] init];
    }
    return [_infantryManTemplate copy];
}

+ (Archer *) createArcher
{
    static Archer *_archerTemplate = nil;
    if (!_archerTemplate) {
        _archerTemplate = [[Archer alloc] init];
    }
    return [_archerTemplate copy];
}

+ (HorseMan *) createHorseman
{
    static HorseMan *_horsemanTemplate = nil;
    if (!_horsemanTemplate) {
        _horsemanTemplate = [[HorseMan alloc] init];
    }
    return [_horsemanTemplate copy];
}

@end


#pragma mark -

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableArray *army = [NSMutableArray new];
        [army addObject:[Barracks createInfantryMan]];
        [army addObject:[Barracks createArcher]];
        [army addObject:[Barracks createHorseman]];

        NSLog(@"%@",army);
    
    
    }
    return 0;
}
