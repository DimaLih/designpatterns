//
//  main.swift
//  StrategySwiftDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

enum StrategyIndex {
	case UpperCase
	case Exclaim
	case Star
}

class Book {
	var title :String = ""
	var author : String = ""
	
	convenience init(title  : String, author : String) {
		self.init()
		self.title = title
		self.author = author
	}
	
	func authorAndTitle() -> String {
		return String.init(format: "%@ by %@", self.title, self.author)
	}
}

class StrategyInterface {
	func showBookTitle(book : Book) -> String {
		return "Error"
	}
}

class StrategyUppercase : StrategyInterface {
	override func showBookTitle(book: Book) -> String {
		return book.title.uppercased()
	}
}

class StrategyExclaim : StrategyInterface {
	override func showBookTitle(book: Book) -> String {
		return book.title.replacingOccurrences(of: " ", with: "!")
	}
}

class StrategyStar : StrategyInterface {
	override func showBookTitle(book: Book) -> String {
		return book.title.replacingOccurrences(of: " ", with: "*")
	}
}

class StrategyContext {
	var strategy : StrategyInterface? = nil
	
	convenience init(index : StrategyIndex) {
		self.init()
		switch index {
		case StrategyIndex.UpperCase	:	self.strategy = StrategyUppercase.init()
		case StrategyIndex.Star			:	self.strategy = StrategyStar.init()
		case StrategyIndex.Exclaim		:	self.strategy = StrategyExclaim.init()
		}
	}
	
	func showTitle(book : Book) -> String {
		return (strategy?.showBookTitle(book:book))!
	}
}


let b = Book.init(title: "PHP for Cats", author: "Larry Truett")

let sc = StrategyContext.init(index: StrategyIndex.UpperCase)
let se = StrategyContext.init(index: StrategyIndex.Exclaim)
let ss = StrategyContext.init(index: StrategyIndex.Star)

print("test 1 - show name context Uppercase")
print( sc.showTitle(book :b) )

print("test 1 - show name contextStar")
print( ss.showTitle(book :b) )

print("test 1 - show name context Exclaim")
print( se.showTitle(book :b) )

print("END TESTING STRATEGY PATTERN")





