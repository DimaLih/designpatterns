//
//  main.m
//  ObserverDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignalEmitter : NSObject
- (void)emitSignal:(NSString *)payload toUser:(NSInteger)userId;
- (void) emitBroadcast:(NSString *)message;
@end;

@implementation SignalEmitter

- (void)emitSignal:(NSString *)payload toUser:(NSInteger)userId
{
	NSLog(@">>> Emitter: send signal to user #%ld",(long)userId);
	NSString *signal = [NSString stringWithFormat:@"Signal_%ld",(long)userId];
	[[NSNotificationCenter defaultCenter] postNotificationName:signal object:payload];
}

- (void) emitBroadcast:(NSString *)message
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"BroadcastSignal" object:message];
}

@end

@interface Receiver : NSObject
{
	NSInteger userId;
}
- (instancetype) initWithUserId:(NSInteger) userId;

@end

@implementation Receiver

- (instancetype) initWithUserId:(NSInteger)aId
{
	if (self = [super init]) {
		userId = aId;
		NSString *signalName = [NSString stringWithFormat:@"Signal_%ld",(long)userId];
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self selector:@selector(signalReceived:)
				   name:signalName object:nil];
		[nc addObserver:self selector:@selector(broadcastReceived:)
				   name:@"BroadcastSignal" object:nil];
	}
	return self;
}

- (void) signalReceived:(NSNotification *) note
{
	NSString *message = [note object];
	NSLog(@"<<< User %ld received signal: %@", (long)userId, message);
}

- (void) broadcastReceived:(NSNotification *)note
{
	NSString *message = [note object];
	NSLog(@"<<< User %ld received broadcast: %@", (long)userId, message);
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		SignalEmitter *radio = [[SignalEmitter alloc] init];
		NSMutableArray *receivers = [NSMutableArray new];
		for (NSInteger i = 0; i < 6; i++) {
			Receiver *r = [[Receiver alloc] initWithUserId:i];
			[receivers addObject:r];
		}
		
		NSDictionary *transmission = @{
									   @0:	@"News",
									   @1:	@"Sport",
									   @5:	@"Objective-C and life",
									   @4:	@"Walking with wolves",
									   @3:	@"Springfiled market",
									   };
		for (NSNumber *key in transmission) {
			NSString *message = transmission[key];
			[radio emitSignal:message toUser:key.integerValue];
		}
		
		[radio emitBroadcast:@"Breaking News - for all receivers"];
		
	}
	return 0;
}
