//
//  main.swift
//  AdapterSwiftDemo
//
//  Created by Водолазкий В.В. on 27.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. 
//  All rights reserved.
//

import Foundation

class OldStyleSensor {
    
    func getTempInFahrengeit() -> Float {
        return 32.0;        // 0 on Celsius
    }
}

class Sensor {
    func  getTemperature() -> Float {
        return -273.0;
    }
}

class Adapter : Sensor {
    
    private let oldSensor  = OldStyleSensor.init()
    
    override func getTemperature() -> Float {
        let t = (oldSensor.getTempInFahrengeit() - 32.0) * 5.0/9.0
        return t
    }
}


    let tempSensor = Adapter.init() as Sensor
    print("temperature in Celsius =", tempSensor.getTemperature())

