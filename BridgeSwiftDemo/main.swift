//
//  main.swift
//  BridgeSwiftDemo
//
//  Created by Водолазкий В.В. on 27.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. 
//  All rights reserved.
//

import Foundation

class TimeImpl {
    var hour : Int = 0
    var minute : Int = 0
    
    convenience init(hour : Int, minute : Int ) {
        self.init()
        self.hour = hour
        self.minute = minute
    }
    
    func printInfo() {
        print("Time - ",self.hour,":", self.minute)
    }
}

class CivilianTimeImpl : TimeImpl {
    var meridianString : String = ""
    
    required convenience init(hour : Int, minute: Int, isAm : Bool) {
        self.init(hour: hour, minute: minute)
        if (isAm) {
            meridianString = "AM"
        } else {
            meridianString = "PM"
        }
    }
    
     override func printInfo() {
        print("Civilian Time - ",self.hour,":",
              self.minute, self.meridianString)

    }
}

class ZuluTimeImpl : TimeImpl {
    var zoneString : String = ""
    
    required convenience init(hour : Int, minute: Int, zone : Int) {
        self.init(hour: hour, minute: minute)
        if (zone == 5) {
            zoneString = "Eastern Standard Time"
        } else if (zone == 6){
            zoneString = "Central Standard Time"
        }
    }
    
    override func printInfo() {
        print("ZULU Time - ",self.hour,":",
              self.minute, self.zoneString)
        
    }
}

class Time {
	
	var time : TimeImpl? = nil
	
	required convenience init(hour : Int, minute: Int) {
		self.init()
		self.time = TimeImpl.init(hour: hour, minute: minute)
	}
	
	func printInfo() {
		self.time?.printInfo()
	}
}

class Civiliantime : Time {
	
	override init() {}
	
	required convenience init(hour : Int, minute: Int, isAm : Bool) {
		self.init()
		self.time = CivilianTimeImpl.init(hour: hour, minute: minute, isAm: isAm)
	}
}

class ZuluTime : Time {
	
	override init() {}
	
	required convenience init(hour : Int, minute: Int, zone : Int) {
		self.init()
		self.time = ZuluTimeImpl.init(hour: hour,
		                              minute: minute,
		                              zone: zone)
	}
}


	let t1 = Time.init(hour: 11, minute: 45)
	t1.printInfo()
	let t2 = Civiliantime.init(hour: 11, minute: 45, isAm: false)
	t2.printInfo()
	let t3 = ZuluTime.init(hour: 11, minute: 45, zone: 6)
	t3.printInfo()

