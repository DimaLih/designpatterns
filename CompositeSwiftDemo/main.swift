//
//  main.swift
//  CompositeSwiftDemo
//
//  Created by Водолазкий В.В. on 04.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

protocol Strength {
	func strength() -> Int
}

class Unit : Strength {
	func strength() -> Int {
		return 0
	}
}

class Archer : Unit {

	override func strength() -> Int {
		return 1
	}
}

class Infantry : Unit {

	override func strength() -> Int {
		return 2
	}
}

class Horseman : Unit {
	
	override func strength() -> Int {
		return 3
	}
}

class CompositeUnit : Strength {
	var units = [Strength] ()
	
	func addUnit(unit: Strength) {
		units.append(unit)
	}
	
	func strength() -> Int {
		var sum : Int = 0
		for i in 0 ..< units.count {
			let u : Strength = units[i]
			sum += u.strength()
		}
		return sum
	}
	
}


let army = CompositeUnit.init()
// each army has 4 legions
for _ in 0 ..< 4 {
	let legion = CompositeUnit.init()
	// each legion has 1200 archers
	for _ in 0 ..< 1200 {
		legion.addUnit(unit: Archer.init())
	}
	// 3000 heavy infantry
	for _ in 0 ..< 3000 {
		legion.addUnit(unit: Infantry.init())
	}
	// And 300 horsemen
	for _ in 0 ..< 300 {
		legion.addUnit(unit: Horseman.init())
	}
	army.addUnit(unit: legion)
}

print ("Total strength of army -",army.strength())

