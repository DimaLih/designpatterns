//
//  main.swift
//  PrototypeSwiftDemo
//
//  Created by Водолазкий В.В. on 25.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. 
//  All rights reserved.
//

import Foundation

class Warrior : NSObject, NSCopying {
    var info : String = "Abstract Warrior"
    var persNumber : Int = 0
  
    required override init()  {
        // This initializer must be required, because another
        // initializer `init(_ model: Warrior)` is required
        // too and we would like to instantiate `Warrior`
        // with simple `Warrior()` as well.
    }
    
    required init(_ model: Warrior) {
        // This initializer must be required unless `GameModel`
        // class is `final`
        info = model.info
        persNumber = model.persNumber
    }

     public func copy(with zone: NSZone? = nil) -> Any {
        return type(of:self).init(self)
    }
    
    func printInfo() {
        print( self.className, " #", self.persNumber)
    }
}

class InfantryMan : Warrior {
    
    required init() {
        super.init()
        self.info = "InfantryMan"
    }
    
    required init(_ model: Warrior) {
        super.init()
        info = model.info
        persNumber = model.persNumber
    }
    
}

class Archer : Warrior {
    
    var range : Int
    
    required convenience init(_ model : Warrior) {
        self.init()
        self.range = 3;
        self.persNumber = model.persNumber
        self.info = model.info
    }
    
    required init() {
        range = 3
        super.init()
        info = "Archer"
    }
    
    override func copy(with zone: NSZone?) -> Any {
        let newArcher = Archer.init()
        newArcher.info = self.info
        newArcher.range = self.range
        newArcher.persNumber = self.persNumber
        return newArcher
    }
    
    override func printInfo() {
        print(self.className,"#",
              self.persNumber, "Range: ", self.range)
    }

}

class Horseman : Warrior {
    required init() {
        super.init()
        self.info = "HorseMan"
    }
    
    required init(_ model: Warrior) {
        super.init()
        info = model.info
        persNumber = model.persNumber
    }
    
}

class Barracks {
    var infantryNum = 0
    var archerNum = 0
    var horsemanNum = 0
    
    let infantryProrotype = InfantryMan.init()
    let archerPrototype  : Archer = Archer.init()
    let horsemanProrotype = Horseman.init()
    
    func createInfantry() -> InfantryMan {
        let newInfantry : InfantryMan = infantryProrotype.copy() as! InfantryMan
        infantryNum += 1
        newInfantry.persNumber = infantryNum
        return newInfantry
    }
    
    func createArcher() -> Archer {
        let newArcher : Archer = archerPrototype.copy() as! Archer
        archerNum += 1
        newArcher.persNumber = archerNum
        return newArcher
        }
    
    func createHorseman() -> Horseman {
        let newHorseman : Horseman = horsemanProrotype.copy() as! Horseman
        horsemanNum += 1
        newHorseman.persNumber = horsemanNum
        return newHorseman
    }
}

let barracks = Barracks.init()

var army : Array <Warrior> = []

army.append(barracks.createInfantry())
army.append(barracks.createArcher())
army.append(barracks.createHorseman())
army.append(barracks.createArcher())
army.append(barracks.createInfantry())

for w  in army {
    w.printInfo()
}


