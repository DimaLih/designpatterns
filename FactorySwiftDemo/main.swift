//
//  main.swift
//  FactorySwiftDemo
//
//  Created by Владимир Водолазкий on 22.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class Warrior {
    func info() -> String {
        return "Anbstract Warrior";
    }
}


class Infantry  : Warrior {
    override func info() -> String {
        return "Infantry"
    }
}

class Archer  : Warrior {
    override func info() -> String {
        return "Archer"
    }
}

class Horseman : Warrior {
    override func info() -> String {
        return "Horseman"
    }
}

// Factory classes

class Factory {
    func createWarrior() -> Warrior {
        return Warrior.init()
    }
}

class InfantryFactory : Factory {
    override func createWarrior() -> Warrior {
        return Infantry.init()
    }
}

class ArcheryFactory : Factory {
    override func createWarrior() -> Warrior {
        return Archer.init()
    }
}

class HorsemanFactory : Factory {
    override func createWarrior() -> Warrior {
        return Horseman.init()
    }
}

// Main program

    let inFactory = InfantryFactory.init()
    let arFactory = ArcheryFactory.init()
    let hoFactory = HorsemanFactory.init()

    var army : Array <Warrior> = []

    army.append(inFactory.createWarrior())
    army.append(arFactory.createWarrior())
    army.append(hoFactory.createWarrior())

    for unit in army {
        print(unit.info())
    }


