//
//  main.m
//  FrontControllerDemo
//
//  Created by Водолазкий В.В. on 07.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeView : NSObject
- (void) show;
@end

@implementation HomeView

- (void) show {
	NSLog(@"Displaying Home Page");
}

@end

@interface StudentView : NSObject
- (void) show;
@end

@implementation StudentView

- (void) show {
	NSLog(@"Displaying Student page");
}

@end

@interface Dispatcher : NSObject {
	HomeView *homeView;
	StudentView *studentView;
}

- (void) dispatch:(NSString *)aKey;

@end

@implementation Dispatcher

- (instancetype) init {
	if (self = [super init]) {
		homeView = [[HomeView alloc] init];
		studentView = [[StudentView alloc] init];
	}
	return self;
}

- (void) dispatch:(NSString *)aKey {
	if ([aKey isEqualToString:@"STUDENT"]) {
		[studentView show];
	} else {
		[homeView show];
	}
}

@end

@interface FrontController : NSObject {
	Dispatcher *dispatcher;
}

@end

@implementation FrontController

- (instancetype) init {
	if (self = [super init]) {
		dispatcher = [[Dispatcher alloc] init];
	}
	return self;
}

- (BOOL) isAuthernticated {
	NSLog(@"User is authenticated successfully");
	return YES;
}

- (void) trackRequest:(NSString *)request {
	NSLog(@"Page requested - %@", request);
}

- (void) dispatchRequest:(NSString *)aString {
	[self trackRequest:aString];
	if ([self isAuthernticated]) {
		[dispatcher dispatch:aString];
	}
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		FrontController *fc = [[FrontController alloc] init];
		[fc dispatchRequest:@"HOME"];
		[fc dispatchRequest:@"STUDENT"];
	
	}
    return 0;
}
