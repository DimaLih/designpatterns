//
//  main.swift
//  ProxySwiftDemo
//
//  Created by Водолазкий В.В. on 11.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
// All rights reserved.
//

import Foundation

class Book {
	var author : String = ""
	var title  : String = ""
	
	convenience init(author : String, title : String) {
		self.init()
		self.author = author
		self.title = title
	}
	
	func showBook() {
		print("Author:", self.author, "Title :", self.title)
	}

	func isEqual(other : Book) -> Bool {
		if (self.author == other.author &&
			self.title == other.title) {
			return true
		}
		return false
	}
}

class BookList {
	var bookList  = [Book]()
	
	func bookCount() -> Int {
		return bookList.count
	}
	
	func addBook(book : Book) {
		bookList.append(book)
	}
	
	func getBook(bookNum : Int) -> Book? {
		if (bookNum >= 0 && bookNum < self.bookCount()) {
			return bookList[bookNum]
		}
		return nil
	}
	
	func removeBook(book: Book) {
		for i in 0 ... self.bookCount() {
			let b = bookList[i]
			if (b.isEqual(other: book)) {
				bookList.remove(at: i)
				return
			}
		}
	}
}


class ProxyBookList {
	// do not initialize on init() call
	var bookList : BookList? = nil
	
	func makeBookList() {
		bookList = BookList.init()
	}
	
	func bookCount() -> Int {
		if (bookList == nil) {
			self.makeBookList()
		}
		return bookList!.bookCount()
	}
	
	func addBook(book : Book) {
		if (bookList == nil) {
			self.makeBookList()
		}
		bookList!.addBook(book: book)
	}
	
	func getBook(bookNum : Int) -> Book? {
		if (bookList == nil) {
			return nil
		}
		return bookList?.getBook(bookNum: bookNum)
	}
	
	func removeBook(book : Book) {
		if (bookList != nil) {
			bookList!.removeBook(book: book)
		}
	}
	
}

let pbl = ProxyBookList.init()
let b1 = Book.init(author: "Terry Pratchet", title: "The Colour of Magic")

pbl.addBook(book: b1)

print("Amount of books - ",pbl.bookCount())

let b2 = pbl.getBook(bookNum: 0)
b2?.showBook()

pbl.removeBook(book: b2!)
print("Amount of books - ",pbl.bookCount())





