//
//  main.swift
//  FrontControllerSwiftDemo
//
//  Created by Водолазкий В.В. on 07.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//
import Foundation

class HomeView {
	func show() {
		print("Displaying Home page")
	}
}

class StudentsView {
	func show() {
		print("Displaying Students page");
	}
}

class Dispatcher {
	let homePage = HomeView.init()
	let studentPage = StudentsView.init()
	
	func dispatch(key : String) {
		if (key == "STUDENT") {
			studentPage.show()
		} else {
			homePage.show()
		}
	}
}

class FrontController {
	let dispatcher = Dispatcher.init()
	
	func isAuthenticated() -> Bool {
		print("User authenticated successfully")
		return true
	}
	
	func trackRequest(request : String) {
		print("Page requested",request);
	}
	
	func dispatchRequest(request : String) {
		self.trackRequest(request: request)
		if (self.isAuthenticated()) {
			dispatcher.dispatch(key: request)
		}
	}
}

let frc = FrontController.init()
frc.dispatchRequest(request: "HOME")
frc.dispatchRequest(request: "STUDENT")

