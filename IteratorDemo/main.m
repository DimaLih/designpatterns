//
//  main.m
//  IteratorDemo
//
//  Created by Водолазкий В.В. on 10.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Book : NSObject
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *author;

-(instancetype) initWithTitle:(NSString *)title andAuthor:(NSString *)author;
- (BOOL) theSameAsBook:(Book *) anotherBook;
@end

@implementation Book

- (instancetype) initWithTitle:(NSString *)title andAuthor :(NSString *)author
{
	if (self = [super init]) {
		self.author = author;
		self.title = title;
	}
	return self;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@: %@ by %@",
			[self class], self.title, self.author];
}

- (BOOL) theSameAsBook:(Book *)anotherBook
{
	return ([self.author isEqualToString:anotherBook.author] &&
			[self.title isEqualToString:anotherBook.title]);
}

@end

@interface BookList : NSObject {
	NSMutableArray <Book *> *list;
}
@property (nonatomic, readonly) NSInteger bookCount;

- (Book *)getBook:(NSInteger) bookNumber;
- (NSInteger) addBook:(Book *)newBook;
- (NSInteger) removeBook:(Book *)bookToRemove;

@end

@implementation BookList

- (instancetype) init
{
	if (self = [super init]) {
		list = [NSMutableArray new];
	}
	return self;
}

- (NSInteger) bookCount
{
	return list.count;
}

- (Book *) getBook:(NSInteger)bookNumber
{
	if (bookNumber < 0 || bookNumber >= list.count) {
		return nil;
	}
	return list[bookNumber];
}

- (NSInteger) addBook:(Book *)newBook
{
	if (newBook && [newBook isKindOfClass:[Book class]]) {
		[list addObject:newBook];
	}
	return self.bookCount;
}

- (NSInteger) removeBook:(Book *)bookToRemove
{
	if (bookToRemove && [bookToRemove isKindOfClass:[Book class]]) {
		for (NSInteger i = 0; i < list.count; i++) {
			Book *b = list[i];
			if ([b theSameAsBook:bookToRemove]) {
				[list removeObject:b];
				break;
			}
		}
	}
	return self.bookCount;
}

@end


@interface BookIterator : NSObject

- (Book *) getCurrentBook;
- (Book *) getNextBook;
- (BOOL) hasNectBook;

@end

@interface BookIterator()

@property (nonatomic, retain) BookList *bookList;
@property (nonatomic, readwrite) NSInteger cBook;

- (instancetype) initWithList:(BookList *)list;

@end

@implementation BookIterator

- (instancetype) initWithList:(BookList *)list
{
	if (self = [super init]) {
		self.bookList = list;
		self.cBook = 0;
	}
	return self;
}

- (Book *) getCurrentBook
{
	return [self.bookList getBook:self.cBook];
}

- (Book *) getNextBook
{
	if ([self hasNectBook]) {
		self.cBook++;
		return [self getCurrentBook];
	}
	return nil;
}

- (BOOL) hasNectBook
{
	return (self.cBook >= 0 && self.cBook < self.bookList.bookCount - 1);
}

@end

@interface BookReverseIterator : BookIterator
@end

@implementation BookReverseIterator

- (instancetype) initWithList:(BookList *)list
{
	if (self = [super initWithList:list]) {
		self.cBook = list.bookCount-1;
	}
	return self;
}


- (BOOL) hasNectBook
{
	return (self.cBook > 0);
}

- (Book *) getNextBook
{
	if ([self hasNectBook]) {
		self.cBook--;
		return [self getCurrentBook];
	}
	return nil;
}

@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"BEGIN TESTING ITERATOR PATTERN");
		NSLog(@"");
		
		
		Book *firstBook = [[Book alloc] initWithTitle:@"Core PHP Programming, Third Edition"
											andAuthor:@"Atkinson and Suraski"];
		Book *secondBook = [[Book alloc] initWithTitle:@"PHP Bible"
											andAuthor:@"Converse and Park"];
		Book *thirdBook = [[Book alloc] initWithTitle:@"Design Patterns"
											 andAuthor:@"Gamma, Helm, Johnson, and Vlissides"];

		BookList *books = [[BookList alloc] init];
		for (Book *b in @[firstBook, secondBook, thirdBook]) {
			[books addBook:b];
		}
		
		NSLog(@"Testing the Iterator");
		
		BookIterator *bit = [[BookIterator alloc] initWithList:books];
		
		NSLog(@"getting next book with iterator :");

		while ([bit hasNectBook]) {
			Book *book = [bit getNextBook];
			NSLog(@">> %@",book);
		}
		
		Book *bb = [bit getCurrentBook];
		NSLog(@"getting current book with iterator :");
		NSLog(@">>> %@",bb);
		
		
		NSLog(@"Testing the Reverse Iterator");
		
		BookReverseIterator *rit = [[BookReverseIterator alloc] initWithList:books];
		while ([rit hasNectBook]) {
			Book *book = [rit getNextBook];
			NSLog(@">> %@",book);
		}
		
		bb = [rit getCurrentBook];
		NSLog(@"getting current book with Reverse iterator :");
		NSLog(@">>> %@",bb);
		
		NSLog(@"END TESTING ITERATOR PATTERN");
		
		
	}
	return 0;
}
