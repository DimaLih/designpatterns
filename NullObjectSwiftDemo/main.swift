//
//  main.swift
//  NullObjectSwiftDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class Animal {
	func makeSound()->String {
		print ("Abstract method! replace me!")
		return ""
	}
}

class Dog : Animal {
	override func makeSound() -> String {
		return "Goow!"
	}
}

class Cat : Animal {
	override func makeSound() -> String {
		return "Meow"
	}
}

class BullObject : Animal {
	override func makeSound() -> String {
		return ""
	}
}

