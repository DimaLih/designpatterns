//
//  main.m
//  ChainOfResponsibilityDemo
//
//  Created by Водолазкий В.В. on 16.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// Common prototype
//
@interface AbstractBookTopic : NSObject

- (void) setTitle:(NSString *)title;
- (NSString *) getTitle;
- (NSString * *) getTopic;
@end

@implementation AbstractBookTopic

@end


@interface BookTopic : AbstractBookTopic

- (instancetype) initWithTopic:(NSString *)topic;
@end


@interface BookTopic ()
// private definitions
@property (nonatomic, retain) NSString *localTopic;
@property (nonatomic, retain) NSString *topicTitle;
@end

@implementation BookTopic

- (instancetype) initWithTopic:(NSString *)topic
{
	if (self = [super init]) {
		self.topicTitle = nil;	// not necessary, just to be sure
		self.localTopic = topic;
	}
	return self;
}

// overwrite abstract methods

- (void) setTitle:(NSString *)title
{
	self.topicTitle = title;
}

- (NSString *) getTitle
{
	if (self.topicTitle) {
		return self.topicTitle;
	} else {
		return @"No topic title is defined yet";
	}
}

- (NSString *) getTopic
{
	return self.localTopic;
}

@end

@interface BookSubTopic : AbstractBookTopic

- (instancetype) initWithTopic:(NSString *)t andParentTopic:(BookTopic *)pt;

- (BookTopic *) getParentTopic;
@end

// private properties
@interface BookSubTopic ()
@property (nonatomic, retain) NSString *localTopic;
@property (nonatomic, retain) BookTopic *parentTopic;
@property (nonatomic, retain) NSString *topicTitle;
@end


@implementation BookSubTopic

- (instancetype) initWithTopic:(NSString *)t andParentTopic:(BookTopic *)pt
{
	if (self = [super init]) {
		self.localTopic = t;
		self.parentTopic = pt;
		self.topicTitle = nil;
	}
	return self;
}

- (NSString *) getTopic
{
	return self.localTopic;
}

- (BookTopic *) getParentTopic
{
	return self.parentTopic;
}

- (NSString *) getTitle
{
	if (self.topicTitle) {
		return self.topicTitle;
	} else {
		// Chain responsibility to parent...
		return [self.parentTopic getTitle];
	}
}

- (void) setTitle:(NSString *)title
{
	self.topicTitle = title;
}


@end



int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"BEGIN TESTING CHAIN OF RESPONSIBILITY PATTERN");
		NSLog(@"");
		
		BookTopic *main = [[BookTopic alloc] initWithTopic:@"PHP 5"];
		NSLog(@"bookTopic before title is set:");
		NSLog(@"topic: %@", [main getTopic] );
		NSLog(@"title: %@",[main getTitle]);
		NSLog(@"");
		
		[main setTitle:@"PHP 5 Recipes by Babin, Good, Kroman, and Stephens"];
		NSLog(@"bookTopic after title is set: ");
		NSLog(@"topic: %@", [main getTopic] );
		NSLog(@"title: %@",[main getTitle]);
		NSLog(@"");
		
		BookSubTopic *st = [[BookSubTopic alloc] initWithTopic:@"PHP 5 Patterns" andParentTopic:main];
		
		NSLog(@"bookSubTopic before title is set: ");
		NSLog(@"topic: %@", [st getTopic] );
		NSLog(@"title: %@",[st getTitle]);
		NSLog(@"");
		
		[st setTitle:@"PHP 5 Objects Patterns and Practice by Zandstra"];
		NSLog(@"bookSubTopic after title is set: ");
		NSLog(@"topic: %@", [st getTopic] );
		NSLog(@"title: %@",[st getTitle]);
		NSLog(@"");
		
		BookSubTopic *st2 = [[BookSubTopic alloc] initWithTopic:@"PHP 5 Patterns for Cats" andParentTopic:main];

		NSLog(@"bookSubSubTopic with no title set: ");
		NSLog(@"topic: %@", [st2 getTopic] );
		NSLog(@"title: %@",[st2 getTitle]);
		NSLog(@"");
		
		[st2 setTitle:nil];
		NSLog(@"bookSubSubTopic with no title for set for bookSubTopic either:");
		NSLog(@"topic: %@", [st2 getTopic] );
		NSLog(@"title: %@",[st2 getTitle]);
		NSLog(@"");
		
		NSLog(@"END TESTING CHAIN OF RESPONSIBILITY PATTERN");
		
	}
    return 0;
}
