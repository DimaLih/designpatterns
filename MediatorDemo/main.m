//
//  main.m
//  MediatorDemo
//
//  Created by Водолазкий В.В. on 10.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


@class BookAuthorColleague;
@class BookTitleColleague;
@class BookColleague;

typedef NS_ENUM(NSInteger, State) {
	StateNonSpecified = 0,
	StateLower,
	StateUpper,
};


@interface BookMediator : NSObject

@property (nonatomic, retain) BookAuthorColleague *authorObject;
@property (nonatomic, retain) BookTitleColleague *titleObject;

- (instancetype) initWithAuthor:(NSString *)author andTitle:(NSString *)title;
- (void) change:(BookColleague *)changingClassIn;

@end

@interface BookColleague : NSObject

@property (nonatomic, retain) BookMediator *mediator;
@property (nonatomic,readwrite) State state;

-(instancetype) initWithMediraor:(BookMediator *)mediator;
- (void) changed:(BookColleague *)changingClassIn;
@end

@implementation BookColleague

- (instancetype) initWithMediraor:(BookMediator *)mediator
{
	if (self = [super init]) {
		self.mediator = mediator;
	}
	return self;
}

- (void) changed:(BookColleague *)changingClassIn
{
	[self.mediator change:changingClassIn];
}

@end

@interface BookAuthorColleague : BookColleague

@property (nonatomic, retain) NSString *author;


- (instancetype) initWithAuthor:(NSString *)author andMediator:(BookMediator *)m;

- (void) setAuthorUpperCase;
- (void) setAuthorLowerCase;

@end

@implementation BookAuthorColleague

- (instancetype) initWithAuthor:(NSString *)author andMediator:(BookMediator *)m
{
	if (self = [super initWithMediraor:m]) {
		self.author = author;
		self.state = StateNonSpecified;
	}
	return self;
}

- (void) setAuthorUpperCase
{
	if (self.state != StateUpper) {
		self.author = [self.author uppercaseString];
		self.state = StateUpper;
		[self.mediator change:self];
	}
}

- (void) setAuthorLowerCase
{
	if (self.state != StateLower) {
		_author = [self.author lowercaseString];
		self.state = StateLower;
		[self.mediator change:self];
	}
}

@end

@interface BookTitleColleague : BookColleague

@property (nonatomic, retain) NSString *title;

- (instancetype) initWithTitle:(NSString *)title andMediator:(BookMediator *)m;

- (void) setTitleUpperCase;
- (void) setTitleLowerCase;

@end

@implementation BookTitleColleague

- (instancetype) initWithTitle:(NSString *)title andMediator:(BookMediator *)m
{
	if (self = [super initWithMediraor:m]) {
		self.title = title;
		self.state = StateNonSpecified;
	}
	return self;
}

- (void) setTitleUpperCase
{
	if (self.state != StateUpper) {
		self.title = [self.title uppercaseString];
		self.state = StateUpper;
		[self.mediator change:self];
		
	}
}

- (void) setTitleLowerCase
{
	if (self.state != StateLower) {
		_title = [self.title lowercaseString];
		self.state = StateLower;
		[self.mediator change:self];
	}
}

@end

@implementation BookMediator

- (instancetype) initWithAuthor:(NSString *)author andTitle:(NSString *)title
{
	if (self = [super init]) {
		self.authorObject = [[BookAuthorColleague alloc] initWithAuthor:author andMediator:self];
		self.titleObject = [[BookTitleColleague alloc] initWithTitle:title andMediator:self];
	}
	return self;
}

- (void) change:(BookColleague *)changingClassIn
{
	if ([changingClassIn isKindOfClass:[BookAuthorColleague class]]) {
		// update title instance
		switch (changingClassIn.state) {
			case StateLower: [self.titleObject setTitleLowerCase]; break;
			case StateUpper: [self.titleObject setTitleUpperCase]; break;
			default: break;
		}
	} else if ([changingClassIn isKindOfClass:[BookTitleColleague class]]) {
		// update author instance
		switch (changingClassIn.state) {
			case StateUpper : [self.authorObject setAuthorUpperCase]; break;
			case StateLower : [self.authorObject setAuthorLowerCase]; break;
			default: break;
		}
	}
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"BEGIN TESTING MEDIATOR PATTERN");
		NSLog(@"");
		
		BookMediator *bm = [[BookMediator alloc] initWithAuthor:@"Gamma, Helm, Johnson, and Vlissides" andTitle:@"Design Patterns"];
		
		BookAuthorColleague *author = bm.authorObject;
		BookTitleColleague *title = bm.titleObject;
		
		
		NSLog(@"Original Author and Title: ");
		NSLog(@"author: %@", author.author);
		NSLog(@"title:  %@", title.title);
		NSLog(@"");
		
		[author setAuthorLowerCase];
		
		NSLog(@"After Author set to Lower Case: ");
		NSLog(@"author: %@", author.author);
		NSLog(@"title:  %@", title.title);
		NSLog(@"");
		
		[title setTitleUpperCase];
		
		NSLog(@"After Title set to Upper Case: ");
		NSLog(@"author: %@", author.author);
		NSLog(@"title:  %@", title.title);
		NSLog(@"");
		
		NSLog(@"END TESTING MEDIATOR PATTERN");
	
	}
	return 0;
}
