//
//  main.m
//  BuilderDemo
//
//  Created by Водолазкий В.В. on 03.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfantryMan : NSObject
@property (nonatomic, readonly) NSString *info;
@end

@implementation InfantryMan

- (NSString *) info
{
	return @"InfantryMan";
}

@end

@interface Horseman : NSObject
@property (nonatomic, readonly) NSString *info;
@end

@implementation Horseman

- (NSString *) info
{
	return @"HorseMan";
}

@end

@interface Archer : NSObject
@property (nonatomic, readonly) NSString *info;
@end

@implementation Archer

- (NSString *) info
{
	return @"Archer";
}

@end

@interface Catapult : NSObject
@property (nonatomic, retain) NSString *info;
@end

@implementation Catapult
- (NSString *) info
{
	return @"Catapult";
}

@end

@interface Elefant : NSObject
@property (nonatomic, retain) NSString *info;
@end

@implementation Elefant
- (NSString *) info
{
	return @"Elefant";
}

@end

#pragma mark -

// Army unties all types of units

@interface Army : NSObject

@property (nonatomic, retain) NSMutableArray  <InfantryMan *> *infantry;
@property (nonatomic, retain) NSMutableArray  <Archer *> *archers;
@property (nonatomic, retain) NSMutableArray <Horseman *> *cavalry;
@property (nonatomic, retain) NSMutableArray <Catapult *> *artillery;
@property (nonatomic, retain) NSMutableArray <Elefant *> *tanks;

- (void) info;

@end

@implementation Army

- (void) info
{
	for (InfantryMan *i in self.infantry) {
		NSLog(@"%@",i.info);
	}
	for (Archer *i in self.archers) {
		NSLog(@"%@",i.info);
	}
	for (Horseman *i in self.cavalry) {
		NSLog(@"%@",i.info);
	}
	for (Catapult *i in self.artillery) {
		NSLog(@"%@",i.info);
	}
	for (Elefant *i in self.tanks) {
		NSLog(@"%@",i.info);
	}
}

@end

// This class defines interface for army building and implements
// its default implementation

@interface ArmyBuilder : NSObject

- (void) createArmy;
- (void) buildInfantryMan;
- (void) buildArcher;
- (void) buildHorseman;
- (void) buildCatapult;
- (void) buildelefant;

@property (nonatomic, readonly) Army *army;

@end

@implementation ArmyBuilder
{
	Army *p;
}

- (id) init
{
	if (self = [super init]) {
		p = [[Army alloc] init];
	}
	return self;
}

- (Army *) army
{
	return p;
}

- (void) createArmy
{
	self.army.infantry = [NSMutableArray new];
	self.army.archers = [NSMutableArray new];
	self.army.cavalry = [NSMutableArray new];
	self.army.artillery = [NSMutableArray new];
	self.army.tanks = [NSMutableArray new];
}


- (void) buildArcher {};
- (void) buildInfantryMan {};
- (void) buildHorseman {};
- (void) buildCatapult {};
- (void) buildelefant {};

@end

//
// Roman army doesn't have elephants
//
@interface RomanArmyBuilder : ArmyBuilder

@end

@implementation RomanArmyBuilder

- (void) buildArcher
{
	[self.army.archers addObject:[Archer new]];
}

- (void) buildInfantryMan
{
	[self.army.infantry addObject:[InfantryMan new]];
}

- (void) buildCatapult
{
	[self.army.artillery addObject:[Catapult new]];
}

- (void) buildHorseman
{
	[self.army.cavalry addObject:[Horseman new]];
}

@end

//
// Carthage army has elephants but diesn't posses catapults'
//
@interface CarthageArmyBuilder : ArmyBuilder

@end

@implementation CarthageArmyBuilder

- (void) buildArcher
{
	[self.army.archers addObject:[Archer new]];
}

- (void) buildInfantryMan
{
	[self.army.infantry addObject:[InfantryMan new]];
}

- (void) buildelefant
{
	[self.army.tanks addObject:[Elefant new]];
}

- (void) buildHorseman
{
	[self.army.cavalry addObject:[Horseman new]];
}

@end

#pragma mark -

// This class defines algorithm to build the whole army regardless its specific


@interface Director : NSObject

- (Army *) createArmy:(ArmyBuilder *)builder;

@end

//
// Creates army with content depending on builder used
//
@implementation Director

- (Army *) createArmy:(ArmyBuilder *)builder
{
	[builder createArmy];
	[builder buildelefant];
	[builder buildHorseman];
	[builder buildCatapult];
	[builder buildArcher];
	[builder buildInfantryMan];
	return builder.army;
}

@end

#pragma mark -

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		Director *dir = [[Director alloc] init];
		RomanArmyBuilder *ra = [[RomanArmyBuilder alloc] init];
		CarthageArmyBuilder *ca = [[CarthageArmyBuilder alloc] init];
		
		Army *romanArmy = [dir createArmy:ra];
		Army *carthageArmy = [dir createArmy:ca];
		
		NSLog(@"Roman Army:");
		[romanArmy info];
		NSLog(@"Carthage Army:");
		[carthageArmy info];
	}
    return 0;
}
