#!/bin/sh

#  CopyExamples.sh
#  DesignPatterns
#
#  Created by admin on 24.02.17.
#  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
#

DST_DIR=${PROJECT_DIR}/DesignPatterns/Examples

cp ${PROJECT_DIR}/AbdtractFactoryDemo/main.m ${DST_DIR}/AbstractFactory-objc
cp ${PROJECT_DIR}/AbstractFactorySwiftDemo/main.swift ${DST_DIR}/AbstractFactory-swift


for PRG in  Builder ObjectPool Factory LazyInit \
			Prototype Singleton Multiton Adapter Bridge Composite Decorator Facade \
			FrontController FlyWeight Proxy ChainOfResponsibility Command Interpreter \
			Iterator Mediator Memento NullObject Observer State Strategy \
			TemplateMethod Visitor
do
	cp ${PROJECT_DIR}/${PRG}Demo/main.m ${DST_DIR}/${PRG}-objc
	cp ${PROJECT_DIR}/${PRG}SwiftDemo/main.swift ${DST_DIR}/${PRG}-swift
done

