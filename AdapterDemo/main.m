//
//  main.m
//  AdapterDemo
//
//  Created by Водолазкий В.В. on 27.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OldStyleSensor : NSObject
@property (nonatomic, readonly) float tempinFahrenheit;
@end

@implementation OldStyleSensor : NSObject

- (float) tempinFahrenheit
{
    // Absolutely randomly chosen value
    float t = 32.0;
    return t;
}

@end


@interface Sensor : NSObject

@property (nonatomic, readonly) float temperature;

@end

@implementation Sensor

- (float) temperature
{
    return 0;
}

@end

@interface Adapter : Sensor {

    OldStyleSensor *fahrenheitSensor;
}

@end

@implementation Adapter

- (instancetype) init
{
    if (self = [super init]) {
        fahrenheitSensor = [[OldStyleSensor alloc] init];
    }
    return self;
}

- (float) temperature
{
    return (fahrenheitSensor.tempinFahrenheit-32.0)*5.0/9.0;
}

@end



int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Sensor *s = [[Adapter alloc] init];
        
        NSLog(@"Temp in Celsius - %.0f", s.temperature);
        
    }
    return 0;
}
