//
//  main.m
//  StateDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 
 1. Create a "wrapper" class that models the state machine
 2. The wrapper class contains an array of state concrete objects
 3. The wrapper class contains an index to its "current" state
 4. The wrapper class contains a state transition table
 5. All client requests are simply delegated to the current state object
 6. Create a state base class that makes the concrete states interchangeable
 7. The State base class specifies default behavior for all messages
 8. The State derived classes only override the messages they need to
 
 */

// 6. Create a state base class that makes the concrete states interchangeable
// 7. The State base class specifies default behavior
@interface State : NSObject

- (void) on;
- (void) off;
- (void) ack;

@end

@implementation State

- (void) on {
	NSLog(@"Error!");
}

- (void) off {
	NSLog(@"Error!");
}

- (void) ack {
	NSLog(@"Error!");
}
@end


@interface A : State
@end

@implementation A

- (void) on {
	NSLog(@"A + on  = C");
}

- (void) off {
	NSLog(@"A + off = B");
}

- (void) ack {
	NSLog(@"A + ack = A");
}

@end

@interface B : State
@end

@implementation B

- (void) on {
	NSLog(@"B + on  = A");
}

- (void) off {
	NSLog(@"B + off = C");
}

@end


@interface C : State
@end

@implementation C

- (void) on {
	// 8. The State derived classes only override the messages they need to
	NSLog(@"C + on  = B");
}


@end


// 1. Create a "wrapper" class that models the state machine


@interface FSM : NSObject
{
	NSMutableArray <State *> *states;
	NSArray <NSArray *> *transitions;
	NSInteger current;
}

- (void) on;
- (void) off;
- (void) ack;

@end


@implementation FSM

- (instancetype) init
{
	if (self = [super init]) {
		states = [NSMutableArray new];
		// 2. states
		[states addObject:[A new]];
		[states addObject:[B new]];
		[states addObject:[C new]];
		// 4. transitions
		transitions = @[@[@2, @1, @0], @[@0, @2, @1], @[@1, @2, @2]];
		// 3. current
		current = 0;
	}
	return self;
}

- (void) next:(NSInteger) msg
{
	NSArray *tmp = transitions[current];
	current = [tmp[msg] integerValue];
}

// 5. All client requests are simply delegated to the current state object

- (void) on
{
	[states[current] on];
	[self next:0];
}

- (void) off
{
	[states[current] off];
	[self next:1];
}

- (void) ack
{
	[states[current] ack];
	[self next:2];
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		FSM *fsm = [[FSM alloc] init];
		NSInteger msgs[8] = {2, 1, 2, 1, 0, 2, 0, 0};
		
		for (NSInteger i = 0; i < 8; i++) {
			NSInteger msg = msgs[i];
			
			if (msg == 0) {
				[fsm on];
			} else if (msg == 1) {
				[fsm off];
			} else if (msg == 2) {
				[fsm ack];
			}
		}
	}
}
