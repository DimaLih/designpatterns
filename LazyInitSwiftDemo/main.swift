//
//  main.swift
//  LazyInitSwiftDemo
//
//  Created by Водолазкий В.В. on 24.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class Beer {
    var beerName : String = ""
    
    static var beerTypes : NSMutableDictionary = [:]
    
    static func printBeerTypes() {
        for beer in Beer.beerTypes {
            print((beer.value as! Beer).beerName);
        }
        print("")
    }
    
    static func getBeerByType(bType : String) -> Beer {
        let oldBeer = Beer.beerTypes[bType]
        if ((oldBeer) != nil) {
            return oldBeer as! Beer
        }
        // not found, create new entry
        let newBeer = Beer.init()
        newBeer.beerName = bType
        Beer.beerTypes[bType] = newBeer
        return newBeer
    }
    
}

    var b = Beer.getBeerByType(bType: "Gambrinus")
    Beer.printBeerTypes()

    b =  Beer.getBeerByType(bType: "Pilsner Urquel")
    Beer.printBeerTypes()

    b = Beer.getBeerByType(bType: "Gambrinus")
    Beer.printBeerTypes()
