//
//  main.m
//  MultitonDemo
//
//  Created by Водолазкий В.В. on 26.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface  InfantryMan : NSObject
@end

@implementation InfantryMan

- (NSString *) description
{
        return @"InfantryMan";
}

@end

@interface Archer : NSObject
@end

@implementation Archer

- (NSString *) description
{
    return @"Archer";
}

@end

@interface Horseman : NSObject
@end

@implementation Horseman

- (NSString *) description
{
    return @"Horseman";
}
@end

@interface Multiton : NSObject

+ (id) sharedInstanceForClassName:(NSString *)aName;

@end

@implementation Multiton

+ (id) sharedInstanceForClassName:(NSString *)aName
{
    static NSMutableDictionary *oD = nil;
    if (!oD) {
        oD = [NSMutableDictionary new];
    }
    id obj = oD[aName];
    if (!obj) {
        Class cl = NSClassFromString(aName);
        obj = [[cl alloc] init];
        oD[aName] = obj;
    }
    return obj;
}

@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {
    
        InfantryMan *infMan = [Multiton sharedInstanceForClassName:@"InfantryMan"];
        Archer *archer = [Multiton sharedInstanceForClassName:@"Archer"];
        Horseman *horseman = [Multiton sharedInstanceForClassName:@"Horseman"];
        
        NSLog(@"%@", infMan);
        NSLog(@"%@", archer);
        NSLog(@"%@", horseman);
        
    
    }
    return 0;
}
