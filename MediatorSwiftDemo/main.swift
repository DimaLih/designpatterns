//
//  main.swift
//  MediatorSwiftDemo
//
//  Created by Водолазкий В.В. on 10.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

enum State {
	case Unknown
	case Lowercase
	case Uppercase
}

class BookMediator {
	var titleObject : TitleColleague? = nil
	var authorObject: AuthorColleague? = nil
	
	convenience init(author : String, title: String) {
		self.init()
		self.titleObject = TitleColleague.init(title: title, mediator: self)
		self.authorObject = AuthorColleague.init(author: author, mediator: self)
	}
	
	func changed(colleague: BookColleague) {
		if colleague is AuthorColleague {
			switch colleague.state {
			case State.Uppercase: self.titleObject?.setTitleUpperCase()
			case State.Lowercase: self.titleObject?.setTitleLowerCase()
			default: break
			}
		} else if colleague is TitleColleague {
			switch colleague.state {
			case State.Uppercase: self.authorObject?.setAuthorUpperCase()
			case State.Lowercase: self.authorObject?.setAuthorLowerCase()
			default: break
			}
		}
	}
}

class BookColleague {
	var mediator : BookMediator? = nil;
	var state : State = State.Unknown

	convenience init(mediator : BookMediator) {
		self.init()
		self.mediator = mediator
	}
	
	func change() {
		self.mediator?.changed(colleague: self)
	}
	
}

class AuthorColleague : BookColleague {
	var authorString : String = ""
	
	convenience init(author : String, mediator : BookMediator) {
		self.init(mediator: mediator)
		self.authorString = author
	}
	
	func setAuthorUpperCase() {
		if (self.state != State.Uppercase) {
			self.state = State.Uppercase
			self.authorString = self.authorString.uppercased()
			self.change()
		}
	}
	
	func setAuthorLowerCase() {
		if (self.state != State.Lowercase) {
			self.state = State.Lowercase
			self.authorString = self.authorString.lowercased()
			self.change()
		}
	}
	func author() -> String {
		return authorString
	}
}

class TitleColleague : BookColleague {
	var titleString : String = ""
	
	convenience init(title : String, mediator : BookMediator) {
		self.init(mediator: mediator)
		self.titleString = title
	}
	
	func setTitleUpperCase() {
		if (self.state != State.Uppercase) {
			self.state = State.Uppercase
			self.titleString = self.titleString.uppercased()
			self.change()
		}
	}
	
	func setTitleLowerCase() {
		if (self.state != State.Lowercase) {
			self.state = State.Lowercase
			self.titleString = self.titleString.lowercased()
			self.change()
		}
	}
	func title() -> String {
		return titleString
	}
}

print("BEGIN TESTING MEDIATOR PATTERN")
print("")

let bm = BookMediator.init(author: "Gamma, Helm, Johnson, and Vlissides", title: "Design Patterns")


let bma = bm.authorObject
let bmt = bm.titleObject


print("Original Author and Title: ")
print("author: ", bma?.author() ?? "None given");
print("title: ", bmt?.title() ?? "None given");
print("")

bma?.setAuthorLowerCase()

print("After Author set to Lower Case: ")
print("author: ", bma?.author() ?? "None given");
print("title: ", bmt?.title() ?? "None given");
print("")

bmt?.setTitleUpperCase()

print("After Title set to Upper Case: ")
print("author: ", bma?.author() ?? "None given");
print("title: ", bmt?.title() ?? "None given");
print("")

print("END TESTING MEDIATOR PATTERN")



