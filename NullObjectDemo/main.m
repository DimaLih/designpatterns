//
//  main.m
//  NullObjectDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject

- (NSString *) makeSound;

@end

@implementation Animal
@end


@interface Dog : Animal
@end

@implementation Dog

- (NSString *) makeSound {
	return @"Gow!";
}

@end

@interface Cat : Animal
@end

@implementation Cat

- (NSString *) makeSound {
	return @"Meow";
}

@end


@interface NullObject : Animal
@end

@implementation NullObject

- (NSString *) makeSound {
	return @"";
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {

		NSArray <Animal *> * objs = @[[Dog new], [Cat new], [NullObject new]];
	
		for (Animal *a in objs) {
			NSLog(@"%@ -> %@", [a class], [a makeSound] );
		}
		
		
	}
	return 0;
}
