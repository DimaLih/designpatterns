//
//  main.m
//  SingletonDemo
//
//  Created by Водолазкий В.В. on 25.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Preferences : NSObject
{
    NSUserDefaults *prefs;
}

+ (Preferences *) sharedPreferences;

@property (nonatomic, retain) NSString *heroName;

@end

NSString * const VVVheroName = @"HeroName";

@implementation Preferences

+ (Preferences *) sharedPreferences
{
    static Preferences *_prefs = nil;
    if (!_prefs) {
        _prefs = [[Preferences alloc] init];
    }
    return _prefs;
}

- (instancetype) init
{
    if (self = [super init]) {
        prefs = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

+ (void) initialize
{
    NSMutableDictionary  *defaultValues = [NSMutableDictionary dictionary];
    //
    // set up intial values for user preferences
    //
    [defaultValues setObject:@"New PLayer" forKey:VVVheroName];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
}

- (NSString *) heroName
{
    return [prefs objectForKey:VVVheroName];
}

- (void) setHeroName:(NSString *)heroName
{
    [prefs setObject:heroName forKey:VVVheroName];
}

@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Preferences *prefs = [Preferences sharedPreferences];
        
        NSLog(@"Default hero - %@", prefs.heroName);
        
        prefs.heroName = @"Napoleon";
        
        NSLog(@"Custom name - %@",prefs.heroName);
        
    }
    return 0;
}
