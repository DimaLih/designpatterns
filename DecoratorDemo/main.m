//
//  main.m
//  DecoratorDemo
//
//  Created by Водолазкий В.В. on 04.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

// common "low denominator"
@protocol Widget <NSObject>

- (void) draw;

@end

@interface TextField : NSObject <Widget> {
	NSInteger width, height;
}

- (instancetype) initWithWidth:(NSInteger)w andHeight:(NSInteger)h;

@end

@implementation TextField

- (instancetype) initWithWidth:(NSInteger)w andHeight:(NSInteger)h {
	if (self = [super init]) {
		width = w;
		height = h;
	}
	return self;
}

- (void) draw {
	NSLog(@"Textfield: width=%ld height=%ld", (long)width, (long)height);
}

@end

// Abstract closs for Decoration

@interface Decorator : NSObject <Widget> {
	NSObject <Widget> *widget;	// should support Widget protocol!
}

- (instancetype) initWithWidget:(NSObject <Widget> *)w;

- (void) draw;

@end

@implementation Decorator

- (instancetype) initWithWidget:(NSObject <Widget> *)w {
	if ([w conformsToProtocol:@protocol(Widget)]) {
		if (self = [super init]) {
			widget = w;
		}
		return self;
	}
	return nil;
}

- (void) draw {
	[widget draw];
}

@end

@interface BorderDecorator : Decorator
@end

@implementation BorderDecorator

- (void) draw {
	[super draw];
	NSLog(@"   BorderDecorator");
}
@end

@interface ScrollDecorator : Decorator
@end

@implementation ScrollDecorator

- (void) draw {
	[super draw];
	NSLog(@"      ScrollDecorator");
}
@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		TextField *tf = [[TextField alloc] initWithWidth:80 andHeight:24];
		Decorator *d1 = [[BorderDecorator alloc] initWithWidget:tf];
		Decorator *d2 = [[ScrollDecorator alloc] initWithWidget:d1];
		
		[d2 draw];
		
	}
    return 0;
}
