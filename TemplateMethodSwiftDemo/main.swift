//
//  main.swift
//  TemplateMethodSwiftDemo
//
//  Created by Водолазкий В.В. on 13.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class Base {
	func a() -> String {
		return "a"
	}
	func c() -> String {
		return "c"
	}
	func e() -> String {
		return "e"
	}
	func ph1() -> String {
		return ""
	}
	func ph2() -> String {
		return ""
	}
	func execute() {
		print( self.a(), self.ph1(),self.c(),
		      self.ph2(), self.e())
	}
}

class One : Base {
	
	override func ph1() -> String {
		return "b"
	}
	override func ph2() -> String {
		return "d"
	}
}

class Two : Base {
	override func ph1() -> String {
		return "2"
	}
	override func ph2() -> String {
		return "4"
	}

}

let one = One.init()
one.execute()
let two = Two.init()
two.execute()


