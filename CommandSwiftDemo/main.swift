//
//  main.swift
//  CommandSwiftDemo
//
//  Created by Водолазкий В.В. on 06.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation


class Game {
	func create() {
		print("Create new game")
	}
	func open(fileName : String) {
		print("Load saved game from file ",fileName)
	}
	func save(fileName : String) {
		print("Save game in the file ",fileName)
	}
	func makeMove(move : String) {
		print("Make move ",move)
	}
}

// Base class
class Command
{
	var pgame : Game? = nil
	
	func execute() {}
	
	init( game : Game) {
		pgame = game
	}
	
}

class CreateGameCommand : Command
{
	override func execute() {
		pgame?.create()
	}
}

class OpenGameCommand : Command
{
	override func execute() {
		let fileName = "SAVE_FILE"
		pgame?.open(fileName: fileName)
	}
};

class SaveGameCommand :  Command
{
	override func  execute( ) {
		let fileName = "SAVE_FILE"
		pgame?.save(fileName: fileName)
	}
}

class MakeMoveCommand : Command
{
	var moveCommand : String = ""
	
	convenience init(game: Game, move : String) {
		self.init(game: game)
		moveCommand = move
	}
	
	override func execute() {
		// Save current state of the game
		pgame?.save( fileName :"TEMP_FILE")
		pgame?.makeMove(move: moveCommand)
	}
}

class UndoCommand : Command
{
	override func execute() {
		// Restore game from the temporary file
		pgame?.open(fileName : "TEMP_FILE")
	}
}

// Init Game engine
let game = Game.init()

// Array where all command will be stored
var movesArray :[Command] = []

// Create new game
let createNewGame = CreateGameCommand.init(game: game)
movesArray.append(createNewGame)

let move1 = MakeMoveCommand.init(game: game, move: "e2-e4")
movesArray.append(move1)
let move2 = MakeMoveCommand.init(game: game, move: "e7-e5")
movesArray.append(move2)
// undo last move
let undo = UndoCommand.init(game: game)
movesArray.append(undo)
// and save game - need to thibk about move...
let save = SaveGameCommand.init(game: game)
movesArray.append(save)


// now we run the whole sequence
for i in 0 ..< movesArray.count  {
	let command = movesArray[i]
	command.execute()
}


