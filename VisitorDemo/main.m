//
//  main.m
//  VisitorDemo
//
//  Created by Водолазкий В.В. on 13.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


@class BookVisitee;
@class SoftwareVisitee;

@interface Visitor : NSObject

@property (nonatomic, retain) NSString *note;

- (void) visitBook:(BookVisitee *) book;
- (void) visitSoftware:(SoftwareVisitee *)software;

@end

@implementation Visitor
@end

@interface Visitee : NSObject
- (void) acceptVisitor:(Visitor *)visitor;
@end

@implementation Visitee
- (void) acceptVisitor:(Visitor *)visitor {}
@end


@interface BookVisitee : Visitee

@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *title;

- (instancetype) initWithTitle:(NSString *)title andAuthor:(NSString *)author;

@end

@implementation BookVisitee

- (instancetype) initWithTitle:(NSString *)title andAuthor:(NSString *)author
{
	if (self = [super init]) {
		self.title = title;
		self.author = author;
	}
	return self;
}

- (void) acceptVisitor:(Visitor *)visitor {
	[visitor visitBook:self];
}

@end

@interface SoftwareVisitee : Visitee

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *company;
@property (nonatomic, retain) NSString *companyURL;

- (instancetype) initWithTitle:(NSString *)title
				   companyName:(NSString *)name
						andURL:(NSString *)url;

@end

@implementation SoftwareVisitee

- (instancetype) initWithTitle:(NSString *)title
				   companyName:(NSString *)name
						andURL:(NSString *)url
{
	if (self = [super init]) {
		self.title = title;
		self.company = name;
		self.companyURL = url;
	}
	return self;
}

- (void) acceptVisitor:(Visitor *)visitor
{
	[visitor visitSoftware:self];
}

@end

@interface PlainDescriptorVisitor : Visitor
@end


@implementation PlainDescriptorVisitor

- (void) visitBook:(BookVisitee *)book
{
	self.note = [NSString stringWithFormat:@"%@ written by %@",
				 book.title, book.author];
}

- (void) visitSoftware:(SoftwareVisitee *)software
{
	self.note = [NSString stringWithFormat:@"%@ made by %@ company website : %@",
				 software.title, software.company, software.companyURL];
}

@end


@interface FancyDescriptionVisitor : Visitor
@end

@implementation FancyDescriptionVisitor

- (void) visitBook:(BookVisitee *)book
{
	self.note = [NSString stringWithFormat:@"%@ ...!*@*! written !*! by !@! %@",
				 book.title, book.author];
}

- (void) visitSoftware:(SoftwareVisitee *)software
{
	self.note = [NSString stringWithFormat:@"%@ ...!!! made !*! by !@@!  %@ ...www website !**! at http:// : %@",
				 software.title, software.company, software.companyURL];
}


@end

//double dispatch any visitor and visitee objects

void acceptVisitor(Visitee *visitee, Visitor *visitor) {
	[visitee acceptVisitor:visitor];
}

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"BEGIN TESTING VISITOR PATTERN");
		NSLog(@"");
		
		BookVisitee *book = [[BookVisitee alloc] initWithTitle:@"Design Patterns" andAuthor:@"Gamma, Helm, Johnson, and Vlissides"];
		
		SoftwareVisitee *soft = [[SoftwareVisitee alloc] initWithTitle:@"Zend Studio" companyName:@"Zend Technologies" andURL:@"www.zend.com"];
		
		PlainDescriptorVisitor *plainVisitor = [PlainDescriptorVisitor new];
		
		acceptVisitor(book, plainVisitor);
		NSLog(@"plain description of book: %@", plainVisitor.note);
		acceptVisitor(soft, plainVisitor);
		NSLog(@"plain description of software: %@",plainVisitor.note);
		NSLog(@"");
		
		FancyDescriptionVisitor *fancy = [FancyDescriptionVisitor new];
		
		acceptVisitor(book, fancy);
		NSLog(@"fancy description of book: %@", fancy.note);
		acceptVisitor(soft, fancy);
		NSLog(@"fancy description of software: %@", fancy.note);
		
		NSLog(@"END TESTING VISITOR PATTERN");
	}
	return 0;
}
