//
//  main.m
//  InterpreterDemo
//
//  Created by Водолазкий В.В. on 07.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
	romanNumeral ::= {thousands} {hundreds} {tens} {ones}
	thousands, hundreds, tens, ones ::= nine | four | {five} {one} {one} {one}
	nine ::= "CM" | "XC" | "IX"
	four ::= "CD" | "XL" | "IV"
	five ::= 'D' | 'L' | 'V'
	one  ::= 'M' | 'C' | 'X' | 'I'
 */


@interface RNInterpreter : NSObject

@property (nonatomic, readonly) NSInteger multiplier;
@property (nonatomic, readonly) NSString *oneRoman;
@property (nonatomic, readonly) NSString *fourRoman;
@property (nonatomic, readonly) NSString *fiveRoman;
@property (nonatomic, readonly) NSString *nineRoman;

- (NSString *) processString:(NSString *)aString withInintialSum:(NSInteger *) total;

@end

@implementation RNInterpreter

- (NSString *) processString:(NSString *)tmp withInintialSum:(NSInteger *)total
{
	NSInteger totalUpdated = *total;
	NSInteger index = 2;
	if ([tmp hasPrefix:self.nineRoman]) {
		totalUpdated += 9 * self.multiplier;
	} else if ([tmp hasPrefix:self.fourRoman]) {
		totalUpdated += 4 * self.multiplier;
	} else {
		if ([tmp hasPrefix:self.fiveRoman]) {
			totalUpdated += 5 * self.multiplier;
			index = 1;
		} else index = 0;
	}
	// process "oneRoman" case - tetrade may contain up yto three entrances
	NSInteger oneStart = index;
	NSInteger lastIndex = index + 3;
	if (lastIndex >= tmp.length) {
		lastIndex = tmp.length - 1;
	}
	for (NSInteger i = oneStart; i <= lastIndex; i++) {
		NSRange r = NSMakeRange(i, 1);
		NSString *current = [tmp substringWithRange:r];
		if ([current isEqualToString:self.oneRoman]) {
			totalUpdated += self.multiplier;
			index++;
		} else {
			break;
		}
	}
	*total  = totalUpdated;
	tmp = [tmp substringFromIndex:index];
	return tmp;
}

// default implemetation for abstract ancestor

- (NSInteger) multiplier { return 0; }
- (NSString *) oneRoman { return @"one"; }
- (NSString *) fourRoman { return @"four"; }
- (NSString *) fiveRoman { return @"five"; }
- (NSString *) nineRoman { return @"nine"; }

@end

@interface RNOne : RNInterpreter
@end

@implementation RNOne
- (NSInteger) multiplier { return 1; }
- (NSString *) oneRoman { return @"I"; }
- (NSString *) fourRoman { return @"IV"; }
- (NSString *) fiveRoman { return @"V"; }
- (NSString *)nineRoman { return @"IX"; }
@end

@interface RNTen : RNInterpreter
@end

@implementation RNTen
- (NSInteger) multiplier { return 10; }
- (NSString *) oneRoman { return @"X"; }
- (NSString *) fourRoman { return @"XL"; }
- (NSString *) fiveRoman { return @"L"; }
- (NSString *)nineRoman { return @"XC"; }
@end

@interface RNHundred : RNInterpreter
@end

@implementation RNHundred
- (NSInteger) multiplier { return 100; }
- (NSString *) oneRoman { return @"C"; }
- (NSString *) fourRoman { return @"CD"; }
- (NSString *) fiveRoman { return @"D"; }
- (NSString *)nineRoman { return @"CM"; }
@end

@interface RNThousand : RNInterpreter
@end

@implementation RNThousand
- (NSInteger) multiplier { return 10; }
- (NSString *) oneRoman { return @"M"; }
- (NSString *) fourRoman { return @""; }
- (NSString *) fiveRoman { return @""; }
- (NSString *)nineRoman { return @""; }
@end

@interface RomanInterpreter : NSObject
{
	RNThousand	*thousands;
	RNHundred	*hundreds;
	RNTen		*tens;
	RNOne		*ones;
}

- (NSInteger) interpret:(NSString *)romanString;

@end

@implementation RomanInterpreter

- (id) init
{
	if (self = [super init]) {
		thousands = [[RNThousand alloc] init];
		hundreds = [[RNHundred alloc] init];
		tens = [[RNTen alloc] init];
		ones = [[RNOne alloc] init];
	}
	return self;
}

- (NSInteger) interpret:(NSString *)romanString
{
	NSInteger total = 0;
	NSString *wrk = romanString;
	wrk = [thousands processString:wrk withInintialSum:&total];
	wrk = [hundreds processString:wrk withInintialSum:&total];
	wrk = [tens processString:wrk withInintialSum:&total];
	wrk = [ones processString:wrk withInintialSum:&total];
	return total;
}

@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		RomanInterpreter *rin = [[RomanInterpreter alloc] init];
		NSString *input = @"CMXCIX";
		NSInteger number = [rin interpret:input];
		NSLog(@"%@ -> %ld",input, (long)number);
	}
	return 0;
}
