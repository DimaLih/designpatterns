//
//  main.swift
//  BuilderSwiftDemo
//
//  Created by Водолазкий В.В. on 23.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

import Foundation

class InfantryMan {
    func Info() -> String  {
        return "InfantryMan";
    }
}

class Archer {
    func Info() -> String {
        return "Archer";
    }
}

class Horseman {
    func Info() -> String {
        return "Horseman";
    }
}

class Catapult {
    func Info() -> String {
        return "Catapult";
    }
}

class Elephant {
    func Info() -> String {
        return "Elephant";
    }
}

//
// Army builders
//

class Army {
    
    var infantry : Array < InfantryMan > = []
    var archers  : Array < Archer > = []
    var cavalry  : Array < Horseman > = []
    var artillery : Array < Catapult > = []
    var tanks: Array < Elephant > = []
    
    func PrintInfo() {
        for inf in infantry {
            print(inf.Info())
        }
        for arc in archers {
            print(arc.Info())
        }
        for hor in cavalry {
            print(hor.Info())
        }
        for art in artillery {
            print(art.Info())
        }
        for tan in tanks {
            print(tan.Info())
        }
    }
}

class ArmyBuilder {
    let army : Army = Army.init()


    func buildInfantry() {
        army.infantry.append(InfantryMan.init())
    }
    func buildArcher() {
        army.archers.append(Archer.init())
    }
    func buildHorseman() {
        army.cavalry.append(Horseman.init())
    }
    func buildArtillery() {}
    func buildTanks() {}
}

class RomanArmyBuilder : ArmyBuilder {
    
    override func buildArtillery() {
        army.artillery.append(Catapult.init())
    }
}

class CarthageArmyBuilder : ArmyBuilder {
    
    override func buildTanks() {
        army.tanks.append(Elephant.init())
    }
}


class Director {
    func createArmy( builder : ArmyBuilder) -> Army {
        builder.buildInfantry()
        builder.buildArcher()
        builder.buildHorseman()
        builder.buildArtillery()
        builder.buildTanks()
        
        return builder.army
    }
}

    let director = Director.init()

    let romanBuilder = RomanArmyBuilder.init()
    let carthageBuilder = CarthageArmyBuilder.init()

    let romanArmy = director.createArmy(builder: romanBuilder)
    let carthageArmy = director.createArmy(builder: carthageBuilder)

    print("Roman Army")
    romanArmy.PrintInfo()
    print("")
    print("Carthage Army")
    carthageArmy.PrintInfo()


