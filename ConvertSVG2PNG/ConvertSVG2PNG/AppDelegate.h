//
//  AppDelegate.h
//  ConvertSVG2PNG
//
//  Created by Dmitry Likhtarov on 12.03.17.
//  Copyright © 2017 Dmitry Likhtarov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

