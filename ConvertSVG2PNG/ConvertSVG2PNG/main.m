//
//  main.m
//  ConvertSVG2PNG
//
//  Created by Dmitry Likhtarov on 12.03.17.
//  Copyright © 2017 Dmitry Likhtarov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
