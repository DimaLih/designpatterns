//
//  AppDelegate.m
//  ConvertSVG2PNG
//
//  Created by Dmitry Likhtarov on 12.03.17.
//  Copyright © 2017 Dmitry Likhtarov. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
