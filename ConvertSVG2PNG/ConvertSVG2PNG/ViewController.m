//
//  ViewController.m
//  ConvertSVG2PNG
//
//  Created by Dmitry Likhtarov on 12.03.17.
//  Copyright © 2017 Dmitry Likhtarov. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>
#import <Quartz/Quartz.h>

@interface ViewController() <NSTableViewDataSource, NSTableViewDelegate, NSTextFieldDelegate, WebFrameLoadDelegate>
@property (weak,nonatomic) IBOutlet NSTableView *table;
@property (weak,nonatomic) IBOutlet NSImageView *imageView;
@property (weak,nonatomic) IBOutlet NSTextField *tfPixels3;
@property (weak,nonatomic) IBOutlet NSTextField *tfPixels2;
@property (weak,nonatomic) IBOutlet NSTextField *tfPixels1;
@property (weak,nonatomic) IBOutlet NSButton *razmer3;
@property (weak,nonatomic) IBOutlet NSButton *razmer2;
@property (weak,nonatomic) IBOutlet NSButton *razmer1;

@property (weak,nonatomic) IBOutlet NSTextField *tfPathSource;
@property (weak,nonatomic) IBOutlet NSTextField *tfPathTarget;

@property (weak,nonatomic) IBOutlet NSTextField *tfColorWite;
@property (weak,nonatomic) IBOutlet NSTextField *tfColorBlack;

@property (weak,nonatomic) IBOutlet NSColorWell *colorWhite;
@property (weak,nonatomic) IBOutlet NSColorWell *colorBlack;
@property (weak,nonatomic) IBOutlet NSProgressIndicator *progress;


@end

@implementation ViewController {
    NSMutableArray *tableContents;
    //WebView *www;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tfPathSource.stringValue = @"/Users/Dima/Documents/iProgi/RABOTA/designpatterns/Исходники/SVG белые/";
    self.tfPathTarget.stringValue = @"/Users/Dima/Documents/iProgi/RABOTA/designpatterns/Исходники/SVG белые/png/";

    [self reloadFilesNames];

}
- (void) reloadFilesNames {
    tableContents = [[NSMutableArray alloc] init];
    //NSString *path = @"/Users/Dima/Documents/iProgi/RABOTA/designpatterns/Исходники/SVG белые/";
    NSFileManager *fileManedger = [NSFileManager defaultManager];
    // Список поддиректорий директории или список файлов
    NSDirectoryEnumerator *dEnumerator = [fileManedger enumeratorAtPath:self.tfPathSource.stringValue];
    NSString *file;
    while (file = [dEnumerator nextObject]) {
        NSString *filePath = [self.tfPathSource.stringValue stringByAppendingFormat:@"/%@", file]; // полное имя файла с расширением
        NSString *ext = [file pathExtension];
        if ([ext isEqualToString:@"SVG"] || [ext isEqualToString:@"svg"]) {
            [tableContents addObject:@{@"fileWithPath": filePath, @"name": [file stringByDeletingPathExtension]}];
        }
    }
}
- (void)controlTextDidChange:(NSNotification *)notification {
    NSTextField *textField = [notification object];
    if (textField == self.tfPathSource) {
        [self reloadFilesNames];
        [self.table reloadData];
    }
}
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [tableContents count];
}
- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    
    NSDictionary *stroka = tableContents [row];
    NSString *colID = [tableColumn identifier];
    NSTableCellView *tCell = nil;
    if ([colID isEqualToString:@"AutomaticTableColumnIdentifier.0"]) {
        tCell = [tableView makeViewWithIdentifier:@"AutomaticTableColumnIdentifier.0" owner:self];
        [tCell.textField setStringValue:stroka[@"name"]];
    } else if ([colID isEqualToString:@"AutomaticTableColumnIdentifier.1"]) {
        tCell = [tableView makeViewWithIdentifier:@"AutomaticTableColumnIdentifier.1" owner:self];
        [tCell.textField setStringValue:stroka[@"fileWithPath"]];
    }
    
    return tCell;
}

- (IBAction)QuitButtonPressed:(id)sender
{
    [[NSApplication sharedApplication] terminate:nil];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    // Update the view, if already loaded.
}
- (IBAction)selectRazperChanged:(id)sender {
    self.tfPixels1.enabled = [self.razmer1 state];
    self.tfPixels2.enabled = [self.razmer2 state];
    self.tfPixels3.enabled = [self.razmer3 state];
}
- (IBAction)CreatePNGFilesFromSVG:(id)sender
{
    if ([sender isKindOfClass:[NSButton class]]) {
        [sender setHidden:YES];
        self.progress.hidden = NO;
        self.progress.maxValue = tableContents.count;
        self.progress.doubleValue = 0;
    }
    
    for (int row=0; row<tableContents.count; row++) {
       
        for (int ii=1; ii<3; ii++) { // Два цвета
            
            NSString * colorSuffiks = self.tfColorWite.stringValue;
            CIColor * color = [CIColor colorWithCGColor:self.colorWhite.color.CGColor];
            if (ii==2) {
                color = [CIColor colorWithCGColor:self.colorBlack.color.CGColor];
                colorSuffiks = self.tfColorBlack.stringValue;
            }
            
            NSDictionary *dict = [self frameAnStringSVGFromfile:tableContents[row][@"fileWithPath"] color:color]; // Пытаемся получить фрейм svg файла
            
            NSRect frame = [dict[@"framevalue"] rectValue];
            CGFloat ratio = frame.size.width / frame.size.height;
            NSString * widthImg   = (ratio >1) ? @"100\%" : @"auto"; // картинка горизонтальная : вертикальная
            NSString * heightImg  = (ratio >1) ? @"auto"  : @"100\%";
            NSString *htmlString = htmlString = [NSString stringWithFormat:@"<html><head> <meta name=\"viewport\" content=\"width=device-width, maximum-scale=5.0\" /> </head> <body style=\"margin:0;\"> <div style=\"display:block; margin:0 auto; height:%@;width:%@;\">%@</div></body></html>", widthImg, heightImg, dict[@"svgstring"]];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            if ([self.razmer1 state]) {
                [array addObject:@{@"ext":@"",   @"pixels":[NSNumber numberWithFloat:[self.tfPixels1.stringValue floatValue]]}];
            }
            if ([self.razmer2 state]) {
                [array addObject:@{@"ext":@"@2x",@"pixels":[NSNumber numberWithFloat:[self.tfPixels2.stringValue floatValue]]}];
            }
            if ([self.razmer3 state]) {
                [array addObject:@{@"ext":@"@3x",@"pixels":[NSNumber numberWithFloat:[self.tfPixels3.stringValue floatValue]]}];
            }
//            NSArray *array = @[
//                               @{@"ext":@"@3x",@"pixels":[NSNumber numberWithFloat:[self.tfPixels3.stringValue floatValue]]},
//                               @{@"ext":@"@2x",@"pixels":[NSNumber numberWithFloat:[self.tfPixels2.stringValue floatValue]]},
//                               @{@"ext":@"",   @"pixels":[NSNumber numberWithFloat:[self.tfPixels1.stringValue floatValue]]},
//            ];
            for (NSDictionary *d in array) {
                [self createPngName:
                 [NSString stringWithFormat:@"%@%@%@.png",tableContents[row][@"name"],colorSuffiks,d[@"ext"]]
                             pixels:[d[@"pixels"] floatValue]
                              frame:frame htmlString:htmlString];
            }
        }
        
        
    }
    
    [sender setHidden:NO];
    //self.progress.hidden = YES;

}

- (void)createPngName:(NSString*)name pixels:(CGFloat)pixels frame:(NSRect)frame htmlString:(NSString*)htmlString  {


    CGFloat scale =  (pixels / frame.size.width) / [NSScreen mainScreen].backingScaleFactor;
    
    frame.size.width = frame.size.width * scale;
    frame.size.height = frame.size.height * scale;
    
    WebView *www = [[WebView alloc] initWithFrame:frame frameName:name groupName:@""]; // 33 штуки это фигня:)
    www.frameLoadDelegate = self;
    www.drawsBackground = NO;
    //[self.view addSubview:www]; // на экран не будем выводить
    [[www mainFrame] loadHTMLString:htmlString baseURL:nil];
    NSLog(@"%@:  %.0f  %.0f",name,frame.size.width,frame.size.height);
   
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame {

    NSView *webFrameViewDocView = [[[sender mainFrame] frameView] documentView];
    NSRect cacheRect = [webFrameViewDocView bounds];
    
    NSBitmapImageRep *bitmapRep = [webFrameViewDocView bitmapImageRepForCachingDisplayInRect:cacheRect];
    [webFrameViewDocView cacheDisplayInRect:cacheRect toBitmapImageRep:bitmapRep];
    NSData *data = [bitmapRep representationUsingType: NSPNGFileType properties: @{}];
    NSString *name =[NSString stringWithFormat:@"%@%@",self.tfPathTarget.stringValue, sender.mainFrame.name];
    //NSLog(@"save:\n%@",name);
    [data writeToFile:name atomically:YES];

//    double delayInSeconds = 0.05;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        //[[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
        [self.progress incrementBy:1];
        [self.progress displayIfNeeded];
//    });
    
   
}


- (NSDictionary *) frameAnStringSVGFromfile:(NSString*)nameSVGFile color:(CIColor*)color
{
    // ! Цвет при подкотовке SVG всех элементов делать не черным! так как текст черный идет без тега цвета
    //
    // @"framevalue": размер фрейма svg файла из viewBox в <svg viewBox="34 0 57 296.91">  Если нет всех четырех размеров, то вернем CGRectZero
    // @"svgstring": измененный SVG c линиями и заливками в цвете текста из настроек
    // @"imagename": копия имени входного файла nameSVGFile если это не svg

    NSDictionary *dict = nil;
    NSRect frame = CGRectZero;
    NSString *stringSVG =[NSString stringWithContentsOfFile:nameSVGFile encoding:NSUTF8StringEncoding error:nil];
    if (stringSVG) {
        NSTextCheckingResult * matchs = [[NSRegularExpression regularExpressionWithPattern:@"<\\s*svg[^>]*viewBox\\s*=\\s*\"\\s*([-+]?\\d*\\.?\\d*)\\s*([-+]?\\d*\\.?\\d*)\\s*([-+]?\\d*\\.?\\d*)\\s*([-+]?\\d*\\.?\\d*)" options:NSRegularExpressionAnchorsMatchLines error:nil] firstMatchInString:stringSVG options:0 range:NSMakeRange(0, stringSVG.length)]; //|<\\s*s(v)(g)[^>]*width\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)[^>]*height\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)|<\\s*s(v)(g)[^>]*height\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)[^>]*width\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)
        if ([matchs numberOfRanges]==5) { // только если есть все 4 цифры viewBox="34 0 57 296.91"
            frame = NSMakeRect(
                               [[stringSVG substringWithRange:[matchs rangeAtIndex:1]] floatValue],
                               [[stringSVG substringWithRange:[matchs rangeAtIndex:2]] floatValue],
                               [[stringSVG substringWithRange:[matchs rangeAtIndex:3]] floatValue],
                               [[stringSVG substringWithRange:[matchs rangeAtIndex:4]] floatValue]
                               );
            // Заменим все линии и заливки в SVG на цвет текста
            NSString *hexColor = [self hexFromColor:color];
            NSArray <NSTextCheckingResult *> * array = [[NSRegularExpression regularExpressionWithPattern:@"(?:stroke|fill)\\s*:\\s*#([0-9a-fA-F]+)" options:0 error:nil] matchesInString:stringSVG options:0 range:NSMakeRange(0, stringSVG.length)];
            for (long i = array.count - 1; i >= 0 ; i--) {
                NSTextCheckingResult* match = array[i];
                if (match.numberOfRanges==2 && [match rangeAtIndex:1].length > 0) {
                    stringSVG = [stringSVG stringByReplacingCharactersInRange:[match rangeAtIndex:1] withString:hexColor];
                }
            }
            dict = @{@"svgstring":stringSVG,@"framevalue":[NSValue valueWithRect:frame]};
        }
    }
    return dict;
}

#pragma mark - color to tipa FFFAA99
// Возвращает nil или 6 16-тиричных символов
- (NSString *) hexFromColor:(CIColor *)color
{
    if (!color || color == [CIColor whiteColor]) { return @"ffffff"; }
    return [NSString stringWithFormat:@"%02x%02x%02x", (unsigned int)(color.red * 255), (unsigned int)(color.green * 255), (unsigned int)(color.blue * 255)];;
}

//// Остатки вывода на UIWebView :))
//- (void) configureView
//{
//    // Update the user interface for the detail item.
//    NSString *title = [NSString stringWithFormat:@"PATTERN_%ld_%ld",(long)self.group, (long)self.item];
//    self.title = RStr(title);
//
//    NSString *descr = [NSString stringWithFormat:@"PATTERN_%ld_%ld_DESC",(long)self.group, (long)self.item];
//    self.itemText.text = RStr(descr);
//    if ([self pictureArray].count > self.group) {
//        NSArray *list = [[self pictureArray] objectAtIndex:self.group];
//        if (list.count > self.item) {
//            //-----------------------
//            [self.view layoutIfNeeded]; // !!! Перед манипуляций обновим элементы, чтобы пересчитались размеры в соответствии с резинками
//
//            NSDictionary *dict = [self frameAnStringSVGFromfile:list[self.item]]; // Пытаемся получить фрейм svg файла
//
//            if (!dict[@"framevalue"]) {
//                // Если изображение слишком низкое или отсутствует, то подвинем текстовый блок кверху
//                self.constraintImageHigh.constant = 1;
//                //self.constraintImageVerh.constant = 1;
//            } else {
//                //  Картинка есть. Настроим фрейм под пропорции картинки, но не больше чем пол-экрана
//                CGRect frame = [dict[@"framevalue"] CGRectValue];
//                CGFloat ratio = frame.size.width / frame.size.height;
//                CGFloat visota = self.www.frame.size.width / ratio;
//                if (visota > self.view.frame.size.height / 2) {
//                    visota = self.view.frame.size.height / 2;
//                }
//                self.constraintImageHigh.constant = visota;
//
//#warning Need edit this!!! Я так толком и не смог разобраться нормально отцентрировать
//                // Если это можно сделать CSS, то как? мне проще вычислить размер заранее и вписать в теги ширина-высота:)
//                NSString * widthImg   = (ratio >1) ? @"93\%" : @"auto"; // картинка горизонтальная : вертикальная
//                NSString * heightImg  = (ratio >1) ? @"auto"  : @"93\%";
//                NSString * htmlString;
//                if (dict[@"imagename"]) {
//                    htmlString = [NSString stringWithFormat:@"<html><head> <meta name=\"viewport\" content=\"width=device-width, maximum-scale=5.0\" /> </head> <body style=\"margin:0;\"> <img style=\"display:block; margin:0 auto; height:%@;width:%@;\" src=\"%@\" /></body></html>", widthImg, heightImg, dict[@"imagename"]];
//                } if (dict[@"svgstring"]) {
//                    htmlString = [NSString stringWithFormat:@"<html><head> <meta name=\"viewport\" content=\"width=device-width, maximum-scale=5.0\" /> </head> <body style=\"margin:0;\"> <div style=\"display:block; margin:0 auto; height:%@;width:%@;\">%@</div></body></html>", widthImg, heightImg, dict[@"svgstring"]];
//                }
//                [self.www loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]]];
//            }
//            [self.view layoutIfNeeded]; // !!! После всех манипуляций обновим элементы, размеры и резинки
//        }
//    }
//
//}
//

@end

