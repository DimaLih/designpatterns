//
//  main.m
//  FlyWeightDemo
//
//  Created by Водолазкий В.В. on 09.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject

@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *title;

- (instancetype) initWithAuthor:(NSString *)aAuthor andTitle:(NSString *)aTitle;

@end

@implementation Book

- (instancetype) initWithAuthor:(NSString *)aAuthor andTitle:(NSString *)aTitle
{
	if (self = [super init]) {
		self.author = aAuthor;
		self.title = aTitle;
	}
	return self;
}

- (NSString *) description {
	return [NSString stringWithFormat:@"Book: author - %@, title - %@",
			self.author, self.title];
}

@end


@interface BookFactory : NSObject {
	NSMutableDictionary *books;
}

- (Book *) bookForKey:(NSString *)key;

@end

@implementation BookFactory

- (instancetype) init {
	if (self = [super init]) {
		books = [NSMutableDictionary new];
		[books setObject:[self makeBook1] forKey:@"Book1"];
		[books setObject:[self makeBook2] forKey:@"Book2"];
		[books setObject:[self makeBook3] forKey:@"Book3"];
	}
	return self;
}

- (Book *) bookForKey:(NSString *)key
{
	return books[key];
}

- (Book *) makeBook1 {
	return [[Book alloc] initWithAuthor:@"Terry Pratchet"
							   andTitle:@"Small Gods"];
}

- (Book *) makeBook2 {
	return [[Book alloc] initWithAuthor:@"Terry Pratchet"
							   andTitle:@"The Colour of Magic"];
}

- (Book *) makeBook3 {
		return [[Book alloc] initWithAuthor:@"Terry Pratchet"
								   andTitle:@"Guards! Guards!"];
}

@end

@interface BookShelf : NSObject {
	NSMutableArray *books;
}

- (void) addBook:(Book *)aBook;
- (void) showBooks;

@end

@implementation BookShelf

- (instancetype) init {
	if (self = [super init]) {
		books = [NSMutableArray new];
	}
	return self;
}

- (void) addBook:(Book *)aBook
{
	if (aBook) {
		[books addObject:aBook];
	}
}

- (void) showBooks {
	for (Book *b in books) {
		NSLog(@"%@", b);
	}
}

@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		BookFactory *publisher = [[BookFactory alloc] init];
		BookShelf *shelf1 = [[BookShelf alloc] init];
		BookShelf *shelf2 = [[BookShelf alloc] init];
		
		[shelf1 addBook:[publisher bookForKey:@"Book1"]];
		[shelf1 addBook:[publisher bookForKey:@"Book2"]];
		[shelf1 addBook:[publisher bookForKey:@"Book1"]];
		
		[shelf2 addBook:[publisher bookForKey:@"Book1"]];
		[shelf2 addBook:[publisher bookForKey:@"Book3"]];
		[shelf2 addBook:[publisher bookForKey:@"Book2"]];

		NSLog(@"Books on shelf 1:");
		[shelf1 showBooks];
		NSLog(@"Books on shelf 2:");
		[shelf2 showBooks];
	
	}
    return 0;
}
