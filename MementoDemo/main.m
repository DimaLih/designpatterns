//
//  main.m
//  MementoDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookReader : NSObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, readwrite) NSInteger page;

- (instancetype) initWithTitle:(NSString *)aTitle andPage:(NSInteger)aPage;

@end

@implementation BookReader

- (instancetype) initWithTitle:(NSString *)aTitle andPage:(NSInteger)aPage
{
	if (self = [super init]) {
		self.title = aTitle;
		self.page = aPage;
	}
	return self;
}

@end

@interface BookMark : NSObject {
	NSString *title;
	NSInteger page;
}

- (instancetype) initWithBookReader:(BookReader *)reader;

- (void) setTitle:(BookReader *)reader;
- (void) getTitle:(BookReader *)reader;
- (void) setPage:(BookReader *)reader;
- (void) getPage:(BookReader *)reader;

@end


@implementation BookMark

- (instancetype) initWithBookReader:(BookReader *)reader
{
	if (self = [super init]) {
		[self setPage:reader];
		[self setTitle:reader];
	}
	return self;
}

- (void) setTitle:(BookReader *)reader
{
	title  =reader.title;
}

- (void) getTitle:(BookReader *)reader
{
	reader.title = title;
}

- (void) setPage:(BookReader *)reader
{
	page = reader.page;
}

- (void) getPage:(BookReader *)reader
{
	reader.page = page;
}

@end



int main(int argc, const char * argv[]) {
	@autoreleasepool {

		NSLog(@"BEGIN TESTING MEMENTO PATTERN");
		NSLog(@"");
		
		BookReader *br = [[BookReader alloc] initWithTitle:@"Core PHP Programming, Third Edition" andPage:103];
		BookMark *bm = [[BookMark alloc] initWithBookReader:br];
		
		NSLog(@"(at beginning) bookReader title: %@", br.title);
		NSLog(@"(at beginning) bookReader page: %ld", br.page);
		
		br.page = 104;
		[bm setPage:br];
		
		NSLog(@"(one page later) bookReader page: %ld", br.page);
		
		br.page = 2005;	 //oops! a typo
		
		NSLog(@"(after typo) bookReader page: %ld", br.page);
		
		[bm getPage:br];
		NSLog(@"(back to one page later) bookReader page: %ld", br.page);
		NSLog(@"");
		
		NSLog(@"END TESTING MEMENTO PATTERN");

	}
	return 0;
}
