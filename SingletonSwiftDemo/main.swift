//
//  main.swift
//  SingletonSwiftDemo
//
//  Created by Водолазкий В.В. on 26.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

let VVVheroName = "HERO_NAME"

class Preferences {
    
    static let sharedPreferences = Preferences()
    
    private let prefs : UserDefaults = UserDefaults.standard
    
    //
    // Store property' data in UserDefaults
    //
    var heroName : String {
        get { return prefs.string(forKey:VVVheroName)! }
        set { prefs.set(newValue, forKey:VVVheroName) }
    }
    
    
    private init() {
        // to prevent it direct call
        let defaultValues = [ VVVheroName : "Player Name"]
        prefs.register(defaults: defaultValues)
    }
    
    func flush() {
        prefs.synchronize()
    }
}


let prefs = Preferences.sharedPreferences

print("Initial value -", prefs.heroName)

prefs.heroName = "Buonaparte"

print("Modified value -", prefs.heroName)

