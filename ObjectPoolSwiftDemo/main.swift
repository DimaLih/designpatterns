//
//  main.swift
//  ObjectPoolSwiftDemo
//
//  Created by Водолазкий В.В. on 23.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

class Filer {
    var busy : Bool = false
    var fileNumber : Int = 0;
    
    func useFiler() {
        print("Using filer", fileNumber);
    }
}

class FilerPool {
    var filers : Array < Filer > = []
    
    func acquireFiler() -> Filer {
        for f in filers {
            if f.busy == false {
                f.busy = true
                return f
            }
        }
        let newFiler = Filer.init()
        newFiler.fileNumber = filers.count + 1
        newFiler.busy = true
        filers.append(newFiler)
        return newFiler
    }
    
    func returnFilerToPool(filer : Filer) {
        filer.busy = false
    }
}

    let pool = FilerPool.init()

    let filerA = pool.acquireFiler()
    filerA.useFiler()

    let filerB = pool.acquireFiler()
    filerB.useFiler()

    pool.returnFilerToPool(filer: filerA)

    let filerC = pool.acquireFiler()
    filerC.useFiler()

    let filerD = pool.acquireFiler()
    filerD.useFiler()

    pool.returnFilerToPool(filer: filerB)

    let filerE = pool.acquireFiler()
    filerE.useFiler()



