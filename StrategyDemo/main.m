//
//  main.m
//  StrategyDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, StratgyIndex) {
	StrategyIndexUppercase,
	StrategyIndexExclaim,
	StrategyIndexStar
};


@interface Book : NSObject

@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *title;

@property (nonatomic, readonly) NSString *authorAndTitle;

- (instancetype) initWithTitile:(NSString *)titile andAuthor:(NSString *)author;

@end

@implementation Book

- (instancetype) initWithTitile:(NSString *)titile andAuthor:(NSString *)author
{
	if (self = [super init]) {
		self.title = titile;
		self.author = author;
	}
	return self;
}

- (NSString *) authorAndTitle
{
	return [NSString stringWithFormat:@"%@ by %@", self.title, self.author];
}

@end

@interface StrategyInterface : NSObject

- (NSString *) showBookTitle:(Book *)book;

@end

@implementation StrategyInterface

- (NSString *) showBookTitle:(Book *)book { return nil; }

@end

@interface StrategyCaps : StrategyInterface
@end

@implementation StrategyCaps

- (NSString *) showBookTitle:(Book *)book
{
	return [book.title uppercaseString];
}
@end

@interface StrategyExclaim : StrategyInterface
@end

@implementation StrategyExclaim

- (NSString *) showBookTitle:(Book *)book
{
	NSString *tmp = [book.title uppercaseString];
	return [tmp stringByReplacingOccurrencesOfString:@" " withString:@"!"];
}
@end

@interface StrategyStar: StrategyInterface
@end

@implementation StrategyStar

- (NSString *) showBookTitle:(Book *)book
{
	NSString *tmp = [book.title uppercaseString];
	return [tmp stringByReplacingOccurrencesOfString:@" " withString:@"*"];
}
@end


@interface StrategyContext : NSObject
{
	StrategyInterface *strategy;
}

- (instancetype) initWithIndex:(StratgyIndex)index;
- (NSString *) showBookTitle:(Book *)book;

@end

@implementation StrategyContext

- (instancetype) initWithIndex:(StratgyIndex)index
{
	if (self = [super init]) {
		switch (index) {
			case StrategyIndexExclaim:	strategy = [[StrategyExclaim alloc] init];
				break;
			case StrategyIndexStar:		strategy = [[StrategyStar alloc] init];
				break;
			case StrategyIndexUppercase: strategy = [[StrategyCaps alloc] init];
				break;
		}
	}
	return self;
}

- (NSString *) showBookTitle:(Book *)book
{
	return [strategy showBookTitle:book];
}

@end

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"BEGIN TESTING STRATEGY PATTERN");
		NSLog(@"");
		
		Book *b = [[Book alloc] initWithTitile:@"PHP for Cats"
									 andAuthor:@"Larry Truett"];

		StrategyContext *sc = [[StrategyContext alloc] initWithIndex:StrategyIndexUppercase];
		StrategyContext *ss = [[StrategyContext alloc] initWithIndex:StrategyIndexStar];
		StrategyContext *se = [[StrategyContext alloc] initWithIndex:StrategyIndexExclaim];
	
		NSLog(@"test 1 - show name context Uppercase");
		NSLog(@"%@", [sc showBookTitle:b]);

		NSLog(@"test 2 - show name context Star");
		NSLog(@"%@", [ss showBookTitle:b]);

		NSLog(@"test 3 - show name context Exclaim");
		NSLog(@"%@", [se showBookTitle:b]);

		NSLog(@"END TESTING STRATEGY PATTERN");
	}
	return 0;
}
