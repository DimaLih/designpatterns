//
//  main.swift
//  StateSwiftDemo
//
//  Created by Водолазкий В.В. on 12.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

/**

1. Create a "wrapper" class that models the state machine
2. The wrapper class contains an array of state concrete objects
3. The wrapper class contains an index to its "current" state
4. The wrapper class contains a state transition table
5. All client requests are simply delegated to the current state object
6. Create a state base class that makes the concrete states interchangeable
7. The State base class specifies default behavior for all messages
8. The State derived classes only override the messages they need to

*/

// 6. Create a state base class that makes the concrete states interchangeable
// 7. The State base class specifies default behavior
class State  {

	func on() {
		print("Error!")
	}
	
	func off() {
		print("Error!")
	}
		
	func ack() {
		print("Error!")
	}
}

class A : State {

	override func on() {
		print("A + on  = C")
	}
	
	override func off() {
		print("A + off = B")
	}
	
	override func ack() {
		print("A + ack = A")
	}
}

class B : State {
	
	override func on() {
		print("B + on  = A")
	}
	
	override func off() {
		print("B + off = C")
	}
	
}

class C : State {
	// 8. The State derived classes only override the messages they need to
	override func on() {
		print("C + on  = B")
	}

}


// 1. Create a "wrapper" class that models the state machine



class FSM {
	var states : [State] = []
	// 4. transitions
	let transitions : [[Int]] =  [[2,1,0],[0,2,1],[1,2,2]]
	// 3. current
	var current = 0
	
	init() {
		// 2. states
		states.append(A.init())
		states.append(B.init())
		states.append(C.init())
		
	}
	
	func next(msg:Int) {
		let tmp = transitions[current]
		current = tmp[msg]
	}
	// 5. All client requests are simply delegated to the current state object

	func on() {
		states[current].on()
		self.next(msg: 0)
	}
	
	func off() {
		states[current].off()
		self.next(msg: 1)
	}
	
	func ack() {
		states[current].ack()
		self.next(msg: 2)
	}
	
}


let fsm = FSM.init()
let msgs : [Int] = [2, 1, 2, 1, 0, 2, 0, 0]

for i in 0 ..< msgs.count {
	let msg = msgs[i]
	if (msg == 0) {
		fsm.on();
	} else if (msg == 1) {
		fsm.off();
	} else if (msg == 2) {
		fsm.ack();
	}

}


