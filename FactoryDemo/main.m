//
//  main.m
//  FactoryDemo
//
//  Created by Водолазкий В.В. on  22.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.

#import <Foundation/Foundation.h>


@interface Warrior : NSObject

- (void) info;

@end

@implementation Warrior

- (void) info {};

@end


@interface InfantryMan : Warrior
@end

@implementation InfantryMan

- (void) info
{
    NSLog(@"InfantryMan");
}

@end


@interface Archer : Warrior
@end

@implementation Archer

- (void) info{
    NSLog(@"Archer");
}

@end

@interface Horseman : Warrior
@end

@implementation Horseman

- (void) info
{
    NSLog(@"Horseman");
}

@end

#pragma mark - Factory classes

@interface Factory : NSObject

- (Warrior *) createWarrior;

@end

@implementation Factory

- (Warrior *) createWarrior
{
    return nil;
}

@end

@interface InfantryFactory : Factory
@end

@implementation InfantryFactory

- (Warrior *) createWarrior
{
    return [InfantryMan new];
}

@end

@interface ArcherFactory : Factory
@end

@implementation ArcherFactory

- (Warrior *) createWarrior
{
    return [Archer new];
}

@end

@interface HorsemanFactory : Factory
@end

@implementation HorsemanFactory

- (Warrior *) createWarrior
{
    return [Horseman new];
}

@end


#pragma mark -

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        InfantryFactory *iF = [InfantryFactory new];
        ArcherFactory *aF = [ArcherFactory new];
        HorsemanFactory *hF = [HorsemanFactory new];
        
        NSMutableArray *army = [NSMutableArray new];
        [army addObject:[aF createWarrior]];
        [army addObject:[iF createWarrior]];
        [army addObject:[hF createWarrior]];
        
        for (Warrior *w in army) {
            [w info];
        }
    
    }
    return 0;
}
