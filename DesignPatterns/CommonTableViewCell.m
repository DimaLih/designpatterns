//
//  CommonTableViewCell.m
//  QRWallet
//
//  Created by Водолазкий В.В. on 29.07.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "CommonTableViewCell.h"

@implementation CommonTableViewCell

+ (UINib *) cellNib
{
	return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (CGFloat) cellHeight
{
	return 44.0;		// hust default settings used by IB
}


@end
