//
//  RotatedView.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "GMRotatedView.h"
#import "GMRotatedView+AnimationDelegate.h"
#import "DebugPrint.h"

@implementation GMRotatedView

- (instancetype) initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame]) {
		self.hiddenAfterAnimation = NO;
	}
	return self;
}


- (void) addBackViewWithHeight:(CGFloat) height andColor:(UIColor *)color
{
	GMRotatedView *view = [[GMRotatedView alloc] initWithFrame:CGRectZero];
	view.backgroundColor = color;
	view.layer.anchorPoint = CGPointMake(0.5, 1.0);
	view.layer.transform = [view transform3D];
	view.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:view];
	self.backView = view;
    
    // блин они заточили фреймворк под конкретную высоту, поэтому ограничим
    height = MIN(height, self.bounds.size.height);
    CGFloat top = self.bounds.size.height - height * 0.5;
    //DLog(@"---> %.0f    %.0f  %.0f", self.frame.size.height,   height, top);
    [view addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 constant:height
                         ]];
    
    [self addConstraints:@[[NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:top
                            ],
                           [NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeLeading
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeading
                                                       multiplier:1.0 constant:0
                            ],
                           [NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeTrailing
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTrailing
                                                       multiplier:1.0 constant:0
                            ],
                           ]];
	
}

@end
