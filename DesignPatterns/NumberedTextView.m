//
//  NumberedTextView.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 30.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "NumberedTextView.h"
#import "Preferences.h"
#import "SDVersion.h"

#define TXT_VIEW_INSETS 8.0 // The default insets for a UITextView is 8.0 on all sides

@interface NumberedTextView ()
{
	UIScrollView		*internalScrollView;
	UITextView			*internalTextView;
	NSString			*initialText;
	Preferences			*prefs;
	
	UIColor				*lineNumColor;
	UIColor				*backColor;
}
@end

@implementation NumberedTextView

@synthesize lineNumbers;

- (void) awakeFromNib
{
	[super awakeFromNib];
	[self setupSubviews];
	prefs = [Preferences sharedPreferences];
	backColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
	lineNumColor = prefs.currentThemeColors[kRegexHighlightViewTypeNumber];
	internalTextView.textColor = lineNumColor; // tmp!!!
}


- (void) setupSubviews
{
		[self setContentMode:UIViewContentModeRedraw];
		internalScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
		[internalScrollView       setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
		[internalScrollView        setBackgroundColor:[UIColor clearColor]];
		[internalScrollView          setClipsToBounds:YES];
		[internalScrollView           setScrollsToTop:YES];
		[internalScrollView            setContentSize:self.bounds.size];
		[internalScrollView            setContentMode:UIViewContentModeLeft];
		[internalScrollView               setDelegate:self];
		[internalScrollView                setBounces:NO];
		
	internalTextView = [[UITextView alloc] initWithFrame:self.bounds];
	[internalTextView  setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[internalTextView      setAutocorrectionType:UITextAutocorrectionTypeNo];
	[internalTextView       setSpellCheckingType:UITextSpellCheckingTypeNo];
	[internalTextView        setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	[internalTextView         setBackgroundColor:[UIColor clearColor]];
	[internalTextView           setClipsToBounds:YES];
	[internalTextView            setScrollsToTop:NO];
	[internalTextView             setContentMode:UIViewContentModeLeft];
	[internalTextView                setDelegate:self];
	[internalTextView                 setBounces:NO];
	[internalScrollView addSubview:internalTextView];
	internalTextView.editable = NO;
	// Overwrite the textColor from the nib, set to clearColor
	internalTextView.textColor = [UIColor clearColor];
	// Set the syntax highlighting to use (the tempalate file contains the default highlighting)
//	[internalTextView setHighlightDefinitionWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cpp" ofType:@"plist"]];
//	// Set the color theme to use (all XCode themes are fully supported!)
//	[internalTextView setHighlightTheme:kRegexHighlightViewThemeDusk];

	NSNotificationCenter *nc  =[NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
	[nc addObserver:self selector:@selector(preferredFontChanged:)
												 name:UIContentSizeCategoryDidChangeNotification
											   object:nil];

	[self configureFont];
	[self configureThemeAndHighlight];
	[self addSubview:internalScrollView];
}


- (void)drawRect:(CGRect)rect {
	if (self.lineNumbers) {
		[[internalTextView textColor] set];
		CGFloat xOrigin, yOrigin, width = 0.0;/*, height*/;
		uint numberOfLines = (internalTextView.contentSize.height + internalScrollView.contentSize.height) / internalTextView.font.lineHeight;
		for (uint x = 0; x < numberOfLines; ++x) {
			NSString *lineNum = [NSString stringWithFormat:@"%d:", x+1];
			
			xOrigin = CGRectGetMinX(self.bounds);
			
			yOrigin = ((internalTextView.font.pointSize + fabs(internalTextView.font.descender)) * x) + TXT_VIEW_INSETS - internalScrollView.contentOffset.y;
			
			
			NSDictionary *sizeAttributes = @{
											 NSFontAttributeName: internalTextView.font,
											 NSBackgroundColorAttributeName :backColor,
											 NSForegroundColorAttributeName: lineNumColor,
											 };
			width = [lineNum sizeWithAttributes:sizeAttributes].width;
					 
				//            height = internalTextView.font.lineHeight;
			[lineNum drawAtPoint:CGPointMake(xOrigin, yOrigin) withAttributes:sizeAttributes];
		}
		
		CGRect tvFrame = [internalTextView frame];
		tvFrame.size.width = CGRectGetWidth(internalScrollView.bounds) - width;
		tvFrame.size.height = MAX(internalTextView.contentSize.height, CGRectGetHeight(internalScrollView.bounds));
		tvFrame.origin.x = width;
		[internalTextView setFrame:tvFrame];
		tvFrame.size.height -= TXT_VIEW_INSETS; // This fixed a weird content size problem that I've forgotten the specifics of.
		[internalScrollView setContentSize:tvFrame.size];
	}
}

#pragma mark - Text processing

- (NSString *)text
{
	return initialText;
}

- (void) setText:(NSString *)text
{
	initialText = text;
	
	internalTextView.text = initialText;
	[self setNeedsDisplay];
}

#pragma mark -

- (void) configureThemeAndHighlight
{
	NSDictionary *cTheme = prefs.currentThemeColors;
	backColor = cTheme[kRegexHighlightViewTypeBackground];
	internalTextView.backgroundColor = backColor;
	lineNumColor = cTheme[kRegexHighlightViewTypeNumber];
	internalTextView.textColor = cTheme[kRegexHighlightViewTypeText];
	self.backgroundColor = backColor;
	[self setNeedsDisplay];
}

- (void) themeChanged:(NSNotification *) note
{
	[self configureThemeAndHighlight];
}


- (void)preferredFontChanged:(NSNotification *)notification
{
	[self configureFont];
}

- (void) configureFont
{
	internalTextView.font = [self preferredFontForTextStyle:UIFontTextStyleBody scale:1.0];
	[self setNeedsDisplay];
	
}

- (UIFont *)preferredFontForTextStyle:(NSString *)style scale:(CGFloat)scale
{
	// We first get prefered font descriptor for provided style.
	UIFontDescriptor *currentDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:style];
	
	// Then we get the default size from the descriptor.
	// This size can change between iOS releases.
	// and should never be hard-codded.
	CGFloat headlineSize = [currentDescriptor pointSize];
	
	// We are calculating new size using the provided scale.
	CGFloat scaledHeadlineSize = lrint(headlineSize * scale);
	if ([SDVersion isIPad]) {
		if (scaledHeadlineSize < 10.0) {
			scaledHeadlineSize = 10.0;
		} else if (scaledHeadlineSize > 24.0) {
			scaledHeadlineSize = 24.0;
		}

	} else {
		if (scaledHeadlineSize < 10.0) {
			scaledHeadlineSize = 10.0;
		} else if (scaledHeadlineSize > 14.0) {
			scaledHeadlineSize = 14.0;
		}
	}
	
	// This method will return a font which matches the given descriptor
	// (keeping all the attributes like 'bold' etc. from currentDescriptor),
	// but it will use provided size if it's greater than 0.0.
	return [UIFont fontWithDescriptor:currentDescriptor size:scaledHeadlineSize];
}


#pragma mark - UITextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	[self setNeedsDisplay];
	return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
	[self setNeedsDisplay];
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
	[self setNeedsDisplay];
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	[self setNeedsDisplay];
}


@end
