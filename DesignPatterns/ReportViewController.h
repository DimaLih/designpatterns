//
//  ReportViewController.h
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 23.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportViewController : UIViewController

@property (retain, nonatomic) IBOutlet NSString *textZagolovok;
@property (retain, nonatomic) IBOutlet NSString *textOpisanie;
@property (retain, nonatomic) IBOutlet NSAttributedString *textAttributedOpisanie;
@property (retain, nonatomic) IBOutlet UIImage  *imageDiagram;

- (NSString *) pathFromFilePDF;
- (void) printPDF;


@end
