//
//  GMWalkthroughViewController.m
//  iWish
//
//  Created by Водолазкий В.В. on 04/10/14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "GMWalkthroughViewController.h"
#import "UIView+UIView_AutoLyout.h"
#import "DebugPrint.h"

#import "AppDelegate.h"

#define WalkThroughTimerInterval	4.0
#define WalkThroughPages			6

@interface GMWalkthroughViewController () {
	NSTimer *timer;
	NSInteger currentPage;
	IBOutlet UIButton *registrationButton;
	UIViewController *regController;
	
	// NSLayoutConstraint *imageYPosition;
	
	
}

@end

@implementation GMWalkthroughViewController

@synthesize pageView;
@synthesize pageTitleLabel = pageTitleLabel, pageDescription = pageDescription, regButton = regButton;
@synthesize pageIndicator = pageIndicator;
@synthesize justRegistered = justRegistered, enterButton = enterButton;

- (void) loadView
{
	CGRect applicationFrame = [UIScreen mainScreen].bounds;
	UIView *contentView = [[UIView alloc] initWithFrame:applicationFrame];
	contentView.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:20.0/255.0 blue:70.0/255.0 alpha:1.0];
	self.view = contentView;
	
	self.pageView = [[UIImageView alloc] init];
	[self.pageView setTranslatesAutoresizingMaskIntoConstraints:NO];
	self.pageView.backgroundColor = [UIColor clearColor];
	self.pageView.contentMode = UIViewContentModeScaleAspectFit;
	[self.view addSubview:self.pageView];
	
	// Width constraint, half of parent view width
	
	
	NSDictionary *views = NSDictionaryOfVariableBindings(pageView);
	NSDictionary *metrics = @{@"imageEdge":@120.0,@"padding":@10.0};

	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-40-[pageView]-40-|"
																	  options:0 metrics:nil views:views]];

	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|->=60-[pageView(240.0)]"
																	  options:0 metrics:metrics views:views]];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-<=100-[pageView]"
																	  options:0 metrics:metrics views:views]];
	
	
	// Center horizontally
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.pageView
														  attribute:NSLayoutAttributeCenterX
														  relatedBy:NSLayoutRelationEqual
														 toItem:self.view
													  attribute:NSLayoutAttributeCenterX
													 multiplier:1.0
													   constant:0.0]];
	

	self.pageTitleLabel = [UILabel autolayoutView];
	self.pageTitleLabel.textColor = [UIColor whiteColor];
	
	//	NSLog(@"%@",[UIFont fontNamesForFamilyName:@"Helvetica Neue"]);
	
	UIFont *titleFont = [UIFont fontWithName:@"HelveticaNeue" size:20.];
	pageTitleLabel.font = titleFont;
	[pageTitleLabel setTextAlignment:NSTextAlignmentCenter];
	
	[self.view addSubview:pageTitleLabel];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.pageTitleLabel
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeWidth
														 multiplier:0.8
														   constant:0]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.pageTitleLabel
														  attribute:NSLayoutAttributeCenterX
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeCenterX
														 multiplier:1.0
														   constant:0.0]];


	views = NSDictionaryOfVariableBindings(pageView,pageTitleLabel);
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageView]->=10-[pageTitleLabel]"
																	  options:0
																	  metrics:nil views:views]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageView]-<=20-[pageTitleLabel]"
																	  options:0
																	  metrics:nil views:views]];
	

	self.pageDescription  = [UITextView autolayoutView];
	self.pageDescription.textColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8];
	[self.pageDescription setTextAlignment:NSTextAlignmentCenter];
	self.pageDescription.backgroundColor = [UIColor clearColor];
	//self.pageDescription.numberOfLines = 4;			// автонастройка размеров
	UIFont *descFont = [UIFont fontWithName:@"HelveticaNeue" size:14.];
	self.pageDescription.font = descFont;
	
	[self.view addSubview:self.pageDescription];
	
	
	views = NSDictionaryOfVariableBindings(pageTitleLabel,pageDescription);

	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-20-[pageDescription]-20-|"
																	  options:0
																	  metrics:nil views:views]];

	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageTitleLabel(20.0)]-8-[pageDescription(74.0)]"
																	  options:0
																	  metrics:nil views:views]];

	self.pageIndicator = [UIPageControl autolayoutView];
	pageIndicator.numberOfPages = WalkThroughPages;
	pageIndicator.tintColor = [UIColor colorWithRed:170.0/255.0 green:05.0/255.0 blue:5.0/255.0 alpha:1.0];
	
	[self.view addSubview:pageIndicator];
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.pageIndicator
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeWidth
														 multiplier:1
														   constant:(WalkThroughPages * 20.0)]];
	
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.pageIndicator
														  attribute:NSLayoutAttributeCenterX
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeCenterX
														 multiplier:1.0
														   constant:0.0]];
	views = NSDictionaryOfVariableBindings(pageDescription,pageIndicator);
//	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageDescription]-<=15-[pageIndicator]"
//																	  options:0
//																	  metrics:nil views:views]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageDescription]->=5-[pageIndicator]"
																	  options:0
																	  metrics:nil views:views]];

	
//	self.regButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//	regButton.translatesAutoresizingMaskIntoConstraints = NO;
//	[regButton setTitle:RStr(@"Registration") forState:UIControlStateNormal];
//	regButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
//	
//	[self.regButton addTarget:self action:@selector(regButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//	
//	[regButton setTitleColor:[UIColor colorWithRed:210.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0] forState:UIControlStateNormal];
//	UIImage *backImage = [UIImage imageNamed:@"ButtonBackground.png"];
//	[regButton setBackgroundImage:backImage forState:UIControlStateNormal];
	
//	[self.view addSubview:self.regButton];
	
	enterButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	enterButton.translatesAutoresizingMaskIntoConstraints = NO;
	[enterButton setTitle:RStr(@"Enter") forState:UIControlStateNormal];
	[enterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	enterButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
	CALayer *l = [enterButton layer];
	l.backgroundColor = [UIColor orangeColor].CGColor;
	l.cornerRadius = 7.0;
	
	[self.view addSubview:enterButton];

	
	views = NSDictionaryOfVariableBindings(pageIndicator,enterButton);
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.enterButton
														  attribute:NSLayoutAttributeCenterX
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeCenterX
														 multiplier:1.0
														   constant:0.0]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-40-[enterButton]-40-|"
																	  options:0
																	  metrics:metrics views:views]];

	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageIndicator]-8-[enterButton(40)]-30-|"
																	  options:0
																	  metrics:metrics views:views]];
	
    [enterButton addTarget:self action:@selector(enterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

	
	
//    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
//    [nc addObserver:self selector:@selector(debuggedProfile:) name:WFClientProfileReceived object:nil];
//	[nc addObserver:self selector:@selector(findFriends:) name:WCFFindFriends object:nil];
//	[nc addObserver:self selector:@selector(switchToMainScreen:) name:WFSwitchToMainView object:nil];
}

- (CGFloat) delta:(float)aData
{
	CGRect applicationFrame = [UIScreen mainScreen].bounds;
	return (145.0)/1136.0 * applicationFrame.size.height;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self.nav setNavigationBarHidden:YES animated:NO];

	currentPage = -1;
	
	registrationButton.titleLabel.text = RStr(@"Registration");
	self.pageIndicator.numberOfPages = WalkThroughPages;
	
	
	[self updatePicture:nil];
	timer = [NSTimer scheduledTimerWithTimeInterval:WalkThroughTimerInterval target:self
										   selector:@selector(updatePicture:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updatePicture:(NSTimer *)aTimer
{
	currentPage++;
	if (currentPage >= WalkThroughPages) currentPage = 0;
	NSString *imageName = [NSString stringWithFormat:@"PictureST_%d",(int)(currentPage+1)];
	self.pageView.image = [UIImage imageNamed:imageName];
	NSString *tmp = [NSString stringWithFormat:@"WTTitle-%ld",(long)(currentPage+1)];
	self.pageTitleLabel.text = RStr(tmp);
	NSString *rtmp = [NSString stringWithFormat:@"WTText-%ld",(long)(currentPage+1)];
	self.pageDescription.text = RStr(rtmp);

	self.pageIndicator.currentPage = currentPage;
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)regButtonPressed:(id)sender
{
	if ([timer isValid]) {
		[timer invalidate];
		timer = nil;
	}
	
//	DumbRegistrationViewController *dumb = [[DumbRegistrationViewController alloc] init];
//	[self.navigationController pushViewController:dumb animated:YES];
	
}

- (IBAction) enterButtonPressed:(id)sender
{
	if ([timer isValid]) {
		[timer invalidate];
		timer = nil;
	}
//    DumbAuthorizationViewController *dumb = [[DumbAuthorizationViewController alloc] init];
//    [self.navigationController pushViewController:dumb animated:YES];
    
    [self.nav setNavigationBarHidden:NO animated:NO];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.navigationController.navigationBar.alpha = 1;
    });

	[self.view removeFromSuperview];
}

- (void) switchToMainScreen:(NSNotification *) note
{
//	MainViewViewController *mainController = [[MainViewViewController alloc] init];
//	[self.navigationController popToRootViewControllerAnimated:NO];
//	[self.navigationController pushViewController:mainController animated:NO];
}


- (void) findFriends:(NSNotification *) note
{
//	NSDictionary *dict = [note object];			// данные о текущем юзере
//	FindFriendsViewController *ff = [[FindFriendsViewController alloc] init];
//	[self.navigationController popToRootViewControllerAnimated:NO];
//	[self.navigationController pushViewController:ff animated:YES];

}

- (void) debuggedProfile:(NSNotification *) note
{
//    NSDictionary *dict = [note object];
//    TMPShowMyProfileViewController *tmp = [[TMPShowMyProfileViewController alloc] initWithDictionary:dict];
//    [self.navigationController popToRootViewControllerAnimated:NO];
//    [self.navigationController pushViewController:tmp animated:YES];
}


@end
