//
//  UIView+TakeSnapshot.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 24/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "UIView+TakeSnapshot.h"

@implementation UIView (TakeSnapshot)


- (UIImage *) takeSnapshot:(CGRect)frame
{
	UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0);
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	if (!ctx) {
		return nil;
	}
	CGContextTranslateCTM(ctx, frame.origin.x * -1, frame.origin.y * -1);
	[self.layer  renderInContext:ctx];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return image;
}



@end
