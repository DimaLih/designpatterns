//
//  GMFoldingCell.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "GMFoldingCell.h"
#import "GMRotatedView+AnimationDelegate.h"
#import "UIView+TakeSnapshot.h"


@interface GMFoldingCell () {
	//UIView *animationView;
	//NSMutableArray <NSDate *> *durationsForExpandedState;
	//NSMutableArray <NSDate *> *durationsForCollapsedState;
}
@property (nonatomic, retain) UIView *animationView;

@end



@implementation GMFoldingCell

//@synthesize animationView = animationView;

- (void) awakeFromNib
{
	[super awakeFromNib];
	[self commonInit];
}

/**
 Call this method in methods init(style: UITableViewCellStyle, reuseIdentifier: String?) after creating Views
 */

- (void) commonInit
{
	[self configureDefaultState];
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	
	CALayer *l = self.containerView.layer;
	l.cornerRadius = self.foregroundView.layer.cornerRadius;
	l.masksToBounds = YES;
	
	self.durationsForExpandedState = [NSMutableArray new];
	self.durationsForCollapsedState = [NSMutableArray new];
}

#pragma mark - Configuration

- (void) configureDefaultState
{
	NSAssert(self.foregroundViewTop || self.containerViewTop, @"At least one of constarints - foregroundViewTop or  containerViewTop should be set in XIB");

	self.containerViewTop.constant = self.foregroundViewTop.constant;
	self.containerView.alpha = 0.0;
	

    for (NSLayoutConstraint *cN in self.foregroundView.constraints) {
        if (cN.firstAttribute == NSLayoutAttributeHeight && cN.secondItem == nil) {
            self.foregroundView.layer.anchorPoint = CGPointMake(0.5, 1.0);
            self.foregroundViewTop.constant += cN.constant / 2;
            break;
        }
    }

    self.foregroundView.layer.transform = self.foregroundView.transform3D;

	[self createAnimationView];
	[self.contentView bringSubviewToFront:self.foregroundView];
}


- (NSArray <GMRotatedView *> *) createAnimationItemView
{
	NSMutableArray <GMRotatedView*> * items = [NSMutableArray new];
	[items addObject:self.foregroundView];
	NSMutableArray <GMRotatedView *> * rotatedViews = [NSMutableArray new];
	
	NSArray <GMRotatedView *> *sortedViews = [self.animationView.subviews sortedArrayUsingComparator:^NSComparisonResult(GMRotatedView *obj1, GMRotatedView *obj2) {
		if (obj1.tag < obj2.tag ) {
			return NSOrderedAscending;
		} else if (obj1.tag > obj2.tag) {
			return NSOrderedDescending;
		} else {
			return NSOrderedSame;
		}
	}];
	for (NSInteger i = 0; i < sortedViews.count; i++) {
        GMRotatedView * itemView = sortedViews[i];
        if ([itemView isKindOfClass:GMRotatedView.class]) {
            [rotatedViews addObject:itemView];
            GMRotatedView *backview = itemView.backView;
            if (backview) {
                [rotatedViews addObject:backview];
            }
        }
	}
    [items addObjectsFromArray:rotatedViews];
	return items;
}


- (void) configureAnimationItems:(AnimationType)animationType
{
    for (GMRotatedView *gv in self.animationView.subviews) {
        if ([gv isKindOfClass:GMRotatedView.class]) {
            if (animationType == AnimationTypeOpen) {
                gv.alpha = 0.0;
            } else {
                gv.alpha = 1.0;
                gv.backView.alpha = 0.0;
            }
        }
    }
}
	

- (void) createAnimationView
{
    self.animationView = [[UIView alloc] initWithFrame:self.containerView.frame];
	self.animationView.layer.cornerRadius = self.foregroundView.layer.cornerRadius;
	self.animationView.backgroundColor = [UIColor clearColor];
	self.animationView.translatesAutoresizingMaskIntoConstraints = NO;
	self.animationView.alpha = 0.0;
	
    [self.contentView addSubview:self.animationView];
	
    // copy constraints from containerView
	for (NSLayoutConstraint *constraint in self.contentView.constraints) {
		id firstItem = constraint.firstItem;
		id secondItem = constraint.secondItem;
		if (firstItem && [firstItem isKindOfClass:[UIView class]] && firstItem == self.containerView) {
			NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:self.animationView attribute:constraint.firstAttribute relatedBy:constraint.relation toItem:constraint.secondItem attribute:constraint.secondAttribute multiplier:constraint.multiplier constant:constraint.constant];
			if (newConstraint) {
                [self.contentView addConstraint:newConstraint];
			}
		} else if (firstItem && [firstItem isKindOfClass:[UIView class]] && secondItem && [secondItem isKindOfClass:[UIView class]] && secondItem == self.containerView) {
			NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:firstItem attribute:constraint.firstAttribute relatedBy:constraint.relation toItem:self.animationView attribute:constraint.secondAttribute multiplier:constraint.multiplier constant:constraint.constant];
			if (newConstraint) {
                [self.contentView addConstraint:newConstraint];
			}
		}
	}

    for (NSLayoutConstraint *constraint in self.containerView.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            UIView *item = constraint.firstItem;
            if (item && [item isKindOfClass:UIView.class] && item == self.containerView) {
                NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:self.animationView attribute:constraint.firstAttribute relatedBy:constraint.relation toItem:nil attribute:constraint.secondAttribute multiplier:constraint.multiplier constant:constraint.constant];
                if (newConstraint) {
                    [self.animationView addConstraint:newConstraint];
                }
            }
        }
    }
	
}


- (void)addImageItemsToAnimationView
{
	self.containerView.alpha = 1.0;
	CGSize containerViewSize = self.containerView.bounds.size;
	CGSize foregroundViewSize = self.foregroundView.bounds.size;
	
	// First object
	UIImage *image = [self.containerView takeSnapshot:CGRectMake(0.0, 0.0, containerViewSize.width, foregroundViewSize.height)];
	UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
	imageView.tag = 0;
	imageView.layer.cornerRadius = self.foregroundView.layer.cornerRadius;
	[self.animationView addSubview:imageView];
	
	// Second object
	image = [self.containerView takeSnapshot:CGRectMake(0.0, foregroundViewSize.height, containerViewSize.width, foregroundViewSize.height)];
    imageView = [[UIImageView alloc] initWithImage:image];
	GMRotatedView *rotatedview = [[GMRotatedView alloc] initWithFrame:imageView.frame];
	rotatedview.tag = 1;
	rotatedview.layer.anchorPoint = CGPointMake(0.5, 0.0);
	rotatedview.layer.transform = [rotatedview transform3D];
	[rotatedview addSubview:imageView];
	[self.animationView addSubview:rotatedview];
	rotatedview.frame = CGRectMake(imageView.frame.origin.x,
								   foregroundViewSize.height,
								   containerViewSize.width,
								   foregroundViewSize.height);
	// add other views
	double itemHeight = (containerViewSize.height - 2.0 * foregroundViewSize.height) / (@(self.itemCount).floatValue - 2);
	if (self.itemCount == 2) {
		// decrease containerView height or increase itemCount
		NSAssert(containerViewSize.height - 2 * foregroundViewSize.height == 0,
				 @"_containerView.height is too high");
	} else {
		// decrease containerView height or increase itemCount
		NSAssert(containerViewSize.height - 2 * foregroundViewSize.height >= itemHeight,
				 @"contanerView.height too high");
	}
	double yPosition = 2.0 * foregroundViewSize.height;
	NSInteger tag = 2;
	for (NSInteger i = 2; i < self.itemCount; i++) {
		image = [self.containerView takeSnapshot:CGRectMake(0, yPosition, containerViewSize.width, itemHeight)];
		UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
		GMRotatedView *rotatedView = [[GMRotatedView alloc] initWithFrame:imageView.frame];
		[rotatedView addSubview:imageView];
		rotatedView.layer.anchorPoint = CGPointMake(0.5, 0.0);
		rotatedView.layer.transform = rotatedView.transform3D;
		[self.animationView addSubview:rotatedView];
		rotatedView.frame = CGRectMake(0, yPosition,
									   rotatedView.bounds.size.width,
									   itemHeight);
        rotatedView.tag = tag;
        tag++;
		yPosition += itemHeight;
	}
	self.containerView.alpha = 0.0;
	
	// Add back view
	NSArray <GMRotatedView *> *sorted = self.animationView.subviews;
    sorted = [sorted sortedArrayUsingComparator:^NSComparisonResult(UIView *obj1, UIView *obj2) {
        if (obj1.tag < obj2.tag) {
            return NSOrderedAscending;
        } else if (obj1.tag > obj2.tag) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];

    GMRotatedView *previousView = nil;

    for (NSInteger i = 0; i < sorted.count; i++) {
        GMRotatedView * container = sorted[i];
        //NSLog(@"------->>>>>> %ld - %ld",i, container[i].tag);
		if ([container isKindOfClass:GMRotatedView.class] && container.tag > 0 && container.tag < sorted.count) {
            //NSLog(@"------->>>>>>%@",previousView);
            if (previousView) {
                [previousView addBackViewWithHeight:container.bounds.size.height andColor:self.backViewColor];
            }
			previousView = container;
		}
	}
	self.animationItemViews = [[NSMutableArray alloc] initWithArray:[self createAnimationItemView]];
}

- (void) removeImageItemsFromAnimationView
{
	if (!self.animationView) {
		return;
	}
	for (UIView *sView in self.animationView.subviews) {
		[sView removeFromSuperview];
	}
}

/// Unfold cell.
///
/// - Parameters:
///   - value: unfold = true; collapse = false.
///   - animated: animate changes.
///   - completion: A block object to be executed when the animation sequence ends.
	
-(void) unfold:(BOOL) unfold animated:(BOOL) animated completion:(void (^ _Nullable)(void))completion
{
	if (animated) {
		if (unfold) {
			[self openAnimation:completion];
		} else {
			[self closeAnimation:completion];
		}
	} else {
		self.foregroundView.alpha = unfold ? 0.0 : 1.0;
		self.containerView.alpha = unfold ? 1.0 : 0.0;
	}
}
	
- (BOOL) isAnimating
{
	return (self.animationView.alpha == 1.0);
}
	
- (NSTimeInterval) animationDurationForIndex:(NSInteger) itemIndex
									withType:(AnimationType) type
{
	if (type == AnimationTypeClose) {
        if (itemIndex >= self.durationsForCollapsedState.count) return 0;
		return [self.durationsForCollapsedState[itemIndex] timeIntervalSince1970];
	} else {
        if (itemIndex >= self.durationsForExpandedState.count) return 0;
		return [self.durationsForExpandedState[itemIndex] timeIntervalSince1970];
	}
}
	
- (NSArray <NSDate *> *) durationSequenceForType:(AnimationType) type
{
	NSMutableArray <NSDate *> *durations = [NSMutableArray new];
	for (NSInteger i = 0; i < self.itemCount; i++) {
		NSTimeInterval duration = [self animationDurationForIndex:i withType:type];
		[durations addObject:[NSDate dateWithTimeIntervalSince1970:(duration / 2.0)]];
		 [durations addObject:[NSDate dateWithTimeIntervalSince1970:(duration / 2.0)]];
	  }
	return durations;
 }


- (void) openAnimation:(void (^ __nullable)(void))completion
{
	self.isUnfolded = YES;
	[self removeImageItemsFromAnimationView];
	[self addImageItemsToAnimationView];
	
	self.animationView.alpha = 1.0;
	self.containerView.alpha = 0.0;
	NSArray <NSDate *> *durations = [self durationSequenceForType:AnimationTypeOpen];
	NSTimeInterval delay = 0.0;
	NSString *timing = kCAMediaTimingFunctionEaseIn;
	CGFloat from = 0.0;
	CGFloat to = -M_PI/2.0;
	BOOL hidden = YES;
	[self configureAnimationItems:AnimationTypeOpen];
	if (!self.animationItemViews) {
		return;
	}
	for (NSInteger index = 0; index < self.animationItemViews.count; index++) {
		GMRotatedView *animatedView = self.animationItemViews[index];
		NSTimeInterval dt = [durations[index] timeIntervalSince1970];
		[animatedView foldingAnimation:timing from:from to:to duration:dt delay:delay hidden:hidden];
		from = (from == 0.0) ? M_PI/2.0 : 0.0;
		to = (to == 0.0) ? -M_PI/2.0 : 0.0;
		timing = (timing == kCAMediaTimingFunctionEaseIn) ? kCAMediaTimingFunctionEaseOut : kCAMediaTimingFunctionEaseIn;
        hidden = !hidden;
		delay += [durations[index] timeIntervalSince1970];
	}
	
//    NSArray <GMRotatedView *> *sortedViews = [self.animationView.subviews sortedArrayUsingComparator:^NSComparisonResult(GMRotatedView *obj1, GMRotatedView *obj2) {
//        if (obj1.tag < obj2.tag) {
//            return NSOrderedAscending;
//        } else if (obj1.tag > obj2.tag) {
//            return NSOrderedDescending;
//        } else {
//            return NSOrderedSame;
//        }
//    }];
    
	//NSAssert(sortedViews.count > 0, @"subvies array should not be empty!");
    for (int i = 0; i < self.animationView.subviews.count; i++) {
        UIView *firstItemView = self.animationView.subviews[0];
        if (firstItemView.tag == 0) {
            firstItemView.layer.masksToBounds = YES;
            NSTimeInterval startDelay = [durations[0] timeIntervalSince1970];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(startDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                firstItemView.layer.cornerRadius = 0.0;
            });
            break;
        }
    }

	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		self.animationView.alpha = 0.0;
        self.containerView.alpha = 1.0;
		if (completion) completion();
	});
}

- (void) closeAnimation:(void (^ __nullable)(void))completion
{
	self.isUnfolded = NO;
	[self removeImageItemsFromAnimationView];
	[self addImageItemsToAnimationView];
	
	NSAssert(self.animationItemViews, @"animationItemViews should not be nil!");
	NSAssert(self.animationItemViews.count > 0, @"animationItemViews should not be rmpty!");

	if (!self.animationItemViews) {
		NSLog(@"Fatal error!");
		return;
	}
	self.animationView.alpha = 1.0;
	self.containerView.alpha = 0.0;
	NSArray <NSDate *> *ddd = [self durationSequenceForType:AnimationTypeClose];
	// reverse array
	NSArray* durations = [[ddd reverseObjectEnumerator] allObjects];
	
	NSTimeInterval delay = 0.0;
	NSString *timing = kCAMediaTimingFunctionEaseIn;
	CGFloat from = 0.0;
	CGFloat to = M_PI/2.0;
	BOOL hidden = YES;
	[self configureAnimationItems:AnimationTypeClose];
	
	if (durations.count < self.animationItemViews.count) {
		NSLog(@"Invalid override fr method animationDuration(itemIndex:NSInteger, type:AnimationType");
		return;
	}
	NSArray <GMRotatedView *> *animatedItemsRotated = [[self.animationItemViews reverseObjectEnumerator] allObjects];
	for (NSInteger index = 0; index < self.animationItemViews.count; index++) {
		GMRotatedView *animatedView = animatedItemsRotated[index];
		NSTimeInterval duration = [durations[index] timeIntervalSince1970];
		[animatedView foldingAnimation:timing from:from to:to duration:duration delay:delay hidden:hidden];
		to = (to == 0.0 ? M_PI/2 : 0.0);
		from = (from == 0.0 ? -M_PI/2 : 0.0);
		timing = (timing == kCAMediaTimingFunctionEaseIn ? kCAMediaTimingFunctionEaseOut : kCAMediaTimingFunctionEaseIn);
		hidden = !hidden;
		delay += [durations[index] timeIntervalSince1970];
	}
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		self.animationView.alpha = 0.0;
		if (completion) completion();
	});
//    NSArray <GMRotatedView *> *sortedViews = [self.animationView.subviews sortedArrayUsingComparator:^NSComparisonResult(GMRotatedView *obj1, GMRotatedView *obj2) {
//        if (obj1.tag < obj2.tag) {
//            return NSOrderedAscending;
//        } else if (obj1.tag > obj2.tag) {
//            return NSOrderedDescending;
//        } else {
//            return NSOrderedSame;
//        }
//    }];
	//NSAssert(sortedViews.count > 0, @"subvies array should not be empty!");
    UIView *firstItemView;
    for (int i = 0; i < self.animationView.subviews.count; i++) {
        firstItemView = self.animationView.subviews[0];
        if (firstItemView.tag == 0) {
            firstItemView.layer.cornerRadius = 0.0;
            firstItemView.layer.masksToBounds = YES;
            NSTimeInterval durationFirst = [durations[0] timeIntervalSince1970];
            NSTimeInterval after = delay - durationFirst * 2.0;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(after * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                firstItemView.layer.cornerRadius = self.foregroundView.layer.cornerRadius;
                [firstItemView setNeedsDisplay];
                [firstItemView setNeedsLayout];
                //NSLog(@"👿 👿 👿 👿 👿 👿 👿 👿 👿 👿 👿 👿 👿 👿");
            });
            break;
        }
    }

}


@end
