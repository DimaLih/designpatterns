//
//  SetupCell.h
//  Maple
//
//  Created by Водолазкий В.В. on 04.09.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    TableTagCoin,
    TableTagLanguage,
    TableTagWalk,
    TableTagTheme,
} TableTag;

extern NSString * const SetupCellReusableID;
extern NSString * const LanguageCellReusableID;

@interface SetupCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIStackView *stackViewLang;
@property (readwrite)  BOOL expandedCell;

- (void) setData:(NSArray *)data;

@end
