//
//  AppDelegate.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 28.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const GOOGLEAD_APP_ID;
extern NSString * const GOOGLEAD_BANNER_ID;
extern NSString * const GOOGLEAD_REWARD_ID;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

