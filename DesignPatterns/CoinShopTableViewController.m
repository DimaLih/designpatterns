//
//  CoinShopTableViewController.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "CoinShopTableViewController.h"
#import "Preferences.h"
#import "CellForCoinShop.h"
//#import "IAPHelper.h"
#import "IAPShare.h"
#import "Utils.h"
#import "FCAlertView.h"

#define NUMBERS_STATIC_CELLS 3

@interface CoinShopTableViewController () {
    //IAPHelper *iapHelper;
    //NSArray * products;
    NSMutableArray *allProducts;
    IAPHelper *iap;
    FCAlertView *alert;
    NSArray *productIDs;
}

@end

@implementation CoinShopTableViewController

-(void)vihod:(id)sender {
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self dismissViewControllerAnimated:YES completion:NULL];
//    } else {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

	self.title = RStr(@"Coin Shop");
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

#define iAPP_export10       @"geomatix.cz.DPT.export10"
#define iAPP_export25       @"geomatix.cz.DPT.export25"
#define iAPP_exportForever  @"geomatix.cz.DPT.exportForever"
#define iAPP_blockAd        @"geomatix.cz.DPT.blockAd"
#define iAPP_Subscr_3m      @"geomatix.cz.DesignPatterns.3month" // Shared Secret eacc5fd3518e4f989b8a94abb4e1d776 

    
    productIDs = @[ iAPP_export10,
                    iAPP_export25,
                    iAPP_exportForever,
                    iAPP_blockAd ];
    allProducts = [[NSMutableArray alloc] initWithCapacity:productIDs.count];
    if(!iap) {
        
        iap = [[IAPHelper alloc] initWithProductIdentifiers:[NSSet setWithArray:productIDs]];
#ifdef DEBUG
        iap.production = NO; // Играем в песочнице
#else
        iap.production = YES;
#endif
        
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (![Utils  hasInternetBezAlerta]) {
        DLog(@"Нет соединения с интернетом!");
        alert = [[FCAlertView alloc] init];
        [alert makeAlertTypeWarning];
        alert.colorScheme = alert.flatRed;
        [alert showAlertWithTitle:nil withSubtitle:RStr(@"INTERNET_IS_NOT") withCustomImage:nil withDoneButtonTitle:RStr(@"Close") andButtons:nil];
        [alert doneActionBlock:^{
            [self vihod:nil];
        }];
    } else {

        // Загрузим список покупок с эпла
        dispatch_async(dispatch_get_main_queue(), ^{
            self->alert = [[FCAlertView alloc] init];
            //alert.customImageScale = 1.2;
            self->alert.colorScheme = self->alert.flatBlue;
            self->alert.hideDoneButton = YES;
            [self->alert makeAlertTypeProgressBig];
            [self->alert showAlertInView:self withTitle:RStr(@"Acces to AppStore...") withSubtitle:RStr(@"PlaceWaitBays") withCustomImage:nil withDoneButtonTitle:nil andButtons:nil];
        });

        [iap requestProductsWithCompletion:^(SKProductsRequest* request,SKProductsResponse* response)
         {
             [self->allProducts addObjectsFromArray:
              @[[@{@"productID" : self->productIDs[0],
                   @"icon"        : @"Moneta10",
                   @"numbers"     : @10,
                   @"nameItem"    : RStr(@"10 Coin"),
                   @"opisanie"    : RStr(@"10 Coin Opisanie"),
                   @"price"       : [NSString stringWithFormat:RStr(@"%@"),RStr(@"...")], //RStr(@"$ %@"),RStr(@"1")// Предлагаю цену не писать, а грузить от эпла
                   @"buttonText"  : RStr(@"Purhase")} mutableCopy],
                
                [@{@"productID"   : self->productIDs[1],
                   @"icon"        : @"Moneta25",
                   @"numbers"     : @25,
                   @"nameItem"    : RStr(@"25 Coin"),
                   @"opisanie"    : RStr(@"25 Coin Opisanie"),
                   @"price"       : [NSString stringWithFormat:RStr(@"%@"),RStr(@"...")],
                   @"buttonText"  : RStr(@"Purhase")} mutableCopy],
                
                [@{@"productID"   : self->productIDs[2],
                   @"nameItem"    : RStr(@"100000000 Coin"),
                   @"numbers"     : @COINSMAX,
                   @"icon"        : @"MonetaBes",
                   @"opisanie"    : RStr(@"100000000 Coin Opisanie"),
                   @"price"       : [NSString stringWithFormat:RStr(@"%@"),RStr(@"...")],
                   @"buttonText"  : RStr(@"Purhase")} mutableCopy],
                
                [@{@"productID"   : self->productIDs[3],
                   @"nameItem"    : RStr(@"Block Ad's"),
                   @"numbers"     : @TAGBLOCKAD,
                   @"icon"        : @"blockAd",
                   @"opisanie"    : RStr(@"Block Ad's Opisanie"),
                   @"price"       : [NSString stringWithFormat:RStr(@"%@"),RStr(@"...")],
                   @"buttonText"  : RStr(@"Purhase")} mutableCopy]]];

             if (self->iap.products.count != 4) {
                 NSLog(@"Список покупок не равен 4! Что то пошло не так?");
             }
             //products = response.products; // Должно быть четыре покупки
             DLog(@"Loaded list of products...");
             if (response.invalidProductIdentifiers.count > 0) {
                 NSLog(@" ⁉️ invalidIdentifiers in CoinShop: %@",response.invalidProductIdentifiers);
             }
             for (SKProduct* product in self->iap.products) {
                 NSString * title = product.localizedTitle;
                 NSString * opis = product.localizedDescription;
                 NSString * price = [self->iap getLocalePrice:product];
                 NSString * prodId = product.productIdentifier;
                 if (prodId.length > 0 && price.length > 0 && title.length > 0 && opis.length > 0 && product.productIdentifier.length > 0) {
                     for (int i = 0; i < self->allProducts.count; i++) { // некрасиво конечно но быстро:)
                         if ([self->allProducts[i][@"productID"] isEqualToString:prodId]) {
                             self->allProducts[i][@"nameItem"] = title;
                             self->allProducts[i][@"opisanie"] = opis;
                             self->allProducts[i][@"price"] = price;
                         }
                     }
                 }
             }
             [self startRomashka:NO]; // ромашка стоп

             DLog(@"%@",self->allProducts);
             
             
             [self refreshTable];
         }];

    }
    
    // смена темы
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(coinChanged:)  name:VVVexportCoinUsed object:nil];
    
    [self themeChanged:nil];


}
- (void) refreshTable {
    [self.tableView reloadData];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:VVVcurrentThemeChanged object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:VVVexportCoinUsed object:nil];
}

- (void) themeChanged:(NSNotification *) note
{
    Preferences *prefs = [Preferences sharedPreferences];
    UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];;
    UIColor *navBackColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    
    self.navigationController.navigationBar.barTintColor = navBackColor;
    self.navigationController.navigationBar.tintColor = textColor;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:textColor}];

    self.tableView.backgroundColor = navBackColor;
    [self.tableView reloadData];
}

//- (void) coinChanged:(NSNotification *) note
//{
//    // После обновления монет уходим из магазина
//    // Может сообщение выдать? но мне кажется не стОит...
//    //[self vihod:nil];
//}



- (IBAction)btRestorePressed:(id)sender
{
    if (![Utils hasInternet]) { // Если вдруг интернета нет, то не пускаем сюда
        return;
    }

    // Нажали кнопку Восстановить покупки
    [self startRomashka:YES];
    
    DLog(@"Пытаемся Восстановить покупки...");
    [iap restoreProductsWithCompletion:^(SKPaymentQueue *payment, NSError *error) {
        //check with SKPaymentQueue
        if (payment.transactions.count > 0) {
            DLog(@"Незавершенных транзакций: payment.transactions.count = %lu",(unsigned long)payment.transactions.count);
            for (SKPaymentTransaction *transaction in payment.transactions) {
                NSString *purchased = transaction.payment.productIdentifier;
                DLog(@"purchased = %@",purchased);
                if(purchased.length > 0) {
                    if ([purchased isEqualToString:iAPP_exportForever]) {
                        // Уже купили 100500 монет, что делаем? закрываем экран магазина?
                        // Надо обновить кнопки на всех экранах, пошлём им пуш?
                        DLog(@"Восстановлено: %@ \n (ошибок: %@)",payment,error);
                        [[Preferences sharedPreferences] addEternityExports];
                        [self vihod:nil];
                    } else if ([purchased isEqualToString:iAPP_blockAd]) {
                        DLog(@"Восстановлено: %@ \n (ошибок: %@)",payment,error);
                        [[Preferences sharedPreferences] setAdIsOff:YES];
                        [self vihod:nil];
                    }
                }
            }
        } else {
             DLog(@"Ещё ничего не куплено ранее!");
        }
        [self startRomashka:NO]; // ромашка стоп
    }];
    
}
- (IBAction)btBayPressed:(id)sender
{
    if (![Utils hasInternet]) { // Если вдруг интернета нет, то не пускаем сюда
        return;
    }
    
    [self startRomashka:YES];
    NSInteger index = 0;
    switch ([sender tag]) {
        case 10          : index = 0; break;
        case 25          : index = 1; break;
        case COINSMAX    : index = 2; break;
        case TAGBLOCKAD  : index = 3; break;
    }
    
    [iap requestProductsWithCompletion:^(SKProductsRequest* request,SKProductsResponse* response)
     {
         if(response.products.count > 0  && self->iap.products.count > 0) {
             if (index >= self->iap.products.count) {
                 DLog(@"‼️ Что то не таК!");
                 [self startRomashka:NO]; // ромашка стоп
                 return;
             }
             SKProduct* product = self->iap.products[index];
             DLog(@"Пытаемся купить: %@ : %@",product.localizedTitle, [self->iap getLocalePrice:product]);
             
             [self->iap buyProduct:product onCompletion:^(SKPaymentTransaction* trans) {
                 
                 if(trans.error) {
                     NSLog(@"🕷Ошибка: %@",[trans.error localizedDescription]);
                 } else if(trans.transactionState == SKPaymentTransactionStatePurchased) {
                     
                     [self->iap checkReceipt:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]] AndSharedSecret:@"your sharesecret" onCompletion:^(NSString *response, NSError *error) {
                         
                         // Сделаем небольшую защиту от пионеров
                         //Convert JSON String to NSDictionary
                         NSDictionary* rec = [IAPShare toJSON:response];
                         
                         if([rec[@"status"] integerValue]==0) {
                             //[iap provideContentWithTransaction:trans];
                             DLog(@"SUCCESS %@",response);
                             //DLog(@"Pruchases %@",iap.purchasedProducts);
                             if (rec[@"receipt"]) {
                                 if (rec[@"receipt"][@"bundle_id"]) {
                                     NSString *bi = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
                                     if ([rec[@"receipt"][@"bundle_id"] isEqualToString:bi]) {
                                         for (NSDictionary *d in rec[@"receipt"][@"in_app"]) {
                                             // TODO: на самом деле могут быть незавершенные транзакции, надо бы разобраться потом, что с ними делать
                                             DLog(@"🦀 d[@\"product_id\"]=%@",d[@"product_id"]);
                                             if ([d[@"product_id"] isEqualToString:iAPP_export10]) {
                                                 [[Preferences sharedPreferences] add10Exports];
                                                 [self vihod:nil];
                                                 break;
                                             } else if ([d[@"product_id"] isEqualToString:iAPP_export25]) {
                                                 [[Preferences sharedPreferences] add10Exports];
                                                 [self vihod:nil];
                                                 break;
                                             } else if ([d[@"product_id"] isEqualToString:iAPP_exportForever]) {
                                                 [[Preferences sharedPreferences] addEternityExports];
                                                 [self vihod:nil];
                                                 break;
                                             } else if ([d[@"product_id"] isEqualToString:iAPP_blockAd]) {
                                                 [[Preferences sharedPreferences] setAdIsOff:YES];
                                                 [self vihod:nil];
                                                 break;
                                             }

                                         }
                                     }
                                 }
                             }
                             
                         }
                         else {
                             NSLog(@"🕷🕷Ошибка");
                         }
                     }];
                 } else if(trans.transactionState == SKPaymentTransactionStateFailed) {
                     NSLog(@"🕷🕷🕷Ошибка");
                 }
                 [self startRomashka:NO]; // ромашка стоп
             }];//end of buy product
         }
     }];
    
}
-(void)startRomashka:(BOOL)yesNo { // индикатор активности сети
    
    // анимацию сделать с ромашкой или еще с чем, чтобы было понятно, почему заблокирована таблица
    if (yesNo == YES) {
        self.tableView.userInteractionEnabled = NO;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self->alert = [[FCAlertView alloc] init];
            //alert.customImageScale = 1.2;
            self->alert.colorScheme = self->alert.flatBlue;
            self->alert.hideDoneButton = YES;
            //alert.titleWite = RStr(@"Acces to AppStore...");
            [self->alert makeAlertTypeProgressBig];
            [self->alert showAlertInView:self withTitle:RStr(@"Acces to AppStore...") withSubtitle:RStr(@"PleaseWaitPurchase") withCustomImage:nil withDoneButtonTitle:nil andButtons:nil];
//            [alert addButton:RStr(@"Close") withActionBlock:^{
//                // При принудительном закрытии может что нибудь сделаем?
//                // Типа прервем все транзакции
//                // Илии вообще убрать эту кнопку.
//                // Осмыслить...
//            }];

            //[alert makeAlertSpisokViborov:@[RStr(@"Одну запись"),RStr(@"Выбранный раздел"), RStr(@"Всё (медленно!)")] complete:^(NSInteger nomer, NSString * btText) {
                // nomer = 0 Одна запись // 1 Текущий раздел // 2 Всё
                //if ([btText isEqualToString:RStr(@"Напечатать")]) {
//                    ReportViewController *report = [[ReportViewController alloc] init];
//                    report.regim = nomer;
//                    [report printPDF];
                //} else if ([btText isEqualToString:RStr(@"Отправить")]) {
//                    ReportViewController *report = [[ReportViewController alloc] init];
//                    report.regim = nomer;
//                    NSString *path = [report pathFromFilePDF];
//                    // Экспорт PDF куда нибудь. На симуляторе это не работает, только на девайсе
//                    UIDocumentInteractionController * docController = [[UIDocumentInteractionController alloc] init];
//                    docController.URL = [NSURL fileURLWithPath:path];
//                    [docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
                //} // if ([btText isEqualToString
            //}]; //[alert makeAlertSpisokViborov
        });
        
    } else {
        self.tableView.userInteractionEnabled = YES;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->alert dismissAlertView];
            self->alert = nil;
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allProducts.count + NUMBERS_STATIC_CELLS; // 3 - Список покупок + Описание + Восстановить + Копилка
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 160;
    } else if (indexPath.row == allProducts.count + NUMBERS_STATIC_CELLS - 1) {
        return 44;
    }
    return 76;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    CellForCoinShop *cell;

    if (indexPath.row == 0) { // Описание
        cell = (CellForCoinShop*)[tableView dequeueReusableCellWithIdentifier:@"Cell_Opis" forIndexPath:indexPath];
        cell.opisanie.text = RStr(@"Explain coins");
    } else
    if (indexPath.row == 1) { // Монетки которые имеем
        cell = (CellForCoinShop*)[tableView dequeueReusableCellWithIdentifier:@"Cell_Have" forIndexPath:indexPath];
        cell.iconView.image = [UIImage imageNamed:@"Moneta"];
        NSInteger coins = [Preferences sharedPreferences].exportCoins;
        if (coins >= COINSMAX) {
            cell.nameItem.text = RStr(@"CoinsForever");
            cell.coins.text = RStr(@"∞");
        } else if (coins == 0) {
            cell.nameItem.text = RStr(@"NoCoins");
            cell.coins.text = @"0";
        } else {
            cell.nameItem.text = [NSString stringWithFormat:RStr(@"RemainedCoiunsFormat"),coins];
            cell.coins.text = [NSString stringWithFormat:RStr(@"%ld"),(long)coins];
        }
    } else
    if (indexPath.row == allProducts.count + 2) { // Восстановить покупки
        cell = (CellForCoinShop*)[tableView dequeueReusableCellWithIdentifier:@"Cell_Restore" forIndexPath:indexPath];
        [cell.buttonBay setTitle:RStr(@"Restore The Previously Purchased") forState:UIControlStateNormal];

    } else
    if (allProducts.count > 0) {  // Покупки
        cell = (CellForCoinShop*)[tableView dequeueReusableCellWithIdentifier:@"Cell_Bay" forIndexPath:indexPath];
        NSDictionary * dict = allProducts[indexPath.row - 2];
        
        [cell.buttonBay setTitle:RStr(@"Purchase") forState:UIControlStateNormal];
        cell.buttonBay.tag = [dict[@"numbers"] integerValue]; // d tag = количество монеток
        cell.iconView.image = [UIImage imageNamed:dict[@"icon"]]; // RStr(@"3 Coins for Export");
		//
		// После того как список покупок загружен - испльзуем локальное описание. И надо бы три картинки
		// для каждой покупки - моннетка с 10, 25 и бесконесностью - размером 1024-1024 для Appstore и
		// маленькоие - сюда как иконки
        // OK
		//
        // Далее инфу будем брать Из ЭплСтора или локально?
        cell.nameItem.text = dict[@"nameItem"]; // RStr(@"3 Coins for Export");
        cell.opisanie.text = dict[@"opisanie"]; // RStr(@"DescriptionThis");
        cell.price.text = dict[@"price"]; // [NSString stringWithFormat:@"%@",@"25 руб."];
                
    }
    
    return cell;
}

//- (NSString *) remainedCoinsStr
//{
//	NSInteger coins = [Preferences sharedPreferences].exportCoins;
//	if (coins >= COINSMAX) {
//		return RStr(@"CoinsForever");
//	} else if (coins > 0) {
//		return [NSString stringWithFormat:RStr(@"RemainedCoiunsFormat"), coins];
//	} else {
//		return RStr(@"NoCoins");
//	}
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





@end
