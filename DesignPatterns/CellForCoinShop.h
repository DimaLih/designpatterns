//
//  CellForCoinShop.h
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 19.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForCoinShop : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel    * coins;
@property (weak, nonatomic) IBOutlet UILabel    * nameItem;
@property (weak, nonatomic) IBOutlet UILabel    * price;
@property (weak, nonatomic) IBOutlet UITextView * opisanie;
@property (weak, nonatomic) IBOutlet UIButton   * buttonBay;
@property (weak, nonatomic) IBOutlet UIImageView   * iconView;

@end
