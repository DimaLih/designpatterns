//
//  Utils.m
//  MobilnyBalansSmart
//
//  Created by Dmitry Likhtarov on 10.10.13.
//  Copyright (c) 2013 Dmitry Likhtarov. All rights reserved.
//

#import "Utils.h"
#import <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <UIKit/UIKit.h>
#import "FCAlertView.h"
#import "Preferences.h"




@implementation Utils

#pragma mark -
#pragma mark Проверка на сеть, вай фай и тд

//Проверить, есть ли Интернет
+(BOOL)hasInternet {
//#warning Надо бы проверить на пинг!!!
    BOOL otvet=NO;
    otvet=[Utils hasInternetBezAlerta];
    if (!otvet) { // Если сети нет то  Убьем все алерты и выведем мессадж
//        UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
//        while (topVC.presentedViewController) {
//            topVC = topVC.presentedViewController;
//            if ([topVC isKindOfClass:[UIViewController class]]) {
//                [topVC dismissViewControllerAnimated:NO completion:nil];
//            }
//        }
        
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ошибка сети" message:[NSString stringWithFormat:@"Интернет недоступен. Если используется Сотовая сеть, то проверьте, разрешено ли программе ее использовать в настройке телефона(планшета) (нажмите кнопку \"Home\", найдите иконку \"Настройки\", далее: \"Сотовые данные\", найдите группу \"Сотовые данные для ПО\" и включите переключатель для %@ программы)",@"этой"] preferredStyle:UIAlertControllerStyleAlert];
//        [alertController addAction:[UIAlertAction actionWithTitle:@"Закрыть" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//        }]];
//        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:alertController animated:YES completion:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            FCAlertView *alert = [[FCAlertView alloc] init];
            [alert makeAlertTypeWarning];
            alert.colorScheme = alert.flatRed;
            
            [alert showAlertInWindow:[[[UIApplication sharedApplication] delegate] window] withTitle:nil withSubtitle:RStr(@"INTERNET_IS_NOT") withCustomImage:nil withDoneButtonTitle:RStr(@"Close") andButtons:nil];
//            [alert doneActionBlock:^{
//                [self reloadTableAfterDeleteObject:categ];
//            }];
        });

        
    }
    return otvet;
}
+(BOOL)hasInternetBezAlerta {
    BOOL otvet=NO;
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if(!didRetrieveFlags) {
        //        otvet=NO;
        //return NO; // Сети нет никакой
    }
    // Есть или нету сети в принципе
    otvet = ( (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired) ) ? YES : NO;

    return otvet;
}

@end

