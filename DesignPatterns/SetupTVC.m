//
//  SetupTVC.m
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 24.09.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "SetupTVC.h"
#import "Preferences.h"
#import "CoinShopTableViewController.h"
#import "SetupCell.h"
#import "ThemeSelectorViewController.h"
#import "UIViewController+LMSideBarController.h"
#import "MainViewController.h"
#import "SDVersion.h"

//#define HEIHT_BUTTON 30

@interface SetupTVC () <UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate>
{
    Preferences * prefs;
    NSArray *menu;
    ThemeSelectorViewController *themeViewController;
    NSMutableArray *backgorunds;
    NSMutableArray *foregrounds;
}

@property (weak, nonatomic) IBOutlet UIButton *buttonLogo;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConstraintViewLogoHeight;

@end

@implementation SetupTVC

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([SDVersion deviceSize] == Screen4inch) {
        self.constraintWith.constant = 220;
        self.ConstraintViewLogoHeight.constant = 20;
        [self.buttonLogo removeFromSuperview];
        [self.versionLabel removeFromSuperview];
    } else {
        self.constraintWith.constant = 250;
        self.ConstraintViewLogoHeight.constant = 100;
    }

    prefs = [Preferences sharedPreferences];

    [self themeChanged:nil];
    
//    CALayer *l = self.iconImageView.layer;
//    l.cornerRadius = self.iconImageView.frame.size.width / 2.0;
//    l.borderColor = [UIColor yellowColor].CGColor;
//    l.borderWidth = 1.5;
    //
    // Constract version number
    //
    NSString *revision = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"Version %@", revision];
    //NSString *buildNumber = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    //self.versionLabel.text = [NSString stringWithFormat:@"Version %@ (%@)", revision, buildNumber];
   
    // Строки 0 секции таблицы
    menu = @[
             @[ RStr(@"Coin Shop"),          @"bt_Coin",       @(TableTagCoin)     ],
             @[ RStr(@"Show start screen"),  @"appicon180",    @(TableTagWalk)     ],
             //@[ RStr(@"Default Language"),   @"logoM_objc",    @(TableTagLanguage) ],
             //@[ RStr(@"Theme select"),       @"SettingsIcon",  @(TableTagTheme)    ],
             ];
    
    // Для выбора темы в секции 1:
    backgorunds = [NSMutableArray new];
    foregrounds = [NSMutableArray new];
    for (RegexHighlightViewTheme i = kRegexHighlightViewThemeBasic; i < kRegexHighlightViewThemesCount; i++) {
        [backgorunds addObject:[prefs backgroundForTheme:i]];
        [foregrounds addObject:[prefs foregroundForTheme:i]];
    }
    
    self.cellLanguageExpanded = NO;

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
    [nc addObserver:self selector:@selector(themeChanged:) name:VVVcurrentLangProgChanged object:nil];
    [nc addObserver:self selector:@selector(preferredFontChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];

}

- (void) preferredFontChanged:(NSNotification *)notification
{
    // При изменениеи масштабирования шрифтов
    [self.tableView reloadData];
}

- (void) themeChanged:(NSNotification *) note
{
    
    UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];;
    //UIColor *backColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    UIColor *backColor = [prefs darkerColorForColor:prefs.currentThemeColors[kRegexHighlightViewTypeButton]];
    UIColor *navBackColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    
    self.versionLabel.textColor = textColor;
    self.buttonLogo.tintColor = [UIColor colorWithWhite: (prefs.themeIsLight)?0.1:0.9 alpha:1.0];
    self.tableView.backgroundColor = backColor;
    self.view.backgroundColor = backColor;
    [self.tableView reloadData];
    
    self.navigationController.navigationBar.barTintColor = navBackColor;
    self.navigationController.navigationBar.tintColor = textColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:textColor}];
    
}

#pragma mark - Table view
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return menu.count;
    } else if (section == 1) { // Выбор языка програмирования
        return 1;
    }
    return backgorunds.count;
}
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.0001f;
    }
    return 30;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && [SDVersion deviceSize] == Screen4inch) {
        return 30;
    }
    return UITableViewAutomaticDimension;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"";
    } else if (section == 1) {
        return RStr(@"Default language");
    } else {
        return RStr(@"Theme Color");
    }
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
//    header.tintColor = [prefs darkerColorForColor:prefs.currentThemeColors[kRegexNavBarViewTypeBackground]];
    header.tintColor = prefs.currentThemeColors[kRegexNavBarViewTypeBackground];
    header.textLabel.textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
    header.textLabel.alpha = 0.75;    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSArray *record = menu[indexPath.row];
        SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:SetupCellReusableID  forIndexPath:indexPath];
        cell.data = record;
        return cell;
    } else if (indexPath.section == 1) {
        SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:LanguageCellReusableID  forIndexPath:indexPath];
        cell.expandedCell = self.cellLanguageExpanded;
        cell.data = @[ RStr(@"Default Language"),   @"logoM_objc",    @(TableTagLanguage) ];
        return cell;
    }
    
    // Выбор темы
    static NSString *simpleTableIdentifier = @"ThemeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    NSString *themeText = [NSString stringWithFormat:@"THEME_NAME_%d",(int)indexPath.row];
    cell.textLabel.text = RStr(themeText);
    cell.textLabel.textColor = foregrounds[indexPath.row];
    cell.textLabel.font = [prefs fontForTheme:(RegexHighlightViewTheme)indexPath.row key:kRegexHighlightViewDefaultFont scale:1];
    cell.backgroundColor = backgorunds[indexPath.row];
    cell.accessoryType = (prefs.currentTheme == indexPath.row ? UITableViewCellAccessoryCheckmark :
                          UITableViewCellAccessoryNone);
    return cell;

}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2 || (indexPath.section == 1 && self.cellLanguageExpanded)) { // Имитируем груптайбл для ios7 ios-7 ios 7
        
        float cornerSize = 7.5;
        CGRect frame=cell.bounds;
        frame.origin.x=cell.bounds.origin.x+10;
        frame.size.width=cell.bounds.size.width-20;
        UIBezierPath *maskPath;
        
        if (indexPath.row == 0 && [tableView numberOfRowsInSection:indexPath.section] == 1) { // закруглить все углы, если в секции только одна строка
            maskPath = [UIBezierPath bezierPathWithRoundedRect:frame byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(cornerSize, cornerSize)];
        } else if (indexPath.row == 0) { // закруглить только верхние углы, если в секции более одной строки
            maskPath = [UIBezierPath bezierPathWithRoundedRect:frame byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(cornerSize, cornerSize)];
        } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1) { // закруглить нижнии углы последней строки
            maskPath = [UIBezierPath bezierPathWithRoundedRect:frame byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(cornerSize, cornerSize)];
        } else { // обрезать слева и справа средние строки
            maskPath = [UIBezierPath bezierPathWithRect:frame];
        }
        
        CAShapeLayer *mlayer = [[CAShapeLayer alloc] init];
        
        CAShapeLayer *strokelayer = [[CAShapeLayer alloc] init];
        strokelayer.name = @"Ramka";
        UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
        strokelayer.strokeColor = textColor.CGColor;
        strokelayer.fillColor = [UIColor clearColor].CGColor;
        strokelayer.lineWidth = 0.7;
        
        strokelayer.path = maskPath.CGPath;
        mlayer.path =  maskPath.CGPath;
        for (CALayer *layer in cell.layer.sublayers) {
            if ([layer.name isEqualToString:@"Ramka"]) {
                [layer removeFromSuperlayer];
                break;
            }
        }
        [cell.layer addSublayer:strokelayer];
        cell.layer.mask = mlayer;
        //        cell.layer.masksToBounds = YES;
    } else if (indexPath.section == 1 && !self.cellLanguageExpanded) {
        for (CALayer *layer in cell.layer.sublayers) {
            if ([layer.name isEqualToString:@"Ramka"]) {
                [layer removeFromSuperlayer];
                break;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger tag = 0;
    if (indexPath.section == 0) {
        SetupCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        tag = cell.tag;
        if (tag == TableTagCoin) { // Магазин
            [self openCoinShop];
        } else if (tag == TableTagWalk) { // Стартовый экран с заставкой
            [(MainViewController*)self.sideBarController.contentViewController walkShow];
        } else if (tag == TableTagTheme) { // Настройка темы
            [self themeSelectorClicked:nil];
        }
    } else if (indexPath.section == 1) {
        SetupCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        tag = cell.tag;
        if (tag == TableTagLanguage) { // Выбор языка по умолчанию
            self.cellLanguageExpanded = !self.cellLanguageExpanded;
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:YES];
        }
    } else { //  if (indexPath.section == 1) {
        prefs.currentTheme = (RegexHighlightViewTheme)indexPath.row;
        [tableView reloadData];
    }
    
    if (tag != TableTagLanguage) {
        [self.sideBarController hideMenuViewController:YES];
    }
}
#pragma mark Делегат сайдбара

#pragma mark Действия по кнопкам

- (void) openCoinShop
{
    CoinShopTableViewController *v = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinShopTableViewController"];
    [self.sideBarController.navigationController pushViewController:v animated:YES];
}

- (IBAction)themeSelectorClicked:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (!themeViewController) {
            themeViewController = [[ThemeSelectorViewController alloc] init];
        }
        // iPad - use standard popup view
        themeViewController.modalPresentationStyle = UIModalPresentationPopover;
        [self presentViewController:themeViewController animated:YES completion:nil];
        
        // configure the Popover presentation controller
        UIPopoverPresentationController *popController = [themeViewController popoverPresentationController];
        popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        //popController.barButtonItem = rightButton;
        popController.delegate = self;
    } else {
        if (!themeViewController) {
            themeViewController = [[ThemeSelectorViewController alloc] init];
            themeViewController.parent = self;
            
            [self.view addSubview:themeViewController.view];
        }
        if (themeViewController.isViewVisible) {
            [themeViewController hideFromScreen];
        } else {
            [themeViewController showOnScreen];
        }
        
    }
}


@end
