//
//  ThemeSelectorViewController.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 05.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "ThemeSelectorViewController.h"
#import "Preferences.h"
//#import "SDVersion.h"

@interface ThemeSelectorViewController () {
	NSMutableArray *backgorunds;
	NSMutableArray *foregrounds;
	Preferences	*prefs;
}


@end

@implementation ThemeSelectorViewController

- (id) init
{
	if (self = [super initWithNibName:[[self class] description] bundle:nil]) {
        CGSize frame = self.preferredContentSize;
        frame.height = kRegexHighlightViewThemesCount * (1.7f + 44.0f);
        self.preferredContentSize = frame;
	}
	return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	prefs = [Preferences sharedPreferences];
	backgorunds = [NSMutableArray new];
	foregrounds = [NSMutableArray new];
	for (RegexHighlightViewTheme i = kRegexHighlightViewThemeBasic; i < kRegexHighlightViewThemesCount; i++) {
		[backgorunds addObject:[prefs backgroundForTheme:i]];
		[foregrounds addObject:[prefs foregroundForTheme:i]];
	}
	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		// оформляем как выползающую снизу таблицу
		CGRect sb = [UIScreen mainScreen].bounds;
		CGFloat height = kRegexHighlightViewThemesCount * (1.0f + 44.0f) ;
		CGFloat limit = sb.size.height / 2.0;
		if (height > limit) {
			//height = limit;
		}
		self.view.frame = CGRectMake(0.0, sb.size.height, sb.size.width, height);
		[self.view setNeedsLayout];
		
//		// add touch recogniser to dismiss this controller
//		UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissMe)];
//		[self.view addGestureRecognizer:tap];
		self.isViewVisible = NO;
	}


}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(preferredFontChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) preferredFontChanged:(NSNotification *)notification
{
    // При изменениеи масштабирования шрифтов
    [self.tableview reloadData];
}


- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dismissMe
{
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		[self dismissViewControllerAnimated:YES completion:nil];
	} else {
		[self hideFromScreen];
	}
}

//- (BOOL) prefersStatusBarHidden
//{
//	return YES;
//}



#pragma mark - 

- (void) showOnScreen
{
	CGRect b = [UIScreen mainScreen].bounds;
	CGRect destFrame = self.view.frame;
	destFrame.origin.y = b.size.height - destFrame.size.height;
	[UIView beginAnimations : @"Show1" context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationBeginsFromCurrentState:FALSE];
	self.view.frame = destFrame;
	[UIView commitAnimations];
	self.isViewVisible = YES;
}

- (void) hideFromScreen
{
	CGRect b = [UIScreen mainScreen].bounds;
	CGRect destFrame = self.view.frame;
	destFrame.origin.y = b.size.height;
	[UIView beginAnimations : @"hide1" context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationBeginsFromCurrentState:FALSE];
	self.view.frame = destFrame;
	[UIView commitAnimations];
	self.isViewVisible = NO;
}


#pragma mark -

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return backgorunds.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *simpleTableIdentifier = @"ThemeCell";
 
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
 
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
	}
 
	NSString *themeText = [NSString stringWithFormat:@"THEME_NAME_%d",(int)indexPath.row];
	cell.textLabel.text = RStr(themeText);
	cell.textLabel.textColor = foregrounds[indexPath.row];
    cell.textLabel.font = [prefs fontForTheme:(RegexHighlightViewTheme)indexPath.row key:kRegexHighlightViewDefaultFont scale:1];
	cell.backgroundColor = backgorunds[indexPath.row];
	cell.accessoryType = (prefs.currentTheme == indexPath.row ? UITableViewCellAccessoryCheckmark :
						  UITableViewCellAccessoryNone);
	return cell;
	
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	prefs.currentTheme = (RegexHighlightViewTheme)indexPath.row;
	[tableView reloadData];
}



@end
