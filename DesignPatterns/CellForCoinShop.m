//
//  CellForCoinShop.m
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 19.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "CellForCoinShop.h"
#import "Preferences.h"


//@interface CellForCoinShop()
//
//@end

@implementation CellForCoinShop

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTheme:) name:VVVcurrentThemeChanged object:nil];
    
    [self updateTheme:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) updateTheme:(NSNotification *) note
{
    NSDictionary *colors = [[Preferences sharedPreferences] currentThemeColors];
    self.backgroundColor = colors[kRegexHighlightViewTypeBackground];
    self.opisanie.textColor = colors[kRegexHighlightViewTypeText];
    self.nameItem.textColor = colors[kRegexHighlightViewTypeText];
    self.price.textColor = colors[kRegexHighlightViewTypeText];

    
    self.buttonBay.titleLabel.textColor = colors[kRegexHighlightViewTypeButton];
    CALayer *l = self.buttonBay.layer;
    l.backgroundColor = [(UIColor *)colors[kRegexNavBarViewTypeBackground] CGColor];
    l.borderColor = [(UIColor *)colors[kRegexHighlightViewTypeText] CGColor];
    l.borderWidth = 1;
    l.cornerRadius = 8.0;
}

@end
