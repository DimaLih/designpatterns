//
//  Utils.h
//  MobilnyBalansSmart
//
//  Created by Dmitry Likhtarov on 10.10.13.
//  Copyright (c) 2013 Dmitry Likhtarov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(BOOL)hasInternet;
+(BOOL)hasInternetBezAlerta;

@end
