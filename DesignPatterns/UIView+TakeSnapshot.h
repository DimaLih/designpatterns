//
//  UIView+TakeSnapshot.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 24/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TakeSnapshot)

- (UIImage *) takeSnapshot:(CGRect)frame;

@end
