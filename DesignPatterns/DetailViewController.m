//
//  DetailViewController.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 28.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "DetailViewController.h"
#import "SourceCodeViewController.h"
#import "Preferences.h"
#import "FCAlertView.h"
#import "ReportViewController.h"
#import "CoinShopTableViewController.h"
#import "IGLDropDownMenu.h"

@interface DetailViewController () <IGLDropDownMenuDelegate, UIScrollViewDelegate, UIDocumentInteractionControllerDelegate> {
	Preferences *prefs;
    CGFloat heightScrollMinimum;
    CGFloat heightScrollMaximum;
}

@property (weak, nonatomic) IBOutlet UITextView *itemText;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *exampleButton;

@property (nonatomic, retain) IGLDropDownMenu *exportSelector;
@property (nonatomic, retain) IGLDropDownMenu *simplesBurron;
@property (nonatomic, weak) IBOutlet UIView *viewMenuLeft;
@property (nonatomic, weak) IBOutlet UIView *viewMenuRight;

@property (nonatomic, weak) IBOutlet UIScrollView *svImage;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicture;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraintImageHigh;

@end

@implementation DetailViewController

//- (BOOL) pictureAvailable
//{
//    if ([self pictureArray].count < self.group) return NO;
//    NSArray *list = [[self pictureArray] objectAtIndex:self.group];
//    if (list.count == 0 && list.count < self.item) {
//        return NO;
//    }
//    return YES;
//}


#pragma mark - Зум картинки и его фрейма
- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.itemPicture;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    // тут логику возможно еще продумать...
    if (self.svImage.contentSize.height < heightScrollMinimum) {
        self.constraintImageHigh.constant = heightScrollMinimum;
    } else if (self.svImage.contentSize.height > heightScrollMaximum) {
        self.constraintImageHigh.constant = heightScrollMaximum;
    } else {
        self.constraintImageHigh.constant = self.svImage.contentSize.height;
    }
    
    // анимируем все это по своему шобнедёргался
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25f animations: ^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) { }];
}
- (void)doubleTap:(UITapGestureRecognizer *)tap
{
    CGFloat zoomScale = 1;
    if (self.svImage.contentSize.height < heightScrollMaximum) {
        self.constraintImageHigh.constant = heightScrollMaximum;
        zoomScale = heightScrollMaximum / heightScrollMinimum;
        if (zoomScale>2) {
            zoomScale = 2;
        }
    } else {
        self.constraintImageHigh.constant = heightScrollMinimum;
    }
    
    // анимируем все это по своему шобнедёргался
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25f animations: ^{
        [self.view layoutIfNeeded];
        self.svImage.zoomScale = zoomScale;
     } completion:^(BOOL finished) { }];
}
#pragma mark -
- (void) configureView
{
    // Update the user interface for the detail item.
    NSString *title = [NSString stringWithFormat:@"PATTERN_%ld_%ld",(long)self.group, (long)self.item];
    self.title = RStr(title);
    
    NSString *descr = [NSString stringWithFormat:@"PATTERN_%ld_%ld_DESC",(long)self.group, (long)self.item];
    self.itemText.text = RStr(descr);
    [self.view layoutIfNeeded]; // !!! Перед манипуляций обновим элементы, чтобы пересчитались размеры в соответствии с резинками

    UIImage *image = [self imageBlackOrWhiteOrBlackOnly:NO];
    self.svImage.zoomScale = 1; // !
    self.itemPicture.image = image;
    if (image) { // Если изображение слишком низкое или отсутствует, то подвинем текстовый блок кверху
        CGFloat ratio = image.size.width / image.size.height;
        if (ratio >1) {
            self.constraintImageHigh.constant = self.itemPicture.frame.size.width / ratio;
            [self.view setNeedsUpdateConstraints];
            //[UIView animateWithDuration:0.5f animations:^{
            //    [self.view layoutIfNeeded];
            //} completion:nil];
        }
    } else { // Когда нет картинки, текст растянем на весь экран до верха
        self.constraintImageHigh.constant = 0;
        //self.constraintImageVerh.constant = 0;
    }
    [self.view layoutIfNeeded]; // !!! После всех манипуляций обновим элементы, размеры и резинки

    // Установим минимальные и максимальные размеры просмотра картинки
    // для айпада достаточно и пол-экрана или даже меньше
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        heightScrollMaximum = [[UIScreen mainScreen] bounds].size.height * 0.4;
    } else {
        heightScrollMaximum = [[UIScreen mainScreen] bounds].size.height * 0.6;
    }
    heightScrollMinimum = self.constraintImageHigh.constant;
    if (heightScrollMaximum < heightScrollMinimum) {
        heightScrollMaximum = heightScrollMinimum; // и такое возможно. наверное:)
    }
    
}
- (UIImage *)imageBlackOrWhiteOrBlackOnly:(BOOL)isBlackOnly
{
    // Вернем картинку в цвете в зависимости от темы, или черную, если isBlackOnly==YES
    NSString *name = @"Black";
    if (prefs.currentTheme == kRegexHighlightViewThemeDusk || prefs.currentTheme == kRegexHighlightViewThemeMidnight || prefs.currentTheme == kRegexHighlightViewThemeNostalgy) {
        if (isBlackOnly == NO) {
            name = @"White";
        }
    }
    UIImage *image = nil;
    NSString *pattern = [prefs patternNameForGroup:self.group andItem:self.item];
    if (pattern) {
        name = [NSString stringWithFormat:@"%@_%@",pattern,name];
        // Все картинки с этими именами хранятся в Diagramms.xcassets
        image = [UIImage imageNamed:name];
    }
    
//    if (!image && [self pictureArray].count > self.group) {
//#warning !!! TEMP!!! пока не структурируем список файлов
//        NSArray *list = [[self pictureArray] objectAtIndex:self.group];
//        if (list.count > self.item) {
//            image = [UIImage imageNamed:list[self.item]];
//        }
//    }
    return image;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
    
    //self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModePrimaryHidden; // хреново и всегда

    // Для отладки в IB присвоены цвета на вьюхи
    self.view.backgroundColor = [UIColor whiteColor];
    self.itemText.backgroundColor = [UIColor clearColor];
    self.svImage.backgroundColor = [UIColor clearColor];
    self.itemPicture.backgroundColor = [UIColor clearColor];
    self.viewMenuLeft.backgroundColor = [UIColor clearColor];
    self.viewMenuRight.backgroundColor = [UIColor clearColor];
    
    // Добавим отступ снизу, чтобы скрол красивее до низу был
    self.itemText.contentInset = UIEdgeInsetsMake(0, 0, 65, 0);


    prefs = [Preferences sharedPreferences];
	//[self configureView];
    
    [self buttonButtomSetup];

    // Следим за сменой темы оформления
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
    
    // Действие по двойному тапу на картинку
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    tap.numberOfTapsRequired = 2;
    [self.svImage addGestureRecognizer:tap];

	
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self configureFont];
    [self configureView];
    [self themeChanged:nil];
    [self coinChanged:nil];

    dispatch_async(dispatch_get_main_queue(), ^{
        // Скролим текст вверх до навигатионбара.
        // запускать в этой обертке, иначе не будет правильно скролиться
        [self.itemText scrollRangeToVisible:NSMakeRange(0, 0)]; // Пролистать к началу
        [self.itemText setContentOffset:CGPointZero animated:NO];
    });


    // Subscribing to UIContentSizeCategoryDidChangeNotification
	// to get notified when user chages the preferred size.
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(preferredFontChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
    [nc addObserver:self selector:@selector(coinChanged:) name:VVVexportCoinUsed object:nil];
}
- (void) viewDidDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	// Unsubscribe from All notifications
	//[[NSNotificationCenter defaultCenter] removeObserver:self]; // Смену темы всегда слушаем!
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
    [nc removeObserver:self name:VVVexportCoinUsed object:nil];

}

- (void) coinChanged:(NSNotification *) note
{
    
    // [Preferences sharedPreferences].exportCoins = 1; // сбросим тестово
    
    // После обновления монет обновить кнопку
    NSInteger coins = [Preferences sharedPreferences].exportCoins;
    for (IGLDropDownItem *item in self.exportSelector.dropDownItems) {
        if (item.indexLDD == Index_Coin) {
            if (coins > 1000000) {
                // Лучше бы сразу удалить эту кнопку, но это муторно
                // поэтому покажем бесконечность, а при следующем обновлении вьюхи
                // эта кнопка больше не будет мозолить глаза
                item.text = [NSString stringWithFormat:@" %@: ∞", RStr(@"Coins left")];
            } else {
                item.text = [NSString stringWithFormat:@" %@: %ld", RStr(@"Coins left"), (unsigned long)coins];
            }
        }
    }
    
    // [self.exportSelector reloadView];
}

#pragma mark - Смена темы оформления
- (void) themeChanged:(NSNotification *) note
{
    [self configureColorForMenu:self.exportSelector];
    [self configureColorForMenu:self.simplesBurron];

	UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
	UIColor *backColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
	UIColor *navBackColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
	
	self.view.backgroundColor = backColor;
	self.itemText.textColor = textColor;
	
	self.navigationController.navigationBar.barTintColor = navBackColor;
	self.navigationController.navigationBar.tintColor = textColor;
	[self.navigationController.navigationBar setTitleTextAttributes:
	 @{NSForegroundColorAttributeName:textColor}];

	
}

- (void) configureColorForMenu:(IGLDropDownMenu *)menu
{
    UIColor *colorButton = [Preferences sharedPreferences].currentThemeColors[kRegexHighlightViewTypeButton];
    if (!colorButton) { colorButton = [UIColor colorWithWhite:0.97 alpha:1]; }
    menu.colorFon = colorButton; // Цвет фона кнопок
    
    UIColor *colorText = [Preferences sharedPreferences].currentThemeColors[kRegexHighlightViewTypeText];
    if (!colorText) { colorText = [UIColor colorWithWhite:0.2 alpha:1]; }
    menu.textColor = colorText; // Цвет текста кнопок
    
    menu.borderColor = colorText; // Цвет рамки
 }

#pragma mark - Определить кнопки
- (void) buttonButtomSetup
{
   //
    //-------------------------
    // Set up Export selector
    //
    self.exportSelector = [[IGLDropDownMenu alloc] init];
    self.exportSelector.frame = CGRectMake(0, self.viewMenuLeft.frame.size.height - 40, 140, 40);
    self.exportSelector.paddingLeft = 5.0f;
    self.exportSelector.delegate = self;
    self.exportSelector.menuText = [NSString stringWithFormat:@" %@",RStr(@"Export")];
    self.exportSelector.type = IGLDropDownMenuTypeStack;
    self.exportSelector.gutterY = 5;
    self.exportSelector.itemAnimationDelay = 0.1;
    self.exportSelector.direction = IGLDropDownMenuDirectionUp;
    self.exportSelector.menuButtonStatic = YES;


//#warning TEMP
//    self.exportSelector.colorFon = [UIColor greenColor];
    
    UIImage *imgLeft = [UIImage imageNamed:@"bt_Share-128"];
    self.exportSelector.menuIconImage = imgLeft;
    self.exportSelector.scaleImage = 0.85f; // уменьшим картинку

    //NSArray *arrayMenuExport = @[RStr(@"Print"), RStr(@" Export PDF")];
    NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
    
    IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
    // Если куплено всё то уберем кнопку чтобы не мозолила
    if (prefs.exportCoins < 100000) {
        item.scaleImage = 0.7f;
        NSInteger coinLeft = [Preferences sharedPreferences].exportCoins;
        item.text = [NSString stringWithFormat:@" %@: %ld", RStr(@"Coins left"), (unsigned long)coinLeft];
        item.iconImage = [UIImage imageNamed:@"bt_Coin"];
        item.indexLDD = Index_Coin;
        [dropdownItems addObject:item];
    }

	//
	// Экспортные функции доступны только в том случае, когда у юзера есть export-coins
	//

    // Для кнопок добавил уникальный индекс indexLDD,
    //
  	item = [[IGLDropDownItem alloc] init];
	item.scaleImage = 0.7f;
	item.text = [NSString stringWithFormat:@" %@",RStr(@"Print")];
	item.iconImage = [UIImage imageNamed:@"bt_Print-128"];
    item.indexLDD = Index_Print_Color;
	[dropdownItems addObject:item];
	
	
	item = [[IGLDropDownItem alloc] init];
	item.scaleImage = 0.7f;
	item.text = [NSString stringWithFormat:@" %@",RStr(@"Export PDF")];
	item.iconImage = [UIImage imageNamed:@"bt_PDF-128"];
    item.indexLDD = Index_Export_PDF;
	[dropdownItems addObject:item];
    
    self.exportSelector.dropDownItems = dropdownItems;
    
    [self.viewMenuLeft addSubview:self.exportSelector];
    [self.exportSelector reloadView];
    //-------------------------
    
    
    //-------------------------
    // Переход к примерам
    //
    self.simplesBurron = [[IGLDropDownMenu alloc] init];
    
    self.simplesBurron.frame = CGRectMake(0, self.viewMenuRight.frame.size.height - 40, 140, 40);
    
    self.simplesBurron.delegate = self;
    self.simplesBurron.menuText = [NSString stringWithFormat:@" %@", RStr(@"Example")];;
    self.simplesBurron.paddingLeft = 5.0f;
    //self.simplesBurron.type = IGLDropDownMenuTypeStack;
    self.simplesBurron.gutterY = 5;
    self.simplesBurron.itemAnimationDelay = 0.0;
    self.simplesBurron.showBackgroundShadow = YES;
    self.simplesBurron.menuButtonStatic = YES;
    self.simplesBurron.direction = IGLDropDownMenuDirectionUp;
    //dropdownItems = [[NSMutableArray alloc] init];
    //IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
    //item.text = [NSString stringWithFormat:@" %@", RStr(@"Example")];
    UIImage *img = [UIImage imageNamed:@"bt_Code-128"];
    self.simplesBurron.menuIconImage = img;
    self.simplesBurron.scaleImage = 0.85f; // уменьшим картинку
 
    //[dropdownItems addObject:item];

    //self.simplesBurron.dropDownItems = dropdownItems;
    
    [self.viewMenuRight addSubview:self.simplesBurron];
    [self.simplesBurron reloadView];
    //-------------------------
   
}
#pragma mark - Методы делегата Меню IGLDropDownMenuDelegate

- (void) dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndexLDD:(NSInteger)indexLDD
{
    if (dropDownMenu == self.exportSelector) { // Print
		//switch (index) {
        switch (indexLDD) {
			case Index_Coin:	    [self openCoinShop]; break;				// IN-App покупки
			case Index_Print_Color:	[self printButtonClicked:nil]; break;	// печать
			case Index_Export_PDF:	[self shareButtonClicked:nil]; break;   // Экспорт PDF
		}
	}
}

//- (void) dropDownMenu:(IGLDropDownMenu *)dropDownMenu expandingChangedWithAnimationCompledted:(BOOL)isExpanding {}
- (void) dropDownMenu:(IGLDropDownMenu *)dropDownMenu expandingChanged:(BOOL)isExpanding {
    if (dropDownMenu == self.simplesBurron) {
        [self exampleButtonClicked:nil];
    }
}
- (IBAction) exampleButtonClicked:(id)sender
{
    NSString *pattern = [prefs patternNameForGroup:self.group andItem:self.item];
    if (pattern) {
        SourceCodeViewController *v = [self.storyboard instantiateViewControllerWithIdentifier:@"SourceCodeViewController"];
        // Собираем словарь с примерами
        NSDictionary *dd = [prefs fileNamesForPattern:pattern];
        if (dd.count > 0) {
            v.examples = dd;
            [self.navigationController pushViewController:v animated:YES];
            return;
        }
    }
    // Примеры отсутствуют - выдаем сообщение
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    [alert makeAlertTypeCaution];
    [alert showAlertInView:self
                 withTitle:RStr(@"No examples found")
              withSubtitle:RStr(@"SRC_EXAMPLE_NOT_PREPARED")
           withCustomImage:nil
       withDoneButtonTitle:RStr(@"OK")
                andButtons:nil];
}

#pragma mark - Масштабирование шрифтов

- (void) preferredFontChanged:(NSNotification *)notification
{
    [self configureFont];
}

- (void) configureFont
{
    // Узнаем размер и присвоим его текущему шрифту
    UIFont *font = [prefs fontForTheme:kRegexHighlightViewThemeDefault key:kRegexHighlightViewDefaultFont scale:1.0];
    self.itemText.font = [font fontWithSize:font.pointSize];
}

#pragma mark - Managing the detail item

- (ReportViewController *) reportController
{
    ReportViewController *report = [[ReportViewController alloc] init];
    //Добавить картинку и текст
    report.textOpisanie  = self.itemText.text;
    report.textZagolovok = self.title;
    report.imageDiagram  = [self imageBlackOrWhiteOrBlackOnly:YES]; // self.itemPicture.image;
    return report;
}

- (void) openCoinShop
{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    // да и на айфоне может с кнопкой Закрыть? или навигацию оставить?
        CoinShopTableViewController *v = (CoinShopTableViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"CoinShopTableViewController"];
        v.modalPresentationStyle = UIModalPresentationFormSheet;

        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:v];
        nav.modalPresentationStyle = UIModalPresentationFormSheet;
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:RStr(@"Close") style:UIBarButtonItemStyleDone target:v action:@selector(vihod:)];
        v.navigationItem.leftBarButtonItem = cancelButton;
        [self.navigationController presentViewController:nav animated:YES completion:nil];
//    } else {
//        CoinShopTableViewController *v = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinShopTableViewController"];
//        [self.navigationController pushViewController:v animated:YES];
//    }
    
}

- (IBAction) shareButtonClicked:(id)sender
{
	BOOL printAvailable = (prefs.exportCoins > 0);
	if (printAvailable) {
		// На симуляторе это не работает, только на девайсе
		UIDocumentInteractionController * docController = [[UIDocumentInteractionController alloc] init];
		docController.URL = [NSURL fileURLWithPath:[[self reportController] pathFromFilePDF]];
		docController.delegate = self;
		[docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
	} else {
		[self informAboutExportCoins];
	}
}

//
//Списываем монетку, если юзер на самом деле отправил данные во внешнее приложние
//
- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(nullable NSString *)application {
	[prefs removeOneCoin];
}


- (IBAction) printButtonClicked:(id)sender
{
	BOOL printAvailable = (prefs.exportCoins > 0);
	if (printAvailable) {
		[[self reportController] printPDF];
	} else {
		[self informAboutExportCoins];
	}
}


- (void) informAboutExportCoins
{
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:RStr(@"No coins left")
																   message:RStr(@"Explain coins")
															preferredStyle:UIAlertControllerStyleAlert];
	UIAlertAction *alertAction = [UIAlertAction actionWithTitle:RStr(@"OK") style:UIAlertActionStyleCancel handler:^(UIAlertAction *actionHandler) {
			
		}];
	[alert addAction:alertAction];
	UIAlertAction *coinShopAction = [UIAlertAction actionWithTitle:RStr(@"Coin Shop") style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {
		[self openCoinShop];
	}];
	[alert addAction:coinShopAction];
	[self presentViewController:alert animated:YES completion:nil];
}

#pragma mark -

//- (NSArray *) pictureArray  // Потом можно удалить это
//{
//    //
//    // Пока диаграммы здесь будут добавляться.
//    // Когда соберем все картинки и причешем их в едином
//    // стиле, может быть следует заменить их на webArchive,
//    // чтобы показывать в UIWebview и обеспечить
//    // автоматическое масштабирование жестами
//    // - но я в этом не уверен ВВ
//    //
//    //
//    static NSArray *ar = nil;
//	if (!ar) {
//		ar = @[
//			   @[
//#warning !!! TEMP EXPERIMENT
//				   // Картинки в SVG можно взять в https://sourcemaking.com/design_patterns/
//                   @"Abstract-factory_2048.png",//@"Svg-Abstract-factory.svg",//@"uml-abstract-factory.jpg",
//				   @"Svg-Builder.svg", //@"uml-builder.jpg",
//                   @"Svg-Factory-method.svg",//@"uml-factory-method.jpg",
//                   @"lazy-init.jpg",
//                   @"Object-Pool.png",
//                   @"uml-prototype.jpg",
//                   @"uml-singleton.jpg",
//                   @"230px-Multiton.png",
//				   ],
//               @[
//                   @"uml-adapter.jpg",
//                   @"uml-bridge.jpg",
//                   @"uml-composite.jpg",
//				   @"uml-decorator-2.gif",
//				   @"uml-facade-1.gif",
//				   @"front-controller.gif",
//				   @"uml-flyweight-1.gif",
//				   // но почему то не показывается :-( // уже показывается ЛДД:)
//				   @"Proxy1.svg",	// @"uml-proxy.gif",
//                   ],
//			   @[
//				   @"uml-chain-of-responsibility.gif",
//                   //@"uml-command.jpg", // Файла нету
//                   @"LD-Command.png", // !!
//                   @"uml-interpreter.gif",
//				   @"uml-iterator.gif",
//				   @"uml-mediator.gif",
//				   @"uml-memento.gif",
//				   //@"Null_Object2.svg",
//                   @"LD-Null_Object2.png", // !!
//				   @"uml-observer.gif",
//				   @"uml-state.gif",
//				   @"uml-strategy.jpg",
//				   @"uml-template-method.gif",
//				   @"uml-visitor.gif",
//			   ],
//			   ];
//    }
//	return ar;
//}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end


//// Остатки вывода на UIWebView :))
//#pragma mark - color to tipa FFFAA99
//// Возвращает nil или 6 16-тиричных символов
//- (NSString *) hexFromColor:(UIColor *)color
//{
//    if (!color || color == [UIColor whiteColor]) { return @"ffffff"; }
//    CGFloat r, g, b, a;
//    [color getRed:&r green:&g blue:&b alpha:&a];
//    return [NSString stringWithFormat:@"%02x%02x%02x", (unsigned int)(r * 255), (unsigned int)(g * 255), (unsigned int)(b * 255)];;
//}
//
//- (void) configureView
//{
//    // Update the user interface for the detail item.
//    NSString *title = [NSString stringWithFormat:@"PATTERN_%ld_%ld",(long)self.group, (long)self.item];
//    self.title = RStr(title);
//
//    NSString *descr = [NSString stringWithFormat:@"PATTERN_%ld_%ld_DESC",(long)self.group, (long)self.item];
//    self.itemText.text = RStr(descr);
//    if ([self pictureArray].count > self.group) {
//        NSArray *list = [[self pictureArray] objectAtIndex:self.group];
//        if (list.count > self.item) {
//            //-----------------------
//            [self.view layoutIfNeeded]; // !!! Перед манипуляций обновим элементы, чтобы пересчитались размеры в соответствии с резинками
//
//            NSDictionary *dict = [self frameAnStringSVGFromfile:list[self.item]]; // Пытаемся получить фрейм svg файла
//
//            if (!dict[@"framevalue"]) {
//                // Если изображение слишком низкое или отсутствует, то подвинем текстовый блок кверху
//                self.constraintImageHigh.constant = 1;
//                //self.constraintImageVerh.constant = 1;
//            } else {
//                //  Картинка есть. Настроим фрейм под пропорции картинки, но не больше чем пол-экрана
//                CGRect frame = [dict[@"framevalue"] CGRectValue];
//                CGFloat ratio = frame.size.width / frame.size.height;
//                CGFloat visota = self.www.frame.size.width / ratio;
//                if (visota > self.view.frame.size.height / 2) {
//                    visota = self.view.frame.size.height / 2;
//                }
//                self.constraintImageHigh.constant = visota;
//
//#warning Need edit this!!! Я так толком и не смог разобраться нормально отцентрировать
//                // Если это можно сделать CSS, то как? мне проще вычислить размер заранее и вписать в теги ширина-высота:)
//                NSString * widthImg   = (ratio >1) ? @"93\%" : @"auto"; // картинка горизонтальная : вертикальная
//                NSString * heightImg  = (ratio >1) ? @"auto"  : @"93\%";
//                NSString * htmlString;
//                if (dict[@"imagename"]) {
//                    htmlString = [NSString stringWithFormat:@"<html><head> <meta name=\"viewport\" content=\"width=device-width, maximum-scale=5.0\" /> </head> <body style=\"margin:0;\"> <img style=\"display:block; margin:0 auto; height:%@;width:%@;\" src=\"%@\" /></body></html>", widthImg, heightImg, dict[@"imagename"]];
//                } if (dict[@"svgstring"]) {
//                    htmlString = [NSString stringWithFormat:@"<html><head> <meta name=\"viewport\" content=\"width=device-width, maximum-scale=5.0\" /> </head> <body style=\"margin:0;\"> <div style=\"display:block; margin:0 auto; height:%@;width:%@;\">%@</div></body></html>", widthImg, heightImg, dict[@"svgstring"]];
//                }
//                [self.www loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]]];
//            }
//            [self.view layoutIfNeeded]; // !!! После всех манипуляций обновим элементы, размеры и резинки
//        }
//    }
//
//}
//
//- (NSDictionary *) frameAnStringSVGFromfile:(NSString*)nameSVGFile
//{
//    // ! Цвет при подкотовке SVG всех элементов делать не черным! так как текст черный идет без тега цвета
//    //
//    // @"framevalue": размер фрейма svg файла из viewBox в <svg viewBox="34 0 57 296.91">  Если нет всех четырех размеров, то вернем CGRectZero
//    // @"svgstring": измененный SVG c линиями и заливками в цвете текста из настроек
//    // @"imagename": копия имени входного файла nameSVGFile если это не svg
//
//    NSDictionary *dict = nil;
//    CGRect frame = CGRectZero;
//    NSString *stringSVG =[NSString stringWithContentsOfURL:[[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]] URLByAppendingPathComponent:nameSVGFile] encoding:NSUTF8StringEncoding error:nil];
//    if (stringSVG) {
//        NSTextCheckingResult * matchs = [[NSRegularExpression regularExpressionWithPattern:@"<\\s*svg[^>]*viewBox\\s*=\\s*\"\\s*([-+]?\\d*\\.?\\d*)\\s*([-+]?\\d*\\.?\\d*)\\s*([-+]?\\d*\\.?\\d*)\\s*([-+]?\\d*\\.?\\d*)" options:NSRegularExpressionAnchorsMatchLines error:nil] firstMatchInString:stringSVG options:0 range:NSMakeRange(0, stringSVG.length)]; //|<\\s*s(v)(g)[^>]*width\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)[^>]*height\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)|<\\s*s(v)(g)[^>]*height\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)[^>]*width\\s*=\\s*\"?\\s*([-+]?\\d*\\.?\\d*)
//        if ([matchs numberOfRanges]==5) { // только если есть все 4 цифры viewBox="34 0 57 296.91"
//            frame = CGRectMake(
//                               [[stringSVG substringWithRange:[matchs rangeAtIndex:1]] floatValue],
//                               [[stringSVG substringWithRange:[matchs rangeAtIndex:2]] floatValue],
//                               [[stringSVG substringWithRange:[matchs rangeAtIndex:3]] floatValue],
//                               [[stringSVG substringWithRange:[matchs rangeAtIndex:4]] floatValue]
//                               );
//            // Заменим все линии и заливки в SVG на цвет текста
//            NSString *hexColor = [self hexFromColor:[Preferences sharedPreferences].currentThemeColors[kRegexHighlightViewTypeText]];
//            NSArray <NSTextCheckingResult *> * array = [[NSRegularExpression regularExpressionWithPattern:@"(?:stroke|fill)\\s*:\\s*#([0-9a-fA-F]+)" options:0 error:nil] matchesInString:stringSVG options:0 range:NSMakeRange(0, stringSVG.length)];
//            for (long i = array.count - 1; i >= 0 ; i--) {
//                NSTextCheckingResult* match = array[i];
//                if (match.numberOfRanges==2 && [match rangeAtIndex:1].length > 0) {
//                    stringSVG = [stringSVG stringByReplacingCharactersInRange:[match rangeAtIndex:1] withString:hexColor];
//                }
//            }
//            dict = @{@"svgstring":stringSVG,@"framevalue":[NSValue valueWithCGRect:frame]};
//        }
//    } else { // Если вдруг это не svg
//        UIImage *image = [UIImage imageNamed:nameSVGFile];
//        if (image) {
//            frame = CGRectMake(0, 0, image.size.width, image.size.height);
//            dict = @{@"imagename":nameSVGFile, @"framevalue":[NSValue valueWithCGRect:frame]};
//        }
//    }
//    return dict;
//}
