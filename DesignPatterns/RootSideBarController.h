//
//  RootSideBarController.h
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 24.09.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "LMSideBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RootSideBarController : LMSideBarController <LMSideBarControllerDelegate>

@end

NS_ASSUME_NONNULL_END
