//
//  GMFoldingCell.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMRotatedView.h"


typedef NS_ENUM(NSInteger, GMFoldingCellAnimationState) {
	GMFoldingCellAnimationStateOpen = 0,
	GMFoldingCellAnimationStateClose,
};

typedef NS_ENUM(NSInteger, AnimationType) {
    AnimationTypeOpen = 0,
    AnimationTypeClose,
};

@interface GMFoldingCell : UITableViewCell

@property (nonatomic, retain) NSMutableArray <NSDate *> *durationsForExpandedState;
@property (nonatomic, retain) NSMutableArray <NSDate *> *durationsForCollapsedState;


-(void) unfold:(BOOL) unfold animated:(BOOL) animated completion:(void (^ __nullable)(void))completion;

- (void) openAnimation:(void (^ __nullable)(void))completion;
- (void) closeAnimation:(void (^ __nullable)(void))completion;

@property (nonatomic, readwrite) BOOL isUnfolded;
- (BOOL) isAnimating;

@property (nonatomic,weak) IBOutlet UIView *containerView;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *containerViewTop;

/// UIView whitch display when cell close

@property (nonatomic, retain) IBOutlet GMRotatedView *foregroundView;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *foregroundViewTop;

///  the number of folding elements. Default 2
@property (nonatomic, readwrite) IBInspectable NSInteger itemCount;

/// The color of the back cell
@property (nonatomic) IBInspectable UIColor *backViewColor;

// @IBInspectable open var backViewColor: UIColor = UIColor.brown
@property (nonatomic) NSMutableArray <GMRotatedView *> *animationItemViews;

@end
