//
//  main.m
//  ObjectPoolDemo
//
//  Created by Владимир Водолазкий on 23.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FilerToRent : NSObject

@property (nonatomic, readwrite) BOOL busy;
@property (nonatomic, readwrite) NSInteger filerNumber;

- (void) useFiler;

@end

@implementation FilerToRent

- (instancetype) init
{
    if (self = [super init]) {
        self.busy = NO;
    }
    return self;
}

- (void) useFiler
{
    if (self.busy) {
        NSLog(@"Using filer #%ld",(long)self.filerNumber);
    }
}

@end


#pragma mark -

@interface ObjectPool : NSObject {
    NSMutableArray  <FilerToRent *> *pool;
}

+ (ObjectPool *) sharedInstance;

- (FilerToRent *) acquireItemFromPool;
- (void) returnItemToPool:(FilerToRent *)item;

@end

@implementation ObjectPool

+ (ObjectPool *) sharedInstance
{
    static ObjectPool *oPool = nil;
    if (!oPool) {
        oPool = [[ObjectPool alloc] init];
    }
    return oPool;
}

- (instancetype) init
{
    if (self = [super init]) {
        pool = [NSMutableArray new];
    }
    return self;
}

- (FilerToRent *) acquireItemFromPool
{
    // Search for a free filer first
    for (FilerToRent *fr in pool) {
        if (fr.busy == NO) {
            fr.busy = YES;
            return fr;
        }
    }
    // No free filer found...
    // Create the new one and add to the pool
    FilerToRent *newFiler = [[FilerToRent alloc] init];
    if (newFiler) {
        newFiler.filerNumber = pool.count+1;
        newFiler.busy = YES;
        [pool addObject:newFiler];
    }
    return newFiler;
}

- (void) returnItemToPool:(FilerToRent *)item
{
    // keep things simple...
    item.busy = NO;
}


@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        ObjectPool *pool = [ObjectPool sharedInstance];
        
        FilerToRent *filerA = [pool acquireItemFromPool];
        [filerA useFiler];
        
        FilerToRent *filerB = [pool acquireItemFromPool];
        [filerB useFiler];
        
        [pool returnItemToPool:filerA];
        
        FilerToRent *filerC = [pool acquireItemFromPool];
        [filerC useFiler];
        
        FilerToRent *filerD = [pool acquireItemFromPool];
        [filerD useFiler];
        
        [pool returnItemToPool:filerB];
        
        FilerToRent *filerE = [pool acquireItemFromPool];
        [filerE useFiler];
        
    }
    return 0;
}
