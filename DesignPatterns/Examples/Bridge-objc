//
//  main.m
//  BridgeDemo
//
//  Created by Водолазкий В.В. on 27.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Implementation layer -

@interface TimeImplementation : NSObject {
    int hour, minute;
}

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute;

@end

@implementation TimeImplementation

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute
{
    if (self = [super init]) {
        hour = aHour;
        minute = aMinute;
    }
    return self;
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"Time - %02d:%02d", hour, minute];
}
@end

@interface CivilianTimeImplementation : TimeImplementation {
    NSString *amString;
}

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andHemisphere:(BOOL)isAm;
@end

@implementation CivilianTimeImplementation

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andHemisphere:(BOOL)isAm {
    if (self = [super initWithHour:aHour andMinute:aMinute]) {
        amString = (isAm ? @"AM" : @"PM");
    }
    return self;
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"CivilianTime - %02d:%02d %@", hour, minute, amString];
}

@end

@interface ZuluTimeImplementation : TimeImplementation {
    NSString *tZone;
}

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andZone:(int)aZone;

@end

@implementation ZuluTimeImplementation

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andZone:(int)aZone {
    if (self = [super initWithHour:aHour andMinute:aMinute]) {
        switch(aZone) {
            case 5: tZone = @"Eastern Standard Time"; break;
            case 6: tZone = @"Central Standard Time"; break;
            default:
                tZone = @"";
        }
    }
    return self;
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"ZuluTime - %02d:%02d %@", hour, minute, tZone];
}

@end

#pragma mark - Abstract Layer -

@interface Time : NSObject

@property (nonatomic, retain) TimeImplementation *time;

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute;

@end

@implementation Time

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute
{
    if (self = [super init]) {
        self.time = [[TimeImplementation alloc] initWithHour:aHour andMinute:aMinute];
    }
    return self;
}

- (NSString *)description
{
    return [self.time description];
}

@end

@interface CivilianTime : Time
- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andHemisphere:(BOOL)isAm;
@end

@implementation CivilianTime

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andHemisphere:(BOOL)isAm {
    if (self = [super init]) {
        self.time = [[CivilianTimeImplementation alloc] initWithHour:aHour andMinute:aMinute andHemisphere:isAm];
    }
    return self;
}

@end

@interface ZuluTime : Time
- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andZone:(int)aZone;
@end

@implementation ZuluTime

- (instancetype) initWithHour:(int)aHour andMinute:(int)aMinute andZone:(int)aZone {
    if (self = [super init]) {
        self.time = [[ZuluTimeImplementation alloc] initWithHour:aHour andMinute:aMinute andZone:aZone];
    }
    return self;
}

@end


#pragma mark -

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Time *t1 = [[Time alloc] initWithHour:11 andMinute:44];
        NSLog(@"%@",t1);
        CivilianTime *t2 = [[CivilianTime alloc] initWithHour:11 andMinute:44 andHemisphere:YES];
        NSLog(@"%@", t2);
        ZuluTime *t3 = [[ZuluTime alloc] initWithHour:11 andMinute:44 andZone:6];
        NSLog(@"%@", t3);
        
    }
    return 0;
}
