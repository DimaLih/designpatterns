//
//  SourceCodeViewController.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 30.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <CoreText/CoreText.h>
#import "SourceCodeViewController.h"
#import "ThemeSelectorViewController.h"
#import "ReportViewController.h"
#import "IGLDropDownMenu.h"
#import "Preferences.h"
#import "CoinShopTableViewController.h"
#import "AppDelegate.h"

@import GoogleMobileAds;

#define EMPTY @""

@interface SourceCodeViewController () <UIPopoverPresentationControllerDelegate, IGLDropDownMenuDelegate, UIDocumentInteractionControllerDelegate, GADInterstitialDelegate>
{
	UIBarButtonItem *rightButton;
	ThemeSelectorViewController *themeViewController;
	Preferences *prefs;
	UIColor *backColor;
	UIColor *lineNumColor;
    UIColor *textColor;
    NSDictionary *dictColors;
}

@property (weak, nonatomic) IBOutlet UITextView *sourceView;

@property (nonatomic, retain) IGLDropDownMenu *languageSelector;
@property (nonatomic, retain) IGLDropDownMenu *exportSelector;
@property (nonatomic, weak) IBOutlet UIView *viewMenuLeft;
@property (nonatomic, weak) IBOutlet UIView *viewMenuRight;

@property (nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation SourceCodeViewController

+ (void) configureColorForMenu:(IGLDropDownMenu *)menu
{
    UIColor *colorButton = [Preferences sharedPreferences].currentThemeColors[kRegexHighlightViewTypeButton];
    if (!colorButton) { colorButton = [UIColor colorWithWhite:0.97 alpha:1]; }
    menu.colorFon = colorButton; // Цвет фона кнопок
    
    
    UIColor *colorText = [Preferences sharedPreferences].currentThemeColors[kRegexHighlightViewTypeText];
    if (!colorText) { colorText = [UIColor colorWithWhite:0.2 alpha:1]; }
    menu.textColor = colorText; // Цвет текста кнопок
    
    menu.borderColor = colorText; // Цвет рамки
}


- (id) init
{
	if (self = [super initWithNibName:[[self class] description] bundle:nil]) {
		
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
	prefs = [Preferences sharedPreferences];

    if (prefs.adIsOff == NO) {
        // Для полноэкранной рекламы
        self.interstitial = [self createAndLoadInterstitial];
    }

//    self.sourceView.editable = NO;
    
    // <---- Чтобы было нагляднее редактировать в сториборде
    self.viewMenuLeft.backgroundColor   = [UIColor clearColor];
    self.viewMenuRight.backgroundColor  = [UIColor clearColor];
    self.sourceView.backgroundColor     = [UIColor clearColor];
	// ---->
    
	rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(themeSelectorClicked:)];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    [self configureFont]; // <- вошло в configureFont-- [self configureThemeAndHighlight]; [self setupSource];
    
    //[self.sourceView becomeFirstResponder];
    
    //
    //-------------------------
    // Set up Export selector
    //
    self.exportSelector = [[IGLDropDownMenu alloc] init];
    self.exportSelector.frame = CGRectMake(0, self.viewMenuLeft.frame.size.height - 40, 140, 40);
    self.exportSelector.paddingLeft = 5.0f;
    self.exportSelector.delegate = self;
    self.exportSelector.menuText = [NSString stringWithFormat:@" %@", RStr(@"Export")];
    self.exportSelector.type = IGLDropDownMenuTypeStack;
    self.exportSelector.gutterY = 5;
    self.exportSelector.itemAnimationDelay = 0.0;
    self.exportSelector.direction = IGLDropDownMenuDirectionUp;
    self.exportSelector.menuButtonStatic = YES;

    UIImage *imgLeft = [UIImage imageNamed:@"bt_Share-128"];
    self.exportSelector.menuIconImage = imgLeft;
    self.exportSelector.scaleImage = 0.85f; // уменьшим картинку

	
	NSMutableArray *dropdownItems = [[NSMutableArray alloc] init];
	
    NSInteger coinLeft = [Preferences sharedPreferences].exportCoins;
    // Если куплено всё то уберем кнопку чтобы не мозолила
    //BOOL printAvailable = (prefs.exportCoins > 0); // засунуть это в префы потом
    //if (!printAvailable) {
    if (prefs.exportCoins < 100000) {
    //if (1 == 1) {
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        item.indexLDD = Index_Coin;
        item.scaleImage = 0.7f;
        item.text = [NSString stringWithFormat:@" %@: %ld", RStr(@"Coins left"), (unsigned long)coinLeft];
        item.iconImage = [UIImage imageNamed:@"bt_Coin"];
        [dropdownItems addObject:item];
    }
    
    NSArray *arrayMenuExport = @[RStr(@"Print B/W"), RStr(@"Print Color"), RStr(@"Export PDF"), RStr(@"Export Text")];
    NSArray *arrayImageName = @[RStr(@"bt_PrintBW-128"), RStr(@"bt_PrintColor-128"), RStr(@"bt_PDF-128"), RStr(@"bt_TXT-128")];
    NSArray *arrayIndexes = @[@(Index_Print_BW),@(Index_Print_Color),@(Index_Export_PDF),@(Index_Export_TXT)];
    for (int i = 0; i < arrayMenuExport.count; i++) {
        IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        item.indexLDD = [arrayIndexes[i] integerValue];
        item.text = [NSString stringWithFormat:@" %@", arrayMenuExport[i]];
        item.iconImage = [UIImage imageNamed:arrayImageName[i]];
        item.scaleImage = 0.7f;
        [dropdownItems addObject:item];
    }
    self.exportSelector.dropDownItems = dropdownItems;

    [self.viewMenuLeft addSubview:self.exportSelector];
    [self.exportSelector reloadView];
    //-------------------------

    
    //-------------------------
	// Set up language selector
	//
	self.languageSelector = [[IGLDropDownMenu alloc] init];
    
    self.languageSelector.frame = CGRectMake(0, self.viewMenuRight.frame.size.height - 40, 140, 40);
    
	self.languageSelector.delegate = self;
    NSString *menuText = prefs.currentLanguage;
    if (!menuText || menuText.length<1) {
        menuText = RStr(@"Choose Language");
    }
	self.languageSelector.menuText = menuText;
    self.languageSelector.paddingLeft = 5.0f;
	self.languageSelector.type = IGLDropDownMenuTypeStack;
	self.languageSelector.gutterY = 5;
	self.languageSelector.itemAnimationDelay = 0.0;
    self.languageSelector.scaleImage = 0.7f;
	self.languageSelector.direction = IGLDropDownMenuDirectionUp;
    dropdownItems = [[NSMutableArray alloc] init];
	for (NSString *langKey in _examples) {
		IGLDropDownItem *item = [[IGLDropDownItem alloc] init];
        item.text = [NSString stringWithFormat:@" %@", langKey];
        if (langKey) {
            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"logoM_%@",prefs.extensions[langKey]]];
            // уменьшим картинку на множитель
            //img = [img resizeImageToDimension:self.languageSelector.frame.size.height*0.85];
            if (img) {
                item.iconImage = img;
                item.scaleImage = 0.7f;
                if (prefs.currentLanguage && [langKey isEqualToString:prefs.currentLanguage]) {
                    // Если это текущий язык, то картинку и надпись на кнопку
                    self.languageSelector.menuIconImage = img;
                    self.languageSelector.menuText = item.text;
                }
            }
        }
		[dropdownItems addObject:item];
	}
	self.languageSelector.dropDownItems = dropdownItems;

    [self.viewMenuRight addSubview:self.languageSelector];
    [self.languageSelector reloadView];
    //-------------------------

    [SourceCodeViewController configureColorForMenu:self.exportSelector];
    [SourceCodeViewController configureColorForMenu:self.languageSelector];

    // Добавим отступ снизу, чтобы текст из под кнопок вылезал тоже
    self.sourceView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0); //{top, left, bottom, right}
    [self.view layoutIfNeeded];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self coinChanged:nil];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
    [nc addObserver:self selector:@selector(preferredFontChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
    [nc addObserver:self selector:@selector(coinChanged:) name:VVVexportCoinUsed object:nil];

}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) coinChanged:(NSNotification *) note
{
    // После обновления монет обновить кнопку
    NSInteger coins = [Preferences sharedPreferences].exportCoins;
    for (IGLDropDownItem *item in self.exportSelector.dropDownItems) {
        if (item.indexLDD == Index_Coin) {
            if (coins > 1000000) {
                // Лучше бы сразу удалить эту кнопку, но это муторно
                // поэтому покажем бесконечность, а при следующем обновлении вьюхи
                // эта кнопка больше не будет мозолить глаза
                item.text = [NSString stringWithFormat:@" %@: ∞", RStr(@"Coins left")];
            } else {
                item.text = [NSString stringWithFormat:@" %@: %ld", RStr(@"Coins left"), (unsigned long)coins];
            }
        }
    }
    
    //[self.exportSelector reloadView];
}

#pragma mark - Методы делегата Меню IGLDropDownMenuDelegate

- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndex:(NSInteger)index
{
    if (dropDownMenu == self.languageSelector) {
        IGLDropDownItem *item = dropDownMenu.dropDownItems[index];
        dropDownMenu.menuIconImage = item.iconImage;
        //DLog(@"\nitem.text=%@",item.text);
        // Обрежем пробелы в начале и конце, так как название языка совпадает с ключем
        // бред, надо бы потом ввести поле в кнопочку для ключа
        prefs.currentLanguage = [[item.text componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:@""]; // при условии, что там будет то что надо, иначе через словарик переделать
        [prefs flush];
        [self setupSource];
        //[self.sourceView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES]; // Пролистать к началу
        
    }
}
- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu selectedItemAtIndexLDD:(NSInteger)indexLDD
{
    if (dropDownMenu == self.exportSelector) { // Print
        if (indexLDD == Index_Coin) { //  DLog(@"Монетки однако...");
            [self openCoinShop];
        } else if (indexLDD == Index_Print_BW) { // Печать черно-белая
            BOOL exportAvailable = (prefs.exportCoins > 0);
            if (exportAvailable) {
                if (prefs.currentTheme != kRegexHighlightViewThemePrinting && [self textSource]) {
                    RegexHighlightViewTheme themeOld = prefs.currentTheme;
                    prefs.currentTheme = kRegexHighlightViewThemePrinting;
                    ReportViewController *rC = [self reportController];
                    rC.textAttributedOpisanie = [self attributedStringFromString:[self textSource]];
					[rC printPDF];
					prefs.currentTheme = themeOld;
                    
                } else { // Если тема уже печатная, то не обесцвечиваем впустую
					[[self reportController] printPDF];
                }
            } else {
                [self informAboutExportCoins];
            }
        } else if (indexLDD == Index_Print_Color) { // Пачать цветная
            BOOL exportAvailable = (prefs.exportCoins > 0);
            if (exportAvailable) {
                if (prefs.currentTheme != kRegexHighlightViewThemeDefault && [self textSource]) {
                    RegexHighlightViewTheme themeOld = prefs.currentTheme;
                    prefs.currentTheme = kRegexHighlightViewThemeDefault;
                    ReportViewController *rC = [self reportController];
                    rC.textAttributedOpisanie = [self attributedStringFromString:[self textSource]];
					[rC printPDF];
                    prefs.currentTheme = themeOld;
                } else { // Если тема уже дефолтная, то не обесцвечиваем впустую
                   [[self reportController] printPDF];
                }
            } else {
                [self informAboutExportCoins];
            }
        } else if (indexLDD == Index_Export_PDF) { // TODO: Export PDF всгда в теме Default!
            BOOL exportAvailable = (prefs.exportCoins > 0);
            if (exportAvailable) {
                NSString *path;
                if (prefs.currentTheme != kRegexHighlightViewThemeDefault && [self textSource]) {
                    RegexHighlightViewTheme themeOld = prefs.currentTheme;
                    prefs.currentTheme = kRegexHighlightViewThemeDefault;
                    ReportViewController *rC = [self reportController];
                    rC.textAttributedOpisanie = [self attributedStringFromString:[self textSource]];
                    path = [self PathPDFGenerate];
                    prefs.currentTheme = themeOld;
                } else { // Если тема уже без фона, то не обесцвечиваем впустую
                    path = [self PathPDFGenerate];
                }
                // Экспорт PDF с номерами строк куда нибудь. На симуляторе это не работает, только на девайсе
                UIDocumentInteractionController * docController = [[UIDocumentInteractionController alloc] init];
                docController.delegate = self;
                docController.URL = [NSURL fileURLWithPath:path];
                [docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
            } else {
                [self informAboutExportCoins];
            }
        } else if (indexLDD == Index_Export_TXT) { // Export Text
            // Экспорт простого текста без номеров строк куда нибудь. Или отправлять файл с расширением исходным?
            //На симуляторе это не работает, только на девайсе
            BOOL exportAvailable = (prefs.exportCoins > 0);
            if (exportAvailable) {
                NSString *text = [self textSource];
                if (text) {
                    NSString *file=[NSString stringWithFormat: @"%@/Documents/%@",NSHomeDirectory(),@"SourcePatternFromDesignPatterns.txt"];
                    DLog(@"Export TXT to:\n%@",file);
                    [text writeToFile:file atomically:YES encoding:NSUTF8StringEncoding error:nil];
                    UIDocumentInteractionController * docController = [[UIDocumentInteractionController alloc] init];
                    docController.delegate = self;
                    docController.URL = [NSURL fileURLWithPath:file];
                    [docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
                }
            }
            else {
                [self informAboutExportCoins];
            }
        }
    }
}

//
//Списываем монетку, если юзер на самом деле отправил данные во внешнее приложние
//
- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(nullable NSString *)application {
	[prefs removeOneCoin];
}

- (void) openCoinShop
{
    CoinShopTableViewController *v = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinShopTableViewController"];
    [self.navigationController pushViewController:v animated:YES];
}


//- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu expandingChangedWithAnimationCompledted:(BOOL)isExpanding { }

- (void)dropDownMenu:(IGLDropDownMenu *)dropDownMenu expandingChanged:(BOOL)isExpanding {
    if (dropDownMenu == self.languageSelector) {
        if (isExpanding) {
            for (IGLDropDownItem *itemMenu in dropDownMenu.dropDownItems) {
                // Выделим жирным текущий язык
                if ([itemMenu.text rangeOfString:prefs.currentLanguage].location != NSNotFound) {
                    itemMenu.textLabel.font = [UIFont systemFontOfSize:itemMenu.textLabel.font.pointSize weight:UIFontWeightBold];
                    itemMenu.textLabel.textColor = [UIColor redColor];
                } else {
                    itemMenu.textLabel.font = [UIFont systemFontOfSize:itemMenu.textLabel.font.pointSize weight:UIFontWeightRegular];
                    itemMenu.textLabel.textColor = dropDownMenu.menuButton.textLabel.textColor;
                }
            }
        }
        
    }
}

#pragma mark -

- (ReportViewController*)reportController {
    ReportViewController *report = [[ReportViewController alloc] init];
    //Добавить размеченный текст
    report.textAttributedOpisanie = self.sourceView.attributedText;
    report.textOpisanie  = nil;//self.sourceView.text;
    report.textZagolovok = [NSString stringWithFormat:@"%@, %@", self.navigationController.navigationItem.title, prefs.currentLanguage];
    report.imageDiagram  = nil;
    return report;
}
- (NSString*)PathPDFGenerate
{
    NSString *pdfPath = [[self reportController] pathFromFilePDF];
    return pdfPath;
}

- (IBAction)themeSelectorClicked:(id)sender
{
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		if (!themeViewController) {
			themeViewController = [[ThemeSelectorViewController alloc] init];
		}
		// iPad - use standard popup view
		themeViewController.modalPresentationStyle = UIModalPresentationPopover;
		[self presentViewController:themeViewController animated:YES completion:nil];
		
		// configure the Popover presentation controller
		UIPopoverPresentationController *popController = [themeViewController popoverPresentationController];
		popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
		popController.barButtonItem = rightButton;
		popController.delegate = self;
	} else {
		if (!themeViewController) {
			themeViewController = [[ThemeSelectorViewController alloc] init];
			themeViewController.parent = self;
			[self.view addSubview:themeViewController.view];
		}
		if (themeViewController.isViewVisible) {
			[themeViewController hideFromScreen];
		} else {
			[themeViewController showOnScreen];
		}

	}
}

#pragma mark - Theme support -

- (void) configureThemeAndHighlight
{
	NSDictionary *cTheme = prefs.currentThemeColors;
	backColor = cTheme[kRegexHighlightViewTypeBackground];
    lineNumColor = cTheme[kRegexHighlightViewTypeNumber];
    textColor = cTheme[kRegexHighlightViewTypeText];
    
    dictColors = prefs.currentThemeColors;

	self.view.backgroundColor = backColor;
    
    [SourceCodeViewController configureColorForMenu:self.exportSelector];
    [SourceCodeViewController configureColorForMenu:self.languageSelector];

}
//- (void) configureColorForMenu:(IGLDropDownMenu *)menu
//{
//    UIColor *colorButton = prefs.currentThemeColors[kRegexHighlightViewTypeButton];
//    if (!colorButton) { colorButton = [UIColor colorWithWhite:0.97 alpha:1]; }
//    menu.colorFon = colorButton; // Цвет фона кнопок
//    
//    
//    UIColor *colorText = prefs.currentThemeColors[kRegexHighlightViewTypeText];
//    if (!colorText) { colorText = [UIColor colorWithWhite:0.2 alpha:1]; }
//    menu.textColor = colorText; // Цвет текста кнопок
//    
//    menu.borderColor = colorText; // Цвет рамки
//}

- (void) themeChanged:(NSNotification *) note
{
 	[self configureThemeAndHighlight];
    [self setupSource];
    [self.view setNeedsDisplay];
}

#pragma mark - Масштабирование шрифтов

- (void) preferredFontChanged:(NSNotification *)notification
{
    [self configureFont];
}

- (void) configureFont
{
    [self configureThemeAndHighlight];
    [self setupSource];
    
   // [self.view setNeedsDisplay];
}

#pragma mark -
- (NSString*) textSource {
   	NSString *text = self.examples[prefs.currentLanguage];
    if (!text) {
        // Возможно, пример для этого языка отсутствует.
        // В этом случае нам необходимо поставить вариант C\++, а если его нет - первый попавшийся
        NSString *selectedKey =  nil;
        for (NSString *key in self.examples) {
            if ([key isEqualToString:@"C++"]) {
                prefs.currentLanguage = @"C++";
                text = self.examples[@"C++"];
                break;
            } else {
                if (!selectedKey) {
                    selectedKey = key;
                    prefs.currentLanguage = key;
                    text = self.examples[key];
                }
            }
        }
    }
    return text;
}

- (void) setupSource
{
    NSString *text = [self textSource];
	if (text) {
        self.sourceView.scrollEnabled = NO;  // Надо выключить, после изменения включить, иначе не работает правильно!
        self.sourceView.attributedText = [self attributedStringFromString:text];
        self.sourceView.scrollEnabled = YES; // а после изменения включить, иначе не работает правильно!

        dispatch_async(dispatch_get_main_queue(), ^{  // !!! - 3
            // Скролим текст вверх до навигатионбара.
            // запускать в этой обертке, иначе не будет правильно скролиться
            [self.sourceView setContentOffset:CGPointMake(0, -self.sourceView.contentInset.top) animated:NO];
        });
 	}
}
- (NSAttributedString*)attributedStringFromString:(NSString*)text{
    // разметка текста для отображения с текущей темой
    // RegexHighlightViewTheme *theme = prefs.currentTheme;
    // Формируем имя для словарика разметки
    NSDictionary *plistName = prefs.extensions;
    NSBundle *mb = [NSBundle mainBundle];
    NSString *path = [mb pathForResource:plistName[prefs.currentLanguage] ofType:@"plist"];
    NSDictionary *highlightDict = nil;
    if (path) {
        highlightDict = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    if (!highlightDict) {
        path = [mb pathForResource:@"default" ofType:@"plist"];
        highlightDict =  [NSDictionary dictionaryWithContentsOfFile:path];
        
    }
    // Теперь надо разметить файл. При этом табуляторы заменяются на 4 пробела
    // В начало добавляется "<номер строки>:  " и расцвечиваются ключевые слова и прочие элементы
    // Исходная строка преобрахуется в атрибутированную с расстновкой параметров каждого элемента
    // и помещается в UITextView
    
    NSString* newString = [text stringByReplacingOccurrencesOfString:@"\t" withString:@"    "];
    
#warning iPhone4 Текст слишком большой, нужно наверное scaleFont 0.3 для малых экранов
    CGFloat scaleFont = 0.7f;
    UIFont *currentFont = [prefs fontForTheme:prefs.currentTheme key:kRegexHighlightViewDefaultFont scale:scaleFont];
    
    // Атрибуты для всего текста по умолчанию
    NSDictionary *attrDict = @{
                               NSForegroundColorAttributeName : textColor,
                               NSFontAttributeName            : currentFont
                               };
    
    // Всему тексту применим атрибуты по умолчанию
    NSRange range = NSMakeRange(0,[newString length]);
    NSMutableAttributedString* coloredString = [[NSMutableAttributedString alloc] initWithString:newString attributes:attrDict];
    
    //For each definition entry apply the highlighting to matched ranges
    UIColor  * currentColor;
    NSString * expression;
    UIFont   * coloredFont;
    NSArray  * matches;
    // Малой кровью:) но можно и переделать все определения highlightDict на массивы?
    NSArray *sortedKey = @[kRegexHighlightViewTypeKeyword, kRegexHighlightViewTypeURL, kRegexHighlightViewTypeProject, kRegexHighlightViewTypeAttribute, kRegexHighlightViewTypeNumber, kRegexHighlightViewTypeCharacter, kRegexHighlightViewTypeString, kRegexHighlightViewTypeComment, kRegexHighlightViewTypeDocumentationComment, kRegexHighlightViewTypeDocumentationCommentKeyword, kRegexHighlightViewTypePreprocessor, kRegexHighlightViewTypeOther];
    for(NSString* key in sortedKey) {
    //for(NSString* key in highlightDict) {
        expression = highlightDict[key];
        if(!expression||[expression length]<=0) continue;
        
        // Подготовим цвет и шрифт для текушей замены
        currentColor = dictColors[key];
        if (!currentColor) { currentColor = textColor; } // Если нет, то по умолчанию назначим
        coloredFont = [prefs fontForTheme:prefs.currentTheme key:key scale:scaleFont];
        attrDict = @{
                     NSForegroundColorAttributeName : currentColor,
                     NSFontAttributeName            : coloredFont,
                     };
        // Найдем по маске нужные фрагменты и применим атрибуты
        matches = [[NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionDotMatchesLineSeparators error:nil] matchesInString:newString options:0 range:range];
        for(NSTextCheckingResult* match in matches) {
            //[coloredString addAttributes:attrDict range:[match rangeAtIndex:0]];
            // Переберем всё найденное кроме основного блока (с индеком 0 который)
            for (long nomerRange = 1; nomerRange<match.numberOfRanges; nomerRange++) {
                [coloredString addAttributes:attrDict range:[match rangeAtIndex:nomerRange]];
            }
        }
    }
    
    //-------------
    // В начало добавляется "\t<номер строки>:\t" с табуляторами, для выравнивания
    // Найдем все начала строк и добавим туда эти номера строк
    //expression = @"\n"; // Для однотипности добавить надо
    
    if ([currentFont.fontName rangeOfString:@"Courier" options:NSCaseInsensitiveSearch].location == NSNotFound) {
        // Если шрифт не моноширинный, то сделаем его для цифр таковым того же размера и толщины
        // TODO: Италик не учтен как то бредово все. пока только три типа жирности...
        CGFloat weight = UIFontWeightRegular;
        if ([currentFont.fontName rangeOfString:@"Semibold" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            weight = UIFontWeightSemibold;
        } else if ([currentFont.fontName rangeOfString:@"Bold" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            weight = UIFontWeightBold;
        }
        currentFont = [UIFont monospacedDigitSystemFontOfSize:currentFont.pointSize weight:weight];
    }
    
    // Найдем начала строк: @"^" вместе с NSRegularExpressionAnchorsMatchLines
    matches = [[NSRegularExpression regularExpressionWithPattern:@"^" options:NSRegularExpressionAnchorsMatchLines error:nil] matchesInString:coloredString.string options:0 range:range];

    attrDict = @{
                 //NSParagraphStyleAttributeName  : paragraphStyle,
                 NSForegroundColorAttributeName : lineNumColor,
                 NSFontAttributeName            : currentFont
                 };
    
    NSMutableAttributedString *attString;
    for (long i = matches.count-1; i >= 0; i--) { // От конца к началу, так как меняется длина строки
        newString = [NSString stringWithFormat:@"\t%ld:\t",(long)(i+1)];
        attString = [[NSMutableAttributedString alloc] initWithString:newString attributes:attrDict];
        [coloredString insertAttributedString:attString atIndex:[matches[i] rangeAtIndex:0].location];
    }
    //-------------
    // Теперь отформатируем абзацы и расставим табуляторы
    // *****************
    // Найдем начало строки для выравнивания ^\s*\d+\s*:\t(\s*).*$
    // заменим много пробелов на один табутятор и установим для него табстоп и выавнивание строки по этому табстопу
    // Найдем начала строк: @"^" вместе с NSRegularExpressionAnchorsMatchLines
    
    range = NSMakeRange(0,[coloredString length]);
    matches = [[NSRegularExpression regularExpressionWithPattern:@"^" options:NSRegularExpressionAnchorsMatchLines error:nil] matchesInString:coloredString.string options:0 range:range];
    // Вычислим табуляторы для выравнивания строк
    CGFloat tabStop1 = [[NSString stringWithFormat:@"%lu:",(unsigned long)matches.count] sizeWithAttributes:attrDict].width + 2;
    CGFloat tabStop2 = tabStop1 + [@"T" sizeWithAttributes:attrDict].width;
    CGFloat tabStop3 = tabStop2+1; // Его потом будем вычислять

    matches = [[NSRegularExpression regularExpressionWithPattern:@"^\\s*\\d+\\s*:\\t( *).*$" options:NSRegularExpressionAnchorsMatchLines error:nil] matchesInString:coloredString.string options:0 range:range];
    
    CGFloat spaceWeght = [@" " sizeWithAttributes:attrDict].width;
    
    NSTextCheckingResult* match;
    NSMutableParagraphStyle *parStyle;
    for (long i=matches.count-1; i>=0; i--) { // От конца к началу, так как меняется длина строки
        match = matches[i];
        if (match.numberOfRanges > 1) {
            parStyle = [NSMutableParagraphStyle new]; // Надо каждый раз новый создавать!
            range = [match rangeAtIndex:1];
            
            tabStop3 = spaceWeght * range.length + tabStop2; // Длина пробелов в начале строки
            //DLog(@"\n--->%@<---\n%f %f %f",[coloredString.string substringWithRange:range],tabStop1, tabStop2, tabStop3);
            
            parStyle.headIndent = tabStop3 ; // Отступ переноса строки
            parStyle.firstLineHeadIndent = 0.0f;
            parStyle.defaultTabInterval = spaceWeght;
            parStyle.tabStops = @[
                                  // TODO: что есть options? с ходу не нашел, пусть пустые будут пока
                                  [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentRight location:tabStop1 options:@{}],
                                  [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:tabStop2 options:@{}],
                                  [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:tabStop3 options:@{}],
                                  ];
            
            [coloredString addAttributes:@{ NSParagraphStyleAttributeName  : parStyle } range:[match rangeAtIndex:0]];
            if (range.length>0) {
                [coloredString replaceCharactersInRange:range withString:@"\t"]; // заменим пробелы одним табулятором
            }
        }
    }
    // *****************

    return coloredString;
}

#pragma mark -

- (void) informAboutExportCoins
{
    
    if (prefs.adIsOff) {
        [self showAlertInformAboutExportCoins];
    } else if (self.interstitial.isReady) {
        // Если реклама есть, то показать полноразмерный через N нажатий
        [self.interstitial presentFromRootViewController:self];
    } else {
        DLog(@" ‼️ Реклама полноэкранная не готова почему то...");
        [self showAlertInformAboutExportCoins];
    }
    
}

- (void) showAlertInformAboutExportCoins
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:RStr(@"No coins left")
                                                                   message:RStr(@"Explain coins")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:RStr(@"OK") style:UIAlertActionStyleCancel handler:^(UIAlertAction *actionHandler) {
        
    }];
    [alert addAction:alertAction];
    UIAlertAction *coinShopAction = [UIAlertAction actionWithTitle:RStr(@"Coin Shop") style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {
        [self openCoinShop];
    }];
    [alert addAction:coinShopAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

// Для межтраничной Рекламы // https: //developers.google.com/admob/ios/interstitial
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    // Когда предыдущая реклама закрыта создадим новый баннер и отобразим алерт
    [self showAlertInformAboutExportCoins];
    self.interstitial = [self createAndLoadInterstitial];
}
- (GADInterstitial *)createAndLoadInterstitial {
    // Для полноэкранной запросим новую рекламу
    GADRequest *request = [GADRequest request];
    GADInterstitial *inter = [[GADInterstitial alloc] initWithAdUnitID:GOOGLEAD_REWARD_ID];
#ifdef DEBUG
    // для отладки (не уверен надо ли это теперь)
    request.testDevices = @[ kGADSimulatorID ];
#endif
    [inter loadRequest:request];
    inter.delegate = self;
    [inter loadRequest:request];
    return inter;
}

@end
