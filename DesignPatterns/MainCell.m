//
//  MainCell.m
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 19.09.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "MainCell.h"
#import "DebugPrint.h"
#import "DetailViewController.h"
#import "Preferences.h"
#import "MainViewController.h"

NSString * const FoldingCellReusableID    =    @"FoldingCellReusableID";

@interface MainCell() {
    MainViewController *mainVC;
}
@end

@implementation MainCell

+ (UINib *) cellNib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.foregroundView.layer.cornerRadius = 10;
    self.foregroundView.layer.masksToBounds = YES;
    self.foregroundView.layer.borderWidth = 0.2f;

    self.containerView.layer.cornerRadius = 10;
    self.containerView.layer.masksToBounds = YES;
    self.containerView.layer.borderWidth = 0.2f;

    
    if ([Preferences sharedPreferences].exportCoins > COINSMNOGO) {
        // видимо куплен безлимит, удалим иконку с монетками
        [self.coinView removeFromSuperview];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)buttonPressed:(UIButton*)button {
    if (!mainVC) {
        mainVC = [self mainViewController];
    }
    [mainVC buttonCellPressed:button.tag indexPath:self.indexPath];
}

-(MainViewController*)mainViewController
{
    UIView *aView = self.superview;
    while(aView != nil) {
        if([aView isKindOfClass:UITableView.class]) {
            if ([aView respondsToSelector:@selector(dataSource)]) {
                UITableView *tV = (UITableView*)aView;
                mainVC = (MainViewController *)[tV dataSource];
                if ([mainVC isKindOfClass:MainViewController.class]) {
                    return mainVC;
                }
            }
        }
        aView = aView.superview;
    }
    return nil;
}

- (void) setIndexPath:(NSIndexPath *)indexPath
{
    _indexPath = indexPath;
    NSInteger section = indexPath.section, row = indexPath.row;
    
    Preferences *prefs = [Preferences sharedPreferences];
    CGFloat fontSize = prefs.sizeBodyFont; // Узнаем размер шрифта в зависимости от увеличения
    UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
    //UIColor *backColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    UIColor *backColor2 = prefs.currentThemeColors[kRegexHighlightViewTypeButton];

    // Фон оборота фолдингкелла чуть темнее сделаем
    self.backViewColor = [prefs darkerColorForColor:backColor2];
    
    self.foregroundView.layer.borderColor = textColor.CGColor;
    self.containerView.layer.borderColor = textColor.CGColor;

    self.foregroundView.backgroundColor = backColor2;
    self.containerView.backgroundColor = backColor2;

    // Свернутая ячейка
    NSString *title = [NSString stringWithFormat:@"PATTERN_%ld_%ld", section, row];
    self.header.textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
    self.header.font = [UIFont boldSystemFontOfSize:fontSize];
    self.header.text = RStr(title);
    
    NSString *shortText = [NSString stringWithFormat:@"PATTERN_%ld_%ld_SHORT", section, row];
    self.labelShort.font = [UIFont systemFontOfSize: MAX(10,(fontSize * 0.85))];
    self.labelShort.textColor = textColor;
    self.labelShort.text = RStr(shortText);

    // Распахнутая ячейка
    self.header2.textColor = self.header.textColor;
    self.header2.font = self.header.font;
    self.header2.text = self.header.text;
    
    NSString *descr = [NSString stringWithFormat:@"PATTERN_%ld_%ld_DESC", section, row];
    self.textViewDescription.font = [prefs fontForTheme:kRegexHighlightViewThemeDefault key:kRegexHighlightViewDefaultFont scale:1.0];
    self.textViewDescription.textColor = textColor;
    self.textViewDescription.text = RStr(descr);
    
    // Картинка с блок-схемой
    self.imageSchema.image = [prefs imageBlackOrWhiteOrBlackOnly:NO indexPath:indexPath];

    // Список картинок где есть языки примеров
    for (UIView *v in self.stackViewLanguage.subviews) {
        [v removeFromSuperview];
        //[self.stackViewLanguage removeArrangedSubview:v];
    }
    NSString *pattern = [prefs patternNameForGroup:section andItem:row];
    if (pattern) {
        // Собираем словарь с примерами
        NSDictionary *examples = [prefs fileNamesForPattern:pattern];
        for (NSString *langKey in examples) {
            UIImageView *imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"logoM_%@",prefs.extensions[langKey]]]];
            if (imgV) {
                [imgV addConstraint:[NSLayoutConstraint constraintWithItem:imgV
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:imgV
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:1
                                                                  constant:0]];
                [self.stackViewLanguage addArrangedSubview:imgV];
            }
        }
        
    }
    if (prefs.exportCoins < 1) {
        self.coinLabel.text = @"$";
    } else if (prefs.exportCoins < COINSMNOGO) {
        self.coinLabel.text = [NSString stringWithFormat:@"%ld",prefs.exportCoins];
    }
    
}

@end
