//
//  RotatedView+AnimationDelegate.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMRotatedView.h"

@interface GMRotatedView (AnimationDelegate) <CAAnimationDelegate>

- (CATransform3D) transform3D;

- (void) rotatedX:(CGFloat) angle;

- (void) foldingAnimation:(NSString *)timing from:(CGFloat)from to:(CGFloat)to duration:(NSTimeInterval) duration delay:(NSTimeInterval) delay hidden:(BOOL) hidden;

- (void) animationDidStart:(CAAnimation *)anim;

- (void) animationDidStop:(CAAnimation *)anim finished:(BOOL)flag;


@end
