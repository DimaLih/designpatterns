//
//  DetailViewController.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 28.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (readwrite, nonatomic) NSInteger group;
@property (readwrite, nonatomic) NSInteger item;

//- (BOOL) pictureAvailable;

@end

