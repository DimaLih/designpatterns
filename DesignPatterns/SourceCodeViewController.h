//
//  SourceCodeViewController.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 30.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumberedTextView.h"
#import "IGLDropDownMenu.h"
//#import "SDVersion.h"

// Для определения кнопок
#define Index_Coin          1
#define Index_Print_BW      2
#define Index_Print_Color   3
#define Index_Export_PDF    4
#define Index_Export_TXT    5


@interface SourceCodeViewController : UIViewController

//
//    key : @"C++"		value : NSString with example
//    key : @"Obj-C"	value : NSString
//	  key : @"Java"
//
//
@property (nonatomic, retain) NSDictionary *examples;

+ (void) configureColorForMenu:(IGLDropDownMenu *)menu;

@end
