//
//  Preferences.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 04.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Preferences.h"
#import  <SAMKeychain/SAMKeychain.h>
#import <SAMKeychain/SAMKeychainQuery.h>


#define OBJECT_IDENTIFIER_FOR_KEYCHAIN	@"KolbasOiD768"



#define RGBColor(R,G,B) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1]

// From NSRegexHighlight omponent

NSString *const kRegexHighlightViewDefaultFont = @"defaultfont";

NSString *const kRegexHighlightViewTypeText = @"text";
NSString *const kRegexHighlightViewTypeBackground = @"background";
NSString *const kRegexNavBarViewTypeBackground = @"navBarBackground";
NSString *const kRegexHighlightViewTypeButton = @"button";
NSString *const kRegexHighlightViewTypeComment = @"comment";
NSString *const kRegexHighlightViewTypeDocumentationComment = @"documentation_comment";
NSString *const kRegexHighlightViewTypeDocumentationCommentKeyword = @"documentation_comment_keyword";
NSString *const kRegexHighlightViewTypeString = @"string";
NSString *const kRegexHighlightViewTypeCharacter = @"character";
NSString *const kRegexHighlightViewTypeNumber = @"number";
NSString *const kRegexHighlightViewTypeKeyword = @"keyword";
NSString *const kRegexHighlightViewTypePreprocessor = @"preprocessor";
NSString *const kRegexHighlightViewTypeURL = @"url";
NSString *const kRegexHighlightViewTypeAttribute = @"attribute";
NSString *const kRegexHighlightViewTypeProject = @"project";
NSString *const kRegexHighlightViewTypeOther = @"other";


NSString * const  VVVcurrentTheme		=	@"VVV0";
NSString * const  VVVcurrentLanguage	=	@"VVV1";
NSString * const  VVVpaperSize			=	@"VVV3";
NSString * const VVVshowWalkThrough		=	@"VVV6";
// container
NSString * const VVVexportCoins			=	@"VVV4";
NSString * const VVVadOff				=	@"VVV5";

NSString * const VVVcurrentThemeChanged			=	@"VVVcurrentThemeChanged";
NSString * const VVVexportCoinUsed				=	@"VVVexportCoinUsed";
NSString * const VVVrequestToRestorePurchases	=	@"VVVrestorePurchaese";
NSString * const VVVadStatusVhanged				=	@"VVVadStatusVhanged";
NSString * const VVVcurrentLangProgChanged      =   @"VVV6";        // Изменен язык програмирования по умолчанию


@interface Preferences () {
	NSUserDefaults *prefs;
	SAMKeychainQuery *keyChainQuery;
	// Внутренние переменные для хранения в Keychain
	NSNumber *currentCoin;
	NSNumber *adShowIsOff;
}

@end


@implementation Preferences


+ (Preferences *) sharedPreferences
{
	static Preferences *_Preferences;
	if (_Preferences == nil) {
		_Preferences = [[Preferences alloc] init];
	}
	return _Preferences;
}

//
// Init set of data for case when actual preference file is not created yet
//
+ (void)initialize {
	NSMutableDictionary  *defaultValues = [NSMutableDictionary dictionary];
	
	//
	// set up intial values for user preferences
	//
	[defaultValues setObject:@(kRegexHighlightViewThemeBasic) forKey:VVVcurrentTheme];
	[defaultValues setObject:@(LanguageSrcObjC) forKey:VVVcurrentLanguage];
    [defaultValues setObject:@(PaperSheetTypeA4) forKey:VVVpaperSize]; // По умолчанию бумага A4(210x297mm)
	[defaultValues setObject:@(YES) forKey:VVVshowWalkThrough];
	
	[[NSUserDefaults standardUserDefaults] registerDefaults: defaultValues];
	
}

- (id) init {
	if (self = [super init]) {
		prefs = [NSUserDefaults standardUserDefaults];
#ifdef DEBUG
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self selector:@selector(debugUpdates:) name:VVVrequestToRestorePurchases object:nil];
#endif
		// Инициализируем криптоконтейнер и загружаем данные из него
		// если они были записаны ранее
		[self loadcontainerData];
		
	}
	return self;
}

- (void) flush {
	[self saveContainerData];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -

- (PaperSheetType) paperSize
{
    return (PaperSheetType)[prefs integerForKey:VVVpaperSize];
}

- (void) setPaperSize:(PaperSheetType)paperSize
{
    PaperSheetType oldSize = self.paperSize;
    if (oldSize != paperSize) {
        [prefs setInteger:paperSize forKey:VVVpaperSize];
    }
}

- (RegexHighlightViewTheme) currentTheme
{
	return (RegexHighlightViewTheme)[prefs integerForKey:VVVcurrentTheme];
}

- (void) setCurrentTheme:(RegexHighlightViewTheme)currentTheme
{
	RegexHighlightViewTheme oldTheme = self.currentTheme;
	if (oldTheme != currentTheme) {
		[prefs setInteger:currentTheme forKey:VVVcurrentTheme];
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVcurrentThemeChanged object:@(currentTheme)];
		[[UIApplication sharedApplication] setStatusBarStyle:([self themeIsLight] ? UIStatusBarStyleDefault : UIStatusBarStyleLightContent)];
	}
}

#pragma mark - Получение масштабированных фонтов для выбранной темы
- (CGFloat) sizeBodyFont
{
    return  [self fontForTheme:self.currentTheme key:kRegexHighlightViewDefaultFont scale:1.0].pointSize;
}
- (UIFont *) fontForTheme:(RegexHighlightViewTheme)aTheme key:(NSString*)key scale:(CGFloat)scale
{
    //TODO: !!!! Важны только Название и Тип шрифта. Исходный Размер из themeFonts учитывается!!!
    // Рамер будет зависеть только от системных настроек девайса
    // Возвращаем масштабированный шрифт с учетом системных настроек (для Исходного кода)
    // scale - коррекция чтобы скорректировать системные настройки от нашей задачи
    
    UIFont *newFont = [self allFontsForTheme:aTheme][key];
    if (!newFont) { // Чтоб в будующем не глюкануло:)
        newFont = [self allFontsForTheme:aTheme][kRegexHighlightViewDefaultFont];
        if (!newFont) {
            newFont = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        }
    }
    
    UIFontDescriptor *descriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
    
    CGFloat fontSize = descriptor.pointSize * scale;
    
    // Для iPad и iPhone ограничим размер по разному, чтобы выглядел красиво
#warning Checking This Numbers! Эти цифры еще надо отладить постепенно
    CGFloat fontSizeMinimum = 11.0, fontSizeMaximum = 24.0;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            fontSizeMinimum = 10.0; fontSizeMaximum = 36.0;
    }
    if (fontSize < fontSizeMinimum) {
        fontSize = fontSizeMinimum;
    } else if (fontSize > fontSizeMaximum) {
        fontSize = fontSizeMaximum;
    }
    newFont = [newFont fontWithSize:fontSize];

    return newFont;
}

- (NSDictionary *) allFontsForTheme:(RegexHighlightViewTheme) aTheme
{
    // Словарь настроек фонтов где ключ - предназначение подсветки для текущей темы
    return [[self themeFonts] objectForKey:@(aTheme)];
    //return [[self themeFonts] objectForKey:@(self.currentTheme)];
}

- (NSDictionary *) themeFonts
{
    static NSDictionary *themes = nil;
    if (!themes) {
        themes = @{
                   @(kRegexHighlightViewThemeBasic) : @{
                           // -kRegexHighlightViewDefaultFont - фонт по умолчанию для данной темы
//                           kRegexHighlightViewDefaultFont                       :	[UIFont monospacedDigitSystemFontOfSize:14 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14 weight:UIFontWeightThin],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont monospacedDigitSystemFontOfSize:14 weight:UIFontWeightBold],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther                         :	[UIFont systemFontOfSize:14],
                           },
                   @(kRegexHighlightViewThemeDefault) :	@{
                           kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:11 weight:UIFontWeightRegular],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:11 weight:UIFontWeightBold],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute                     :	[UIFont systemFontOfSize:14],
                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:11 weight:UIFontWeightBold],
//                           kRegexHighlightViewTypeOther                         :	[UIFont systemFontOfSize:14],
                           },
                   @(kRegexHighlightViewThemeDusk) : @{
                           kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:12 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther                         :	[UIFont systemFontOfSize:14],
                           },
                   @(kRegexHighlightViewThemeLowKey) : @{
//                           kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:19 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString                        :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute                     :	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther                         :	[UIFont systemFontOfSize:14],
                           },
                   @(kRegexHighlightViewThemeMidnight) : @{
                           kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:12 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther							:	[UIFont systemFontOfSize:14],
                           },
                   @(kRegexHighlightViewThemePresentation) : @{
                           kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:19 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther							:	[UIFont systemFontOfSize:14],
                           },
//                   @(kRegexHighlightViewThemePrinting) : @{
//                   kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:19 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther							:	[UIFont systemFontOfSize:14],
//                           },
                   @(kRegexHighlightViewThemeSunset) : @{
                           kRegexHighlightViewDefaultFont                       :	[UIFont systemFontOfSize:14 weight:UIFontWeightSemibold],
//                           kRegexHighlightViewTypeText							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeBackground					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeComment						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationComment			:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeDocumentationCommentKeyword	:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeString						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeCharacter						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeNumber						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeKeyword						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypePreprocessor					:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeURL							:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeAttribute						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeProject						:	[UIFont systemFontOfSize:14],
//                           kRegexHighlightViewTypeOther							:	[UIFont systemFontOfSize:14],
                           },
                   @(kRegexHighlightViewThemeNostalgy) : @{
                           kRegexHighlightViewDefaultFont                       :	[UIFont fontWithName:@"CourierNewPS-BoldMT" size:14],
                           },
                   };
    }
    return themes;
}

- (NSDictionary *) currentThemeColors
{
	return [[self themeColors] objectForKey:@(self.currentTheme)];
}

- (UIColor *) backgroundForTheme:(RegexHighlightViewTheme)aTheme
{
	NSDictionary *selected = [[self themeColors] objectForKey:@(aTheme)];
	return selected[kRegexHighlightViewTypeBackground];
}

- (UIColor *) foregroundForTheme:(RegexHighlightViewTheme)aTheme
{
	NSDictionary *selected = [[self themeColors] objectForKey:@(aTheme)];
	return selected[kRegexHighlightViewTypeText];
}




- (NSString *) currentLanguage
{
	LanguageSrc l = (LanguageSrc)[prefs integerForKey:VVVcurrentLanguage];
	if (l < [self languageCodes].count) {
		return [[self languageCodes] objectAtIndex:l];
	}
	return nil;
}

- (void) setCurrentLanguage:(NSString *)currentLanguage
{
	for (LanguageSrc i = LanguageSrcCPP; i <  LanguageSrcCount; i++) {
		if ([currentLanguage isEqualToString:[[self languageCodes] objectAtIndex:i]]) {
			[prefs setInteger:i forKey:VVVcurrentLanguage];
            [[NSNotificationCenter defaultCenter] postNotificationName:VVVcurrentLangProgChanged object:currentLanguage];
			return;
		}
	}
}


- (NSDictionary  *) themeColors
{
	static NSDictionary *themes = nil;
	if (!themes) {
		themes = @{
			   @(kRegexHighlightViewThemeBasic) : @{
					kRegexHighlightViewTypeText							:	RGBColor(0, 0, 0),
					kRegexHighlightViewTypeBackground					:	RGBColor(255.0, 255.0, 255.0),
					kRegexNavBarViewTypeBackground						:	RGBColor(224.0, 224.0, 224.0),
                    kRegexHighlightViewTypeButton                       :   RGBColor(244, 244, 244), //  Цвет фона кнопки и ячейки
					kRegexHighlightViewTypeComment						:	RGBColor(0.0, 142.0, 43.0),
					kRegexHighlightViewTypeDocumentationComment			:	RGBColor(0.0, 142.0, 43.0),
					kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(0.0, 142.0, 43.0),
					kRegexHighlightViewTypeString						:	RGBColor(181.0, 37.0, 34.0),
					kRegexHighlightViewTypeCharacter					:	RGBColor(0.0, 0.0, 0.0),
					kRegexHighlightViewTypeNumber						:	RGBColor(0.0, 0.0, 0.0),
					kRegexHighlightViewTypeKeyword						:	RGBColor(6.0, 63.0, 244.0),
					kRegexHighlightViewTypePreprocessor					:	RGBColor(6.0, 63.0, 244.0),
					kRegexHighlightViewTypeURL							:	RGBColor(6.0, 63.0, 244.0),
					kRegexHighlightViewTypeAttribute					:	RGBColor(0.0, 0.0, 0.0),
					kRegexHighlightViewTypeProject						:	RGBColor(49.0, 149.0, 172.0),
					kRegexHighlightViewTypeOther						:	RGBColor(49.0, 149.0, 172.0),
					},
			   @(kRegexHighlightViewThemeDefault) :	@{
					kRegexHighlightViewTypeText							:	RGBColor(0.0, 0.0, 0.0),
					kRegexHighlightViewTypeBackground					:	RGBColor(255.0, 255.0, 255.0),
					kRegexNavBarViewTypeBackground						:	RGBColor(224.0, 224.0, 224.0),
                    kRegexHighlightViewTypeButton                       :   RGBColor(244, 244, 244), //  Цвет фона кнопки и ячейки
					kRegexHighlightViewTypeComment						:	RGBColor(0.0, 131.0, 39.0),
					kRegexHighlightViewTypeDocumentationComment			:	RGBColor(0.0, 131.0, 39.0),
					kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(0.0, 76.0, 29.0),
					kRegexHighlightViewTypeString						:	RGBColor(211.0, 45.0, 38.0),
					kRegexHighlightViewTypeCharacter					:	RGBColor(40.0, 52.0, 206.0),
					kRegexHighlightViewTypeNumber						:	RGBColor(40.0, 52.0, 206.0),
					kRegexHighlightViewTypeKeyword						:	RGBColor(188.0, 49.0, 156.0),
					kRegexHighlightViewTypePreprocessor					:	RGBColor(20.0, 72.0, 48.0),
					kRegexHighlightViewTypeURL							:	RGBColor(21.0, 67.0, 244.0),
					kRegexHighlightViewTypeAttribute					:	RGBColor(150.0, 125.0, 65.0),
					kRegexHighlightViewTypeProject						:	RGBColor(77.0, 129.0, 134.0),
					kRegexHighlightViewTypeOther						:	RGBColor(113.0, 65.0, 163.0),
				   },
			   @(kRegexHighlightViewThemeDusk) : @{
					kRegexHighlightViewTypeText							:	RGBColor(255.0, 255.0, 255.0),
					kRegexHighlightViewTypeBackground					:	RGBColor(40.0, 43.0, 52.0),
					kRegexNavBarViewTypeBackground						:	RGBColor(66.0, 69.0, 78.0),
                    kRegexHighlightViewTypeButton                       :	RGBColor(22, 23, 28),
					kRegexHighlightViewTypeComment						:	RGBColor(72.0, 190.0, 102.0),
					kRegexHighlightViewTypeDocumentationComment			:	RGBColor(72.0, 190.0, 102.0),
					kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(72.0, 190.0, 102.0),
					kRegexHighlightViewTypeString						:	RGBColor(230.0, 66.0, 75.0),
					kRegexHighlightViewTypeCharacter					:	RGBColor(139.0, 134.0, 201.0),
					kRegexHighlightViewTypeNumber						:	RGBColor(139.0, 134.0, 201.0),
					kRegexHighlightViewTypeKeyword						:	RGBColor(195.0, 55.0, 149.0),
					kRegexHighlightViewTypePreprocessor					:	RGBColor(211.0, 142.0, 99.0),
					kRegexHighlightViewTypeURL							:	RGBColor(35.0, 63.0, 208.0),
					kRegexHighlightViewTypeAttribute					:	RGBColor(103.0, 135.0, 142.0),
					kRegexHighlightViewTypeProject						:	RGBColor(146.0, 199.0, 119.0),
					kRegexHighlightViewTypeOther						:	RGBColor(0.0, 175.0, 199.0),
				  },
			   @(kRegexHighlightViewThemeLowKey) : @{
					kRegexHighlightViewTypeText							:	RGBColor(0.0, 0.0, 0.0),
					kRegexHighlightViewTypeBackground					:	RGBColor(255.0, 255.0, 255.0),
					kRegexNavBarViewTypeBackground						:	RGBColor(224.0, 224.0, 224.0),
                    kRegexHighlightViewTypeButton                       :   RGBColor(244, 244, 244), //  Цвет фона кнопки и ячейки
					kRegexHighlightViewTypeComment						:	RGBColor(84.0, 99.0, 75.0),
					kRegexHighlightViewTypeDocumentationComment			:	RGBColor(84.0, 99.0, 75.0),
					kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(84.0, 99.0, 75.0),
					kRegexHighlightViewTypeString						:	RGBColor(133.0, 63.0, 98.0),
					kRegexHighlightViewTypeCharacter					:	RGBColor(50.0, 64.0, 121.0),
					kRegexHighlightViewTypeNumber						:	RGBColor(50.0, 64.0, 121.0),
					kRegexHighlightViewTypeKeyword						:	RGBColor(50.0, 64.0, 121.0),
					kRegexHighlightViewTypePreprocessor					:	RGBColor(0.0, 0.0, 0.0),
					kRegexHighlightViewTypeURL							:	RGBColor(24.0, 49.0, 168.0),
					kRegexHighlightViewTypeAttribute					:	RGBColor(35.0, 93.0, 43.0),
					kRegexHighlightViewTypeProject						:	RGBColor(87.0, 127.0, 144.0),
					kRegexHighlightViewTypeOther						:	RGBColor(87.0, 127.0, 164.0),
				},
			   @(kRegexHighlightViewThemeMidnight) : @{
				   kRegexHighlightViewTypeText							:	RGBColor(255.0, 255.0, 255.0),
				   kRegexHighlightViewTypeBackground					:	RGBColor(0.0, 0.0, 0.0),
				   kRegexNavBarViewTypeBackground						:	RGBColor(34.0, 34.0, 34.0),
                   kRegexHighlightViewTypeButton                        :	RGBColor(25, 25, 25),
				   kRegexHighlightViewTypeComment						:	RGBColor(69.0, 208.0, 106.0),
				   kRegexHighlightViewTypeDocumentationComment			:	RGBColor(59.0, 208.0, 106.0),
				   kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(69.0, 208.0, 106.0),
				   kRegexHighlightViewTypeString						:	RGBColor(255.0, 168.0, 77.0),
				   kRegexHighlightViewTypeCharacter						:	RGBColor(139.0, 138.0, 247.0),
				   kRegexHighlightViewTypeNumber						:	RGBColor(139.0, 138.0, 247.0),
				   kRegexHighlightViewTypeKeyword						:	RGBColor(224.0, 59.0, 160.0),
				   kRegexHighlightViewTypePreprocessor					:	RGBColor(237.0, 143.0, 100.0),
				   kRegexHighlightViewTypeURL							:	RGBColor(36.0, 72.0, 244.0),
				   kRegexHighlightViewTypeAttribute						:	RGBColor(79.0, 108.0, 132.0),
				   kRegexHighlightViewTypeProject						:	RGBColor(0.0, 249.0, 161.0),
				   kRegexHighlightViewTypeOther							:	RGBColor(0.0, 179.0, 248.0),
			   },
			   @(kRegexHighlightViewThemePresentation) : @{
				   kRegexHighlightViewTypeText							:	RGBColor(0.0, 0.0, 0.0),
				   kRegexHighlightViewTypeBackground					:	RGBColor(255.0, 255.0, 255.0),
				   kRegexNavBarViewTypeBackground						:	RGBColor(224.0, 224.0, 224.0),
                   kRegexHighlightViewTypeButton                        :   RGBColor(244, 244, 244), //  Цвет фона кнопки и ячейки
				   kRegexHighlightViewTypeComment						:	RGBColor(38.0, 126.0, 61.0),
				   kRegexHighlightViewTypeDocumentationComment			:	RGBColor(38.0, 126.0, 61.0),
				   kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(38.0, 126.0, 61.0),
				   kRegexHighlightViewTypeString						:	RGBColor(158.0, 32.0, 32.0),
				   kRegexHighlightViewTypeCharacter						:	RGBColor(6.0, 63.0, 244.0),
				   kRegexHighlightViewTypeNumber						:	RGBColor(6.0, 63.0, 244.0),
				   kRegexHighlightViewTypeKeyword						:	RGBColor(140.0, 34.0, 96.0),
				   kRegexHighlightViewTypePreprocessor					:	RGBColor(125.0, 72.0, 49.0),
				   kRegexHighlightViewTypeURL							:	RGBColor(21.0, 67.0, 244.0),
				   kRegexHighlightViewTypeAttribute						:	RGBColor(150.0, 125.0, 65.0),
				   kRegexHighlightViewTypeProject						:	RGBColor(77.0, 129.0, 134.0),
				   kRegexHighlightViewTypeOther							:	RGBColor(113.0, 65.0, 163.0),
				},
			   @(kRegexHighlightViewThemePrinting) : @{
				   kRegexHighlightViewTypeText							:	RGBColor(0.0, 0.0, 0.0),
				   kRegexHighlightViewTypeBackground					:	RGBColor(255.0, 255.0, 255.0),
				   kRegexNavBarViewTypeBackground						:	RGBColor(224.0, 224.0, 224.0),
                   kRegexHighlightViewTypeButton                        :   RGBColor(244, 244, 244), //  Цвет фона кнопки и ячейки
				   kRegexHighlightViewTypeComment						:	RGBColor(113.0, 113.0, 113.0),
				   kRegexHighlightViewTypeDocumentationComment			:	RGBColor(13.0, 113.0, 113.0),
				   kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(64.0, 64.0, 64.0),
				   kRegexHighlightViewTypeString						:	RGBColor(112.0, 112.0, 112.0),
				   kRegexHighlightViewTypeCharacter						:	RGBColor(71.0, 71.0, 71.0),
				   kRegexHighlightViewTypeNumber						:	RGBColor(71.0, 71.0, 71.0),
				   kRegexHighlightViewTypeKeyword						:	RGBColor(108.0, 108.0, 108.0),
				   kRegexHighlightViewTypePreprocessor					:	RGBColor(85.0, 85.0, 85.0),
				   kRegexHighlightViewTypeURL							:	RGBColor(84.0, 84.0, 84.0),
				   kRegexHighlightViewTypeAttribute						:	RGBColor(129.0, 129.0, 129.0),
				   kRegexHighlightViewTypeProject						:	RGBColor(120.0, 120.0, 120.0),
				   kRegexHighlightViewTypeOther							:	RGBColor(86.0, 86.0, 86.0),
			   },
			   @(kRegexHighlightViewThemeSunset) : @{
				   kRegexHighlightViewTypeText							:	RGBColor(0.0, 0.0, 0.0),
				   kRegexHighlightViewTypeBackground					:	RGBColor(255.0, 252.0, 232.0),
				   kRegexNavBarViewTypeBackground						:	RGBColor(224.0, 221.0, 201.0),
                   kRegexHighlightViewTypeButton                       :	RGBColor(255, 249, 201),
				   kRegexHighlightViewTypeComment						:	RGBColor(208.0, 135.0, 36.0),
				   kRegexHighlightViewTypeDocumentationComment			:	RGBColor(208.0, 134.0, 36.0),
				   kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(208.0, 134.0, 36.0),
				   kRegexHighlightViewTypeString						:	RGBColor(232.0, 35.0, 0.0),
				   kRegexHighlightViewTypeCharacter						:	RGBColor(53.0, 86.0, 138.0),
				   kRegexHighlightViewTypeNumber						:	RGBColor(53.0, 86.0, 138.0),
				   kRegexHighlightViewTypeKeyword						:	RGBColor(41.0, 66.0, 119.0),
				   kRegexHighlightViewTypePreprocessor					:	RGBColor(100.0, 100.0, 133.0),
				   kRegexHighlightViewTypeURL							:	RGBColor(67.0, 73.0, 172.0),
				   kRegexHighlightViewTypeAttribute						:	RGBColor(44.0, 50.0, 157.0),
				   kRegexHighlightViewTypeProject						:	RGBColor(180.0, 69.0, 0.0),
				   kRegexHighlightViewTypeOther							:	RGBColor(180.0, 69.0, 0.0),
			   },
               @(kRegexHighlightViewThemeNostalgy) : @{
                   kRegexHighlightViewTypeText							:	RGBColor(255, 255, 255),
                   kRegexHighlightViewTypeBackground					:	RGBColor(0, 0, 128),
                   kRegexNavBarViewTypeBackground						:	RGBColor(0, 0, 165),
                   kRegexHighlightViewTypeButton                       :	RGBColor(0, 0, 90),
                   kRegexHighlightViewTypeComment						:	RGBColor(192, 192, 192),
                   kRegexHighlightViewTypeDocumentationComment			:	RGBColor(192, 192, 192),
                   kRegexHighlightViewTypeDocumentationCommentKeyword	:	RGBColor(192, 192, 192),
                   kRegexHighlightViewTypeString						:	RGBColor(255, 253, 0),
                   kRegexHighlightViewTypeCharacter						:	RGBColor(0, 255, 30),
                   kRegexHighlightViewTypeNumber						:	RGBColor(0, 255, 30),
                   kRegexHighlightViewTypeKeyword						:	RGBColor(255, 255, 0),
                   kRegexHighlightViewTypePreprocessor					:	RGBColor(0, 128, 0),
                   kRegexHighlightViewTypeURL							:	RGBColor(0, 252, 30),
                   kRegexHighlightViewTypeAttribute						:	RGBColor(0, 255, 0),
                   kRegexHighlightViewTypeProject						:	RGBColor(74, 115, 238),
                   kRegexHighlightViewTypeOther							:	RGBColor(176, 147, 186),
                },
//                                           0,     0,   0 --  0, чёрный
//                                           0,     0, 128 --  1, синий
//                                           0,   128,   0 --  2, зелёный
//                                           0,   128, 128 --  3, бирюзовый
//                                           128, 0,     0 --  4, красный
//                                           128, 0,   128 --  5, фиолетовый
//                                           128, 128,   0 --  6, коричневый
//                                           192, 192, 192 --  7, серый
//                                           128, 128, 128 --  8, тёмно-серый
//                                             0,   0, 255 --  9, светло-синий
//                                             0, 255,   0 -- 10, светло-зелёный
//                                             0, 255, 255 -- 11, светло-бирюзовый
//                                           255,   0,   0 -- 12, светло-красный
//                                           255,   0, 255 -- 13, светло-фиолетовый
//                                           255, 255,   0 -- 14, жёлтый
//                                           255, 255, 255 -- 15, белый
			   };
			}
	return themes;
}


- (NSArray *) languageCodes
{
    // значения ключей из extensions
    return [[self extensions] allKeys];
}
- (NSDictionary *) extensions
{
    return @{
             // При добавлении не забыть попровить typedef enum { } LanguageSrc

             @"C++"         : @"cpp",
             @"Objective-C" : @"objc", // кое где было //@"Objective-C" : @"objectivec",
             @"Swift"       : @"swift",
             @"Java"        : @"java",
             @"Perl"        : @"perl",
             @"PHP"         : @"php",
             @"Delphi"      : @"delphi"
             
             };
}

- (NSDictionary *) fileNamesForPattern:(NSString *)patternName
{
    NSDictionary *extensions = [self extensions];
	NSBundle *mb = [NSBundle mainBundle];
	NSMutableDictionary *md = [[NSMutableDictionary alloc] initWithCapacity:extensions.count];
	for (NSString *ext in extensions) {
		NSString *fName = [NSString stringWithFormat:@"%@-%@",patternName, extensions[ext]];
		NSString *pathName = [mb pathForResource:fName ofType:nil];
		if (pathName) {
			NSError *error = nil;
			NSString *srcString = [[NSString alloc] initWithContentsOfFile:pathName encoding:NSUTF8StringEncoding error:&error];
			if (!error) {
				[md setObject:srcString forKey:ext];
			}
		}
	}
	return [NSDictionary dictionaryWithDictionary:md];
}

- (NSString *) patternNameForGroup:(NSInteger)group andItem:(NSInteger)item;
{
    NSArray *section0 = @[
                          @"AbstractFactory",
                          @"Builder",
                          @"Factory",
                          @"LazyInit",
                          @"ObjectPool",
                          @"Prototype",
                          @"Singleton",
                          @"Multiton",
                          ];
    NSArray *section1 = @[
                          @"Adapter",
                          @"Bridge",
                          @"Composite",
						  @"Decorator",
						  @"Facade",
						  @"FrontController",
						  @"FlyWeight",
						  @"Proxy",
                          ];
	NSArray *section2 = @[
						  @"ChainOfResponsibility",
						  @"Command",
						  @"Interpreter",
						  @"Iterator",
						  @"Mediator",
						  @"Memento",
						  @"NullObject",
						  @"Observer",
						  @"State",
						  @"Strategy",
						  @"TemplateMethod",
						  @"Visitor",
						  ];
	
	if (item < 0) {
		return nil;
	}
	
	if (group == 0) {
		if (item < section0.count) {
			return section0[item];
		}
	} else if (group == 1) {
		if (item < section1.count) {
			return section1[item];
		}
	}
	if (item < section2.count) {
		return section2[item];
	}
	
	return nil;
}


- (BOOL) themeIsLight
{
	switch (self.currentTheme) {
		case kRegexHighlightViewThemeBasic:	return YES;
		case kRegexHighlightViewThemeDusk:	return NO;
		case kRegexHighlightViewThemeDefault:	return YES;
		case kRegexHighlightViewThemeLowKey:	return YES;
		case kRegexHighlightViewThemeMidnight:	return NO;
		case kRegexHighlightViewThemeNostalgy:	return NO;
		case kRegexHighlightViewThemePresentation:	return YES;
		case kRegexHighlightViewThemePrinting:	return YES;
		case kRegexHighlightViewThemeSunset:	return YES;
		default:
			return YES;
	}
}



- (void) add10Exports
{
	NSUInteger cCoin = self.exportCoins;
	cCoin += 10;
	self.exportCoins = cCoin;
}

- (void) add25Exports
{
	NSUInteger cCoin = self.exportCoins;
	cCoin += 25;
	self.exportCoins = cCoin;
}

- (void) addEternityExports
{
	NSUInteger cCoin = self.exportCoins;
    cCoin += COINSMAX; // 100000000;
	self.exportCoins = cCoin;
}

#ifdef DEBUG
- (void) debugUpdates:(NSNotification *) note
{
	[self add10Exports];
}
#endif


//
// Тратим одну экспортную монетку
//
- (void) removeOneCoin
{
	NSUInteger cCoin = self.exportCoins;
	if (cCoin < 100000) {
		// богатый тот, кто не платит...
		cCoin--;
		self.exportCoins = cCoin;
	}
}

- (void) setExportCoins:(NSUInteger)exportCoins
{
	NSString *coinAsStr = [NSString stringWithFormat:@"%ld", (long)exportCoins];
    DLog(@"🦀 🦀 🦀 %@",coinAsStr);
    currentCoin = @(exportCoins);
	
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVexportCoinUsed object:nil];

}

- (NSInteger) isExportCoins
{
	return currentCoin.integerValue;
}

- (BOOL) adIsOff
{
	return adShowIsOff.boolValue;
}

- (void) setAdIsOff:(BOOL)adIsOff
{
	adShowIsOff = @(adIsOff);
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVadStatusVhanged object:adShowIsOff];
}

- (BOOL) showWalkThrough
{
	return [prefs boolForKey:VVVshowWalkThrough];
}

- (void) setShowWalkThrough:(BOOL)showWalkThrough
{
	[prefs setBool:showWalkThrough forKey:VVVshowWalkThrough];
}


#pragma mark -  SAM Криптоконтейнер

- (void) loadcontainerData
{
	keyChainQuery = [[SAMKeychainQuery alloc] init];
	keyChainQuery.password = [self prepareP];
	keyChainQuery.service = @"DPbEService";
	keyChainQuery.account = OBJECT_IDENTIFIER_FOR_KEYCHAIN;
	keyChainQuery.label = @"DesignPatternsByExample";
	keyChainQuery.synchronizationMode = YES;
	
	NSError *error = nil;
	BOOL fetched = [keyChainQuery fetch:&error];
	if (fetched) {
		NSDictionary *lookup = (NSDictionary *)keyChainQuery.passwordObject;
		if (lookup) {
			currentCoin = lookup[VVVexportCoins];
			adShowIsOff = lookup[VVVadOff];
		}
	} else {
		if (error && error.code != errSecItemNotFound) {
			DLog(@"!!!! Ошибка при загрузке из контейнера - %@",
				 [error localizedDescription]);
		} else {
			// контейнера еще нет, создаем значения по умолчанию
			// сохранять в контейнере будем позже
			currentCoin = @5;
			adShowIsOff = @NO;
		}
	}
}

- (void) saveContainerData
{
	NSDictionary *containerData = @{
									VVVexportCoins 		:	currentCoin ? currentCoin : @0,
									VVVadOff			:	adShowIsOff ? adShowIsOff : @NO,
									};
	keyChainQuery.passwordObject = containerData;
	NSError *error = nil;
	BOOL saved = [keyChainQuery save:&error];
	if (!saved) {
		if (error) {
			DLog(@"!!!! Не могу сохранить данные в контенере - %@",
				 [error localizedDescription]);
		}
	}
}

- (NSString *)prepareP
{
	NSMutableString *str = [NSMutableString new];
	NSString *a = [[NSDictionary class] description];
	NSString *b = [[UITableViewCell class] description];
	NSString *c = [[UIViewController class] description];
	
	NSInteger passLength = a.length + b.length + c.length;
	for (NSInteger i = 0; i < passLength; i++) {
		NSInteger index = i % 3;
		NSString *toAdd = @"";
		switch (index) {
			case 0: {
				NSInteger j = i / a.length;
				toAdd = [a substringWithRange:NSMakeRange(j, 1)];
				break;
			}
			case 1: {
				NSInteger j = i / b.length;
				toAdd = [b substringWithRange:NSMakeRange(j, 1)];
				break;
			}
			case 2: {
				NSInteger j = i / c.length;
				toAdd = [c substringWithRange:NSMakeRange(j, 1)];
				break;
			}
		}
		[str appendString:toAdd];
	}
	return str;
}

- (UIImage *)imageBlackOrWhiteOrBlackOnly:(BOOL)isBlackOnly indexPath:(NSIndexPath*)indexPath
{
    // Вернем картинку в цвете в зависимости от темы, или черную, если isBlackOnly==YES
    NSString *name = @"Black";
    if (self.currentTheme == kRegexHighlightViewThemeDusk || self.currentTheme == kRegexHighlightViewThemeMidnight || self.currentTheme == kRegexHighlightViewThemeNostalgy) {
        if (isBlackOnly == NO) {
            name = @"White";
        }
    }
    UIImage *image = nil;
    NSString *pattern = [self patternNameForGroup:indexPath.section andItem:indexPath.row];
    if (pattern) {
        name = [NSString stringWithFormat:@"%@_%@",pattern,name];
        // Все картинки с этими именами хранятся в Diagramms.xcassets
        image = [UIImage imageNamed:name];
    }
    
    return image;
}

- (UIColor *)darkerColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r - 0.2, 0.0)
                               green:MAX(g - 0.2, 0.0)
                                blue:MAX(b - 0.2, 0.0)
                               alpha:a];
    return nil;
}


@end
