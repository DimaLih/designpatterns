//
//  MainViewController.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/09/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "MainViewController.h"
#import "GMWalkthroughViewController.h"
#import "Preferences.h"
#import "MainCell.h"
#import "DetailViewController.h"
#import "SourceCodeViewController.h"
#import "ReportViewController.h"
#import "CoinShopTableViewController.h"
#import "GMBannerTableViewCell.h"
#import "AppDelegate.h"

static CGFloat const kCloseCellHeight = 172.0; // 140 + 16 * 2
static CGFloat const kOpenCellHeight = 438.0; // 406 + 16 * 2

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate, GADInterstitialDelegate>
{
	Preferences *prefs;
	GMWalkthroughViewController *walkVC;
    CGFloat fontSize;
    UIDocumentInteractionController * docController;
    BOOL isRunning;
    NSArray <NSNumber*>* sections;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (atomic) NSMutableDictionary *cellHeights; // высоты строк
@property (atomic) NSMutableDictionary *closedHeights; // высоты строк

@property (nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation MainViewController

- (instancetype) init
{
	if (self = [super initWithNibName:[[self class] description] bundle:nil]) {
		
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    prefs = [Preferences sharedPreferences];
    
#ifdef DEBUG
#warning TEMP disabled AD's // задолбала ошибка аудиовидео
    [prefs setAdIsOff:NO];
//    [prefs setAdIsOff:YES];
#endif

    if (prefs.adIsOff == NO) {
        // Для полноэкранной
        self.interstitial = [self createAndLoadInterstitial];
    }
    
    fontSize = prefs.sizeBodyFont;

    if (prefs.showWalkThrough) {
        [self walkShow];
        prefs.showWalkThrough = NO;
	}
    
    [self.tableView registerNib:MainCell.cellNib forCellReuseIdentifier:FoldingCellReusableID];
	[self.tableView registerNib:[GMBannerTableViewCell cellNib] forCellReuseIdentifier:GMBannerTableViewCellReusableID];

    self.cellHeights = [NSMutableDictionary new];
    self.closedHeights = [NSMutableDictionary new];
    
    sections = @[ @(8), @(8), @(12) ];
    
    self.title = RStr(@"Design patterns");

    // смена темы
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
    [self themeChanged:nil];
    
}

- (void) walkShow
{
    walkVC = [[GMWalkthroughViewController alloc] init];
    walkVC.nav = self.navigationController;
    [self.view addSubview:walkVC.view];
    [walkVC.view becomeFirstResponder];
}

- (void) viewWillAppear:(BOOL)animated
{
    //self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
    if (!isRunning) {
        [self preferredFontChanged:nil];
        isRunning = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredFontChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil]; // Слушаем изменение увеличения шрифтов из настроек сисстемы
}
- (void) viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void) themeChanged:(NSNotification *) note
{
    
    UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];;
    UIColor *backColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    UIColor *navBackColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];

    self.tableView.backgroundColor = backColor;
    self.view.backgroundColor = backColor;
    [self.tableView reloadData];

    self.navigationController.navigationBar.barTintColor = navBackColor;
    self.navigationController.navigationBar.tintColor = textColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:textColor}];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Масштабирование шрифтов

- (void) preferredFontChanged:(NSNotification *)notification
{
    fontSize = prefs.sizeBodyFont;
    
    // Высоты строк - лажа
    MainCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UILabel *label = cell.labelShort;
    CGSize size = CGSizeMake(cell.labelShort.frame.size.width, FLT_MAX);
    // устанавливаем высоты для "информационных ячеек
    for (long s = 0; s < sections.count; s++) {
        for (long r = 0; r < sections[s].integerValue; r++) {
            NSNumber *indexNumb = @(s*100+r);
            
            // Для динамической высоты
            NSString *shortText = [NSString stringWithFormat:@"PATTERN_%ld_%ld_SHORT", s, r];
            label.text = RStr(shortText);
            CGSize closeSize = [label sizeThatFits:size];
            NSNumber * newHeightNumb = @(kCloseCellHeight - 48 + closeSize.height); // 48-высота лейбла в IB!
            
            // хреново это согласуется с фолдингкелом
            //self.closedHeights[indexNumb] = newHeightNumb;
            //self.cellHeights[indexNumb] = newHeightNumb;
            
            self.closedHeights[indexNumb] = @(kCloseCellHeight);
            self.cellHeights[indexNumb] = @(kCloseCellHeight);

            
        }
    }
    // и отдельно - для рекламной
    self.cellHeights[@9999] = @([GMBannerTableViewCell cellHeight]);
    
    [self.tableView reloadData];
}


//#pragma mark - WalkThorugh delegate
//
//- (void) regButtonClicked
//{
//    [walkVC.view removeFromSuperview];
//    prefs.showWalkThrough = NO;
//}

#pragma mark - Table View

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger adCounter = prefs.adIsOff ? 0 : 1;
	return sections[section].integerValue + adCounter;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
	if (indexPath.row == 0 && !prefs.adIsOff) {
		return [self.cellHeights[@9999] doubleValue];
	}
	NSInteger adModifier = prefs.adIsOff ? 0 : -1;
	return [self.cellHeights[@(indexPath.section*100+indexPath.row+adModifier)] floatValue];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.00001;
}
- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *t = [NSString stringWithFormat:@"GROUP_%ld",(long)section];
    return RStr(t);
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    //view.tintColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
//    header.backgroundView.backgroundColor = [prefs darkerColorForColor:prefs.currentThemeColors[kRegexHighlightViewTypeButton]];
    header.tintColor = prefs.currentThemeColors[kRegexNavBarViewTypeBackground];
    header.textLabel.textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
    header.textLabel.alpha = 0.75;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0 && !prefs.adIsOff) {
		GMBannerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:GMBannerTableViewCellReusableID];
//        for(UIView* view in cell.contentView.subviews) {
//            if([view isKindOfClass:[GADBannerView class]]) {
//                [view removeFromSuperview];
//            }
//        }
		dispatch_async(dispatch_get_main_queue(), ^{
			[cell cellBannerView:self];
		});
		return cell;
	} else {
    	MainCell *cell = [tableView dequeueReusableCellWithIdentifier:FoldingCellReusableID forIndexPath:indexPath];
		NSInteger adModifier = prefs.adIsOff ? 0 : -1;
		NSIndexPath *modPath = [NSIndexPath indexPathForRow:(indexPath.row + adModifier) inSection:indexPath.section];
		cell.indexPath = modPath;
        
        NSMutableArray <NSDate*>* durations =
        [ NSMutableArray arrayWithArray: @[ [NSDate dateWithTimeIntervalSince1970: .26],
                                            [NSDate dateWithTimeIntervalSince1970: .2],
                                            [NSDate dateWithTimeIntervalSince1970: .2], ]] ;
        [cell setDurationsForExpandedState:durations];
        [cell setDurationsForCollapsedState:durations];

        
    	return cell;
	}
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(MainCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //cell.backgroundColor = [UIColor clearColor];
	if ([cell isKindOfClass:[GMFoldingCell class]]) {
		NSInteger adModifier = prefs.adIsOff ? 0 : -1;
		double cellHeight = [self.cellHeights[@(indexPath.section*100 + indexPath.row + adModifier)] doubleValue];
    	[cell unfold:(fabs(cellHeight - kOpenCellHeight) < 1.0)	animated:NO completion:nil];
	}
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    MainCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.isAnimating) return;
    
    CGFloat duration = 0;
	NSInteger adModifier = prefs.adIsOff ? 0 : -1;
    NSNumber * indexKey = @(indexPath.section*100 + indexPath.row + adModifier);
	double cellHeight = [self.cellHeights[indexKey] doubleValue];

    if (fabs(cellHeight - kOpenCellHeight) < 1.0) {
        //DLog(@"---- Схлопываем");
		
        self.cellHeights[indexKey] = self.closedHeights[indexKey];
        duration = 0.5;
        [cell unfold:NO animated:YES completion:nil];
    } else {
        //DLog(@"---- Раворачиваем");
        self.cellHeights[indexKey]  = @(kOpenCellHeight);
        duration = 0.5;
        [cell unfold:YES animated:YES completion:nil];
    }
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    } completion:^(BOOL finished) {
        //[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row + adModifier inSection:indexPath.section]] withRowAnimation:NO];
    }];
}

#pragma mark - Нажатие кнопок в ячейке

- (void) buttonCellPressed:(NSInteger)tag indexPath:(NSIndexPath*)indexPath
{
    // Рекрама уже учтена в indexPath при создании ячейки!
    
    if (tag == 1) { // экспорт пдф
        if (prefs.exportCoins > 0) {
            if (!docController) {
                docController = [[UIDocumentInteractionController alloc] init];
                docController.delegate = self;
            }
            docController.URL = [NSURL fileURLWithPath:[[self reportControllerForIndexPath:indexPath] pathFromFilePDF]];
            [docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
        } else {
            [self informAboutExportCoins];
        }

    } else if (tag == 2) { // печать
        if (prefs.exportCoins > 0) {
            [[self reportControllerForIndexPath:indexPath] printPDF];
        } else {
            [self informAboutExportCoins];
        }
    } else if (tag == 3) { // монетки
        [self openCoinShop];
    } else if (tag == 4) { // открыть код
        SourceCodeViewController * vc = (SourceCodeViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"SourceCodeViewController"];
        vc.examples = [prefs fileNamesForPattern:[prefs patternNameForGroup:indexPath.section andItem:indexPath.row]];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (tag == 5) { // монетки
        DetailViewController * vc = (DetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
        vc.group = indexPath.section;
        vc.item = indexPath.row;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (ReportViewController *) reportControllerForIndexPath:(NSIndexPath*)indexPath
{
    ReportViewController *report = [[ReportViewController alloc] init];
    //Добавить картинку и текст
    NSString *descr = [NSString stringWithFormat:@"PATTERN_%ld_%ld_DESC", indexPath.section, indexPath.row];
    report.textOpisanie  = RStr(descr);
    report.textZagolovok = self.title;
    report.imageDiagram  = [prefs imageBlackOrWhiteOrBlackOnly:YES indexPath:indexPath]; // self.itemPicture.image;
    return report;
}

- (void) openCoinShop
{
    CoinShopTableViewController *v = (CoinShopTableViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"CoinShopTableViewController"];
    v.modalPresentationStyle = UIModalPresentationFormSheet;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:v];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:RStr(@"Close") style:UIBarButtonItemStyleDone target:v action:@selector(vihod:)];
    v.navigationItem.leftBarButtonItem = cancelButton;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

//
// Списываем монетку, если юзер на самом деле отправил данные во внешнее приложние
//
- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(nullable NSString *)application {
    [prefs removeOneCoin];
}

#pragma mark -

- (void) informAboutExportCoins
{
    
    if (prefs.adIsOff) {
        [self showAlertInformAboutExportCoins];
    } else if (self.interstitial.isReady) {
            // Если реклама есть, то показать полноразмерный через N нажатий
            [self.interstitial presentFromRootViewController:self];
    } else {
        DLog(@" ‼️ Реклама полноэкранная не готова почему то...");
        [self showAlertInformAboutExportCoins];
    }

}

- (void) showAlertInformAboutExportCoins
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:RStr(@"No coins left")
                                                                   message:RStr(@"Explain coins")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:RStr(@"OK") style:UIAlertActionStyleCancel handler:^(UIAlertAction *actionHandler) {
        
    }];
    [alert addAction:alertAction];
    UIAlertAction *coinShopAction = [UIAlertAction actionWithTitle:RStr(@"Coin Shop") style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {
        [self openCoinShop];
    }];
    [alert addAction:coinShopAction];
    [self presentViewController:alert animated:YES completion:nil];

}

// Для межтраничной Рекламы // https: //developers.google.com/admob/ios/interstitial
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    // Когда предыдущая реклама закрыта создадим новый баннер и отобразим алерт
    [self showAlertInformAboutExportCoins];
    self.interstitial = [self createAndLoadInterstitial];
}
- (GADInterstitial *)createAndLoadInterstitial {
    // Для полноэкранной запросим новую рекламу
    GADRequest *request = [GADRequest request];
    GADInterstitial *inter = [[GADInterstitial alloc] initWithAdUnitID:GOOGLEAD_REWARD_ID];
#ifdef DEBUG
    // для отладки (не уверен надо ли это теперь)
    request.testDevices = @[ kGADSimulatorID ];
#endif
    [inter loadRequest:request];
    inter.delegate = self;
    [inter loadRequest:request];
    return inter;
}

@end
