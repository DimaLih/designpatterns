//
//  AppDelegate.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 28.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailViewController.h"
#import "Preferences.h"
//#import "MainViewController.h"
//#import "RootSideBarController.h"

@import GoogleMobileAds;

//
// Рекламная поддержка
//

// ID приложения
NSString * const GOOGLEAD_APP_ID	=	@"ca-app-pub-8065312395587674~5711021147";

// ID простого баннера - показывать на основной странице как первая нераспахиваемая ячейка
// в каждой секции

//
// Reward Video - использовать для предоставления юзеру возможность получить одну экспортную монетку
// за просмотр видео
//
// отладочные идентификаторы
#ifdef DEBUG
    // https://developers.google.com/admob/ios/test-ads
    NSString * const GOOGLEAD_BANNER_ID  =  @"ca-app-pub-3940256099942544/2934735716";
  //NSString * const GOOGLEAD_REWARD_ID  =  @"ca-app-pub-3940256099942544/1712485313";
    NSString * const GOOGLEAD_REWARD_ID  =  @"ca-app-pub-3940256099942544/4411468910";
#else
    NSString * const GOOGLEAD_BANNER_ID =	@"ca-app-pub-8065312395587674/4272165892";
    // ID полноэкранного баннера - показывать при экспорте, если нет монеток
    NSString * const GOOGLEAD_REWARD_ID  =  @"ca-app-pub-8065312395587674/6453151260";
#endif



@interface AppDelegate () <UISplitViewControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	[GADMobileAds configureWithApplicationID:GOOGLEAD_APP_ID];
	
	//MainViewController *mvc = [[MainViewController alloc] init];
    //RootSideBarController *mvc = [[RootSideBarController alloc] init];
	
//    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mvc];
//    self.window.rootViewController = navController;
	
	
//	UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
//	UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
//	navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem;
//	splitViewController.delegate = self;
	
	[[UIApplication sharedApplication] setStatusBarStyle:([[Preferences sharedPreferences] themeIsLight] ? UIStatusBarStyleDefault : UIStatusBarStyleLightContent)];
	
	return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	[[Preferences sharedPreferences] flush];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	[[Preferences sharedPreferences] flush];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	[[Preferences sharedPreferences] flush];
}


#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[DetailViewController class]]
        // Надо ли проверять наличие картинки?
        //&& ([(DetailViewController *)[(UINavigationController *)secondaryViewController topViewController] pictureAvailable] == YES)
        ) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    } else {
        return NO;
    }
}
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        //для iPad разрешим по любому вращать
        return UIInterfaceOrientationMaskAll;
    }
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
