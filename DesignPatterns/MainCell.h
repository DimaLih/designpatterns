//
//  MainCell.h
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 19.09.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "GMFoldingCell.h"

extern NSString * const FoldingCellReusableID;

@interface MainCell : GMFoldingCell

@property (weak, nonatomic) IBOutlet UILabel *header;
@property (weak, nonatomic) IBOutlet UILabel *header2;
@property (weak, nonatomic) IBOutlet UIImageView *imageSchema;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelShort;

@property (weak, nonatomic) IBOutlet UIView *viewDetail;

@property (weak, nonatomic) NSIndexPath *indexPath;

@property (weak, nonatomic) IBOutlet UIStackView *stackViewLanguage;
@property (weak, nonatomic) IBOutlet UIView *coinView;
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;

+ (UINib *) cellNib;

@end
