//
//  RootSideBarController.m
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 24.09.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "RootSideBarController.h"

#import "LMSideBarDepthStyle.h"
#import "SetupTVC.h"
#import "MainViewController.h"
#import "Preferences.h"
#import "SDVersion.h"

@interface RootSideBarController ()
{
    Preferences *prefs;
}

@end

@implementation RootSideBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    prefs = [Preferences sharedPreferences];
    [self themeChanged:nil];
    
    // Init view controllers
    MainViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    SetupTVC *leftMenuVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"SetupTVC"];
    

    // Setup side bar controller
    [self setPanGestureEnabled:YES];
    [self setDelegate:self];
    [self setContentViewController:mainVC];
    [self setMenuViewController:leftMenuVC forDirection:LMSideBarControllerDirectionLeft];

    // Init side bar styles and left view
    LMSideBarDepthStyle *sideBarDepthStyle = [LMSideBarDepthStyle new];
    sideBarDepthStyle.menuWidth = leftMenuVC.constraintWith.constant;
    [self setSideBarStyle:sideBarDepthStyle forDirection:LMSideBarControllerDirectionLeft];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonClicked:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    self.title = RStr(@"Design patterns");
    
    // смена темы
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeChanged:) name:VVVcurrentThemeChanged object:nil];
    [self themeChanged:nil];

}

- (void) themeChanged:(NSNotification *) note
{
    
    UIColor *textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];;
    UIColor *backColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    UIColor *navBackColor = prefs.currentThemeColors[kRegexHighlightViewTypeBackground];
    
    self.view.backgroundColor = backColor;
    
    self.navigationController.navigationBar.barTintColor = navBackColor;
    self.navigationController.navigationBar.tintColor = textColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:textColor}];
    
}

- (IBAction) menuButtonClicked:(id) sender
{
    if (self.currentState == LMSideBarControllerStateDidClose) {
        [self showMenuViewControllerInDirection:LMSideBarControllerDirectionLeft];
    } else {
        [self hideMenuViewController:YES];
    }
}

- (void)sideBarController:(LMSideBarController *)sideBarController didHideMenuViewController:(UIViewController *)menuViewController
{
    SetupTVC * setupVC = (SetupTVC *)[sideBarController menuViewControllerForDirection:LMSideBarControllerDirectionLeft];
    setupVC.cellLanguageExpanded = NO;
    [setupVC.tableView reloadData];
}

@end
