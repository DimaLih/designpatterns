//
//  RotatedView+AnimationDelegate.m
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "GMRotatedView+AnimationDelegate.h"

@implementation GMRotatedView (AnimationDelegate)

- (CATransform3D) transform3D
{
	CATransform3D transform = CATransform3DIdentity;
	transform.m34 = 2.5 / -2000;
	return transform;
}

- (void) rotatedX:(CGFloat) angle
{
	CATransform3D allTransform = CATransform3DIdentity;
	CATransform3D rotation = CATransform3DMakeRotation(angle, 1.0, 0.0, 0.0);
	allTransform = CATransform3DConcat(allTransform, rotation);
	allTransform = CATransform3DConcat(allTransform, [self transform3D]);
	self.layer.transform = allTransform;
}

- (void) foldingAnimation:(NSString *)timing from:(CGFloat)from to:(CGFloat)to duration:(NSTimeInterval) duration delay:(NSTimeInterval) delay hidden:(BOOL) hidden
{
	CABasicAnimation *rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
	rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName:timing];
	rotateAnimation.fromValue = @(from);
	rotateAnimation.toValue = @(to);
	rotateAnimation.duration = duration;
	rotateAnimation.delegate = self;
	rotateAnimation.fillMode = kCAFillModeForwards;
    [rotateAnimation setRemovedOnCompletion:NO]; // .isRemovedOnCompletion = NO;
	rotateAnimation.beginTime = CACurrentMediaTime() + delay;
	self.hiddenAfterAnimation = hidden;
	
	[self.layer addAnimation:rotateAnimation forKey:@"rotation.x"];
}

#pragma mark - CAAimation delegate

- (void) animationDidStart:(CAAnimation *)anim
{
	self.layer.shouldRasterize = YES;
	self.alpha = 1.0;
}


- (void) animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	if (self.hiddenAfterAnimation) {
		self.alpha = 0.0;
	}
	[self.layer removeAllAnimations];
	self.layer.shouldRasterize = NO;
	[self rotatedX:0.0];
}
	


@end
