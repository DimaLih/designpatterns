//
//  MainViewController.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/09/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMSideBarController.h"

@interface MainViewController : UIViewController

- (void) buttonCellPressed:(NSInteger)tag indexPath:(NSIndexPath*)indexPath;

- (void) walkShow;

@end
