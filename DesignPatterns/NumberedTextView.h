//
//  NumberedTextView.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 30.01.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NumberedTextView : UIView <UIScrollViewDelegate, UITextViewDelegate>

@property (nonatomic, readwrite) BOOL lineNumbers;
@property (nonatomic, retain) NSString *text;


@end
