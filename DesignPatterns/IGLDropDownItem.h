//
//  IGLDropDownItem.h
//  IGLDropDownMenuDemo
//
//  Created by Galvin Li on 8/30/14.
//  Copyright (c) 2014 Galvin Li. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGLDropDownItem : UIControl

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger indexLDD;
@property (nonatomic, strong) id object;

@property (nonatomic, strong, readonly) UIView *customView;

@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, strong, readonly) UILabel *textLabel;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) BOOL showBackgroundShadow;

@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) UIColor *colorFon;

@property (nonatomic, assign) CGFloat paddingLeft;
@property (nonatomic, assign) CGFloat scaleImage;

- (instancetype)initWithCustomView:(UIView*)customView;

@end
