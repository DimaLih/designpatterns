//
//  RotatedView.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 18/06/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GMRotatedView : UIView

@property (nonatomic, readwrite) BOOL hiddenAfterAnimation;
@property (weak) GMRotatedView *backView;

- (void) addBackViewWithHeight:(CGFloat) height andColor:(UIColor *)color;

@end
