//
//  Preferences.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 04.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>

// Code Text View themes

#define COINSMAX      100000000
#define COINSMNOGO    1000
#define TAGBLOCKAD    100000001

typedef enum {
	kRegexHighlightViewThemeBasic,
	kRegexHighlightViewThemeDefault,
	kRegexHighlightViewThemeDusk,
	kRegexHighlightViewThemeLowKey,
	kRegexHighlightViewThemeMidnight,
	kRegexHighlightViewThemePresentation,
	kRegexHighlightViewThemePrinting,
	kRegexHighlightViewThemeSunset,
    kRegexHighlightViewThemeNostalgy,
	
	kRegexHighlightViewThemesCount,
} RegexHighlightViewTheme;

FOUNDATION_EXPORT NSString *const kRegexHighlightViewDefaultFont;

FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeText;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeBackground;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeButton;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeComment;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeDocumentationComment;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeDocumentationCommentKeyword;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeString;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeCharacter;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeNumber;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeKeyword;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypePreprocessor;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeURL;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeAttribute;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeProject;
FOUNDATION_EXPORT NSString *const kRegexHighlightViewTypeOther;
FOUNDATION_EXPORT NSString *const kRegexNavBarViewTypeBackground;

typedef enum {
	LanguageSrcCPP	=	0,
	LanguageSrcObjC,
	LanguageSrcPerl,
	LanguageSrcJava,
    LanguageSrcSwift,
    LanguageSrcPhp,
    LanguageSrcDelphi,
    // При добавлении не забыть попровить - (NSDictionary *) extensions
	
	LanguageSrcCount,
} LanguageSrc;

typedef  NS_ENUM(NSInteger, PaperSheetType) {
    // Размеры бумаги для печати
    PaperSheetTypeA4 = 0,
    PaperSheetTypeUSLegal8_11,
    PaperSheetTypeUSLegal8_14,
};

extern NSString * const VVVcurrentThemeChanged;		// Изменена цветовая схема
extern NSString * const VVVexportCoinUsed;			// юзер потратил одну монетку
extern NSString * const VVVrequestToRestorePurchases;
extern NSString * const VVVadStatusVhanged;			// изменился статус показа рекламы
extern NSString * const VVVcurrentLangProgChanged;        // Изменен язык програмирования по умолчанию

@interface Preferences : NSObject

+ (Preferences *) sharedPreferences;

- (void) flush;

@property (nonatomic, readwrite) RegexHighlightViewTheme currentTheme;
@property (nonatomic, readonly) NSDictionary *currentThemeColors;

@property (nonatomic, retain) NSString *currentLanguage;

@property (nonatomic, readwrite) PaperSheetType paperSize;

@property (nonatomic, readwrite) NSUInteger exportCoins;	// количество монеток для экспорта

@property (nonatomic, readwrite) BOOL adIsOff;		// реклама отключена

@property (nonatomic, readwrite) BOOL showWalkThrough;	// показывать инструкцию при запуске

//
// Операции над экспортными монетками
//
- (void) add10Exports;
- (void) add25Exports;
- (void) addEternityExports;
- (void) removeOneCoin;


// Returns list of the source files 
- (NSDictionary *)  fileNamesForPattern:(NSString *)patternName;

// Словарь Название языка : абривиатура
- (NSDictionary *) extensions;

- (NSString *) patternNameForGroup:(NSInteger)indPath andItem:(NSInteger)item;

// Фонт в зависимости от выбранной темы и синтаксиса
- (UIFont *) fontForTheme:(RegexHighlightViewTheme)aTheme key:(NSString*)key scale:(CGFloat)scale;
// размер шрифта UIFontTextStyleBody
- (CGFloat) sizeBodyFont;

- (UIColor *) backgroundForTheme:(RegexHighlightViewTheme) aTheme;
- (UIColor *) foregroundForTheme:(RegexHighlightViewTheme) aTheme;


// Вернем картинку в цвете в зависимости от темы, или черную, если isBlackOnly==YES
- (UIImage *)imageBlackOrWhiteOrBlackOnly:(BOOL)isBlackOnly indexPath:(NSIndexPath*)indexPath;

- (BOOL) themeIsLight;

- (UIColor *)darkerColorForColor:(UIColor *)c;


// Определяем функцию Dlog, которая выводит NSlog только в отладке
#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define DLog(...)
#endif

#define RStr(name) NSLocalizedString(name, name)


@end

//#pragma  mark - Категория UIColor
//@interface UIColor (Utilites)
//// осветлить или затемнить цвет ЛД
//- (UIColor *) colorWithKoofHue:(CGFloat)kHue kSat:(CGFloat)kSat kBri:(CGFloat)kBri kAlpha:(CGFloat)kAlp;
//@end
//@implementation UIColor (Utilites)
//// осветлить или затемнить цвет ЛД
//- (UIColor *) colorWithKoofHue:(CGFloat)kHue kSat:(CGFloat)kSat kBri:(CGFloat)kBri kAlpha:(CGFloat)kAlp {
//    CGFloat hue, saturation, brightness, alpha;
//    [self getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
//    return [UIColor colorWithHue:hue*kHue saturation:saturation*kSat brightness:brightness*kBri alpha:alpha*kAlp];
//}
//@end

//
//#pragma  mark - Категория UIImage
//@interface UIImage (ResizeImage) // Категории UIImage расширим
//- (UIImage *) resizeImageToDimension:(CGFloat)dimension;
//@end
//@implementation UIImage (ResizeImage)
//-(UIImage *)resizeImageToDimension:(CGFloat)dimension { // Изменить размер картинки до заданного
//    if (!self) { return nil; }
//    //DLog(@"old size  %f %f",image.size.width, image.size.height);
//#warning !!! BUG !!! Надо учитывать множитель экрана!!!!
//    CGFloat aspect = self.size.width / self.size.height;
//    CGSize newSize;
//    
//    if (self.size.width > self.size.height) {
//        newSize = CGSizeMake(dimension, dimension / aspect);
//    } else {
//        newSize = CGSizeMake(dimension * aspect, dimension);
//    }
//    
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
//    CGRect newImageRect = CGRectMake(0.0, 0.0, newSize.width, newSize.height);
//    [self drawInRect:newImageRect];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    //DLog(@"new Size  %f %f",newImage.size.width, newImage.size.height);
//    return newImage;
//}
//@end
//
