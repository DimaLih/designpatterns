#!/bin/sh

#  IconGenerator.sh
#  PayBer
#
#  Created by Водолазкий В.В. on 14.03.17.
#  Copyright © 2017 infoshell. All rights reserved.

#
# see https://habrahabr.ru/post/262667/
#
#
# Предустановка:  macports/brew    ImageMagick GhostScript
#
set -x

# если у нас релизная сборка, то нам нужна обычная иконка
if [ $CONFIGURATION = "Release" ]; then
	exit
fi

# номер версии
version=`/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "${INFOPLIST_FILE}"`
# номер билда
build=`/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "${INFOPLIST_FILE}"`
# если нужно, можно взять имя ветки из git
branch=`git rev-parse --abbrev-ref HEAD`

# функция генерации иконки
function processIcon() {
export PATH=$PATH:/usr/local/bin:/opt/local/bin
	base_file=$1
	target_icon_name=$2
	base_path=`find ${SRCROOT} -name $base_file`

	if [[ ! -f ${base_path} || -z ${base_path} ]]; then
		return;
	fi

	target_file=`echo $target_icon_name | sed "s/_base//"`
	target_path="${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/${target_file}"

	width=`identify -format %w ${base_path}`

	echo $target_path
	echo $target_file

	convert -background '#00f' -fill white -gravity center -size ${width}x40 -pointsize 26\
	caption:"${version} (${build})"\
		"${base_path}" +swap -gravity south -composite "${target_path}"
}

# запускаем генерацию
convert_present=`which convert`
if [ -f ${convert-present}]; then
	processIcon "appicon-6.png" "AppIcon60x60@2x.png"
	processIcon "appicon-7.png" "AppIcon60x60@3x.png"
fi
