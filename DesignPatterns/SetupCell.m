//
//  SetupCell.m
//  Maple
//
//  Created by Водолазкий В.В. on 04.09.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "SetupCell.h"
#import "Preferences.h"
#import "SDVersion.h"

NSString * const SetupCellReusableID    =   @"SetupCellReusableID";
NSString * const LanguageCellReusableID =   @"LanguageCellReusableID";

@interface SetupCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImgButtom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLogoHeight;

@end

@implementation SetupCell


- (void)awakeFromNib {
    [super awakeFromNib];
    for (UIView *v in self.stackViewLang.arrangedSubviews) {
        [v removeFromSuperview];
    }
    
    self.constraintLogoHeight.constant = ([SDVersion deviceSize] == Screen4inch) ? 30 : 40;

}

- (void) setData:(NSArray *)data
{
    Preferences *prefs = [Preferences sharedPreferences];
    self.tag = [data[2] integerValue];
    self.name.textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];

    if (self.tag == TableTagLanguage) {
        NSDictionary *extensions = prefs.extensions;
        NSString * currentLanguage = prefs.currentLanguage;
        self.icon.image = [UIImage imageNamed:[NSString stringWithFormat:@"logoM_%@", extensions[currentLanguage]]];
        self.name.textColor = prefs.currentThemeColors[kRegexHighlightViewTypeText];
        
        if (self.expandedCell == YES) {
            //DLog(@"    Распахнем!");
            self.constraintImgButtom.constant = 10;
            for (UIView *v in self.stackViewLang.arrangedSubviews) {
                [v removeFromSuperview];
            }
            
            for (NSString *langKey in extensions.allKeys) {
                if (![langKey isEqualToString:currentLanguage]) {
                    UIButton *bt = [[UIButton alloc] init];
                     bt.imageView.contentMode = UIViewContentModeScaleAspectFit;
                    [bt setTitleColor:prefs.currentThemeColors[kRegexHighlightViewTypeText] forState:UIControlStateNormal];
                    bt.titleEdgeInsets = UIEdgeInsetsMake(0, 16, 0, 0);
                    bt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    [bt setTitle:langKey forState:UIControlStateNormal];
                    [bt setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logoM_%@", extensions[langKey]]] forState:UIControlStateNormal];
                    [bt addTarget:self action:@selector(selectLaguagePressed:) forControlEvents:UIControlEventTouchUpInside];
                    [self.stackViewLang addArrangedSubview:bt];
                    // !!! constraint добавлять после помещения кнопки на вьюшку !!!
                    [bt addConstraint:[NSLayoutConstraint constraintWithItem:bt attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:self.constraintLogoHeight.constant]];

                }
            }
        } else {
            //DLog(@"    свернем!");
            self.constraintImgButtom.constant = 0;
            for (UIView *v in self.stackViewLang.arrangedSubviews) {
                [v removeFromSuperview];
            }
        }

        self.name.text = currentLanguage;

    } else {
        self.icon.image = [UIImage imageNamed:data[1]];
        self.name.text = data[0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) selectLaguagePressed:(UIButton*)button {
    [Preferences sharedPreferences].currentLanguage = button.titleLabel.text;
}

@end
