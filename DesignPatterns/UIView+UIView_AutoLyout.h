



#import <UIKit/UIKit.h>

/**
 *  UIVew helper class to allow proper autolayout processing.
 */
@interface UIView (AutoLyout)

+(id)autolayoutView;

@end
