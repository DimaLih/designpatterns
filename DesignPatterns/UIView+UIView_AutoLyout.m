



#import "UIView+UIView_AutoLyout.h"

@implementation UIView (UIView_AutoLyout)

+(id)autolayoutView
{
	UIView *view = [self new];
	view.translatesAutoresizingMaskIntoConstraints = NO;
	return view;
}

@end
