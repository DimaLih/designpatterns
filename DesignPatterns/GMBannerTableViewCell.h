//
//  GMBannerTableViewCell.h
//  QRWallet
//
//  Created by Водолазкий В.В. on 09.02.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "CommonTableViewCell.h"
@import GoogleMobileAds;

extern NSString * const GMBannerTableViewCellReusableID;

@interface GMBannerTableViewCell : CommonTableViewCell

//@property (weak, nonatomic) IBOutlet UIView *adView;

-(void)cellBannerView:(UIViewController*)rootVC;

@end
