//
//  ReportViewController.m
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 23(😀).02.17.
//  Copyright © 2017 Likhtarov Dmitry All rights reserved.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "ReportViewController.h"
#import "Preferences.h"

@interface ReportViewController () <UIPrintInteractionControllerDelegate>

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraintImageHigh;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraintImageVerh;

@property (retain, nonatomic) IBOutlet UILabel     *lableZagolovok;
@property (retain, nonatomic) IBOutlet UITextView  *textViewOpisanie;
@property (retain, nonatomic) IBOutlet UIImageView *imageViewDiagram;

@end

@implementation ReportViewController

-(id)init{
    if (self = [super init]) {
        self = [[UIStoryboard storyboardWithName:@"ReportVC" bundle:nil] instantiateViewControllerWithIdentifier:@"ReportVC"];
        //
        // Изменим высоту в соответствии с бумагой
        // Элементы должны регулироваться резинками!
        // Для правильного размера PDF исходим из разрешения 72 точки на дюйм
        // US Letter: 8.5x11 = 1.29411765 // US Legal: 8.5x14 = 1.64705882 // A4: 210x297 = 1.41428571 //
        //
        CGFloat width = 612.0f, height = 1008.0f; //PaperSheetTypeUSLegal8_14
        if ([Preferences sharedPreferences].paperSize == PaperSheetTypeA4) {
            width = 595.2653257f; height = 841.8752464f;
        } else if ([Preferences sharedPreferences].paperSize == PaperSheetTypeUSLegal8_11) {
            width = 612.0f; height = 792.0f;
        }
        self.view.bounds = CGRectMake(0, 0, width, height);
    }
    return self;
}

#pragma mark -
- (void) printPDF
{
    
    NSString * pdfPath = [self pathFromFilePDF];
    NSString * pdfFileName  = [pdfPath lastPathComponent];
    
    NSData *myData = [NSData dataWithContentsOfFile: pdfPath];
	//__block BOOL result = NO;
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController canPrintData: myData] ) {
        
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = pdfFileName;
        printInfo.duplex = UIPrintInfoDuplexNone;
        pic.printInfo = printInfo;
        //pic.showsPageRange = NO; // Если iOS<10 не нужны, то это не надо
        pic.printingItem = myData;
        pic.showsPaperSelectionForLoadedPapers = YES;
		
        [pic presentAnimated:YES completionHandler:^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Filed Print PDF! due to error in domain %@ with error code %ld", error.domain, (long)error.code);
			} else {
				[[Preferences sharedPreferences] removeOneCoin];
                // хотелос бы конечно проверку на фактическую отправку задачки на печать
                
                /*  как я понял, для этого надо полностью свой интефейс печати делать, а так только старт диалога можно отследить, хотя в терминал сообщение выводится:
                 2017-08-19 17:12:26.291579+0300 DesignPatterns[1563:493387] HP\032LJ\032Pro\032P1102w._ipp._tcp.local.: Release-Job successful with warning: successful-ok (successful-ok)
                 но перехватить событие я не нашел как...
                 */

			}
        }];
    }
}
//- (void)printInteractionControllerWillStartJob:(UIPrintInteractionController *)printInteractionController {
//        //
//}
//- (void)printInteractionControllerWillPresentPrinterOptions:(UIPrintInteractionController *)printInteractionController {
//    //
//}
//- (void)printInteractionControllerDidDismissPrinterOptions:(UIPrintInteractionController *)printInteractionController {
//    //
//}
//- (void)printInteractionControllerDidFinishJob:(UIPrintInteractionController *)printInteractionControlle {
//    //
//}

//- (void)printInteractionControllerDidFinishJob:(UIPrintInteractionController *)printInteractionController;

-(NSString *) pathFromFilePDF {

    self.lableZagolovok.text    = self.textZagolovok;
    if (self.textOpisanie) {
        self.textViewOpisanie.text  = self.textOpisanie;
    } else {
        self.textViewOpisanie.attributedText = self.textAttributedOpisanie;
    }
    self.imageViewDiagram.image = self.imageDiagram;
    
    if (self.imageDiagram) {
        // Если изображение слишком низкое или отсутствует, то подвинем текстовый блок кверху
        CGFloat ratio = self.imageViewDiagram.image.size.width / self.imageViewDiagram.image.size.height;
        if (ratio >1) {
            self.constraintImageHigh.constant = self.imageViewDiagram.bounds.size.width / ratio;
        }
    } else { // Когда нет картинки, текст растянем на весь экран до верха
        self.constraintImageHigh.constant = 0;
        self.constraintImageVerh.constant = 0;
    }

    [self.view layoutIfNeeded]; // !!! После всех манипуляций обновим элементы, размеры и резинки


    NSString* fileName = [NSString stringWithFormat:@"DesignPatterns_Report.pdf"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:writableDBPath]) {
        [fileManager removeItemAtPath:writableDBPath error:nil];
    }

    CGContextRef pdfContext = NULL;

    CGRect frame = self.view.bounds;
    
    CFStringRef path = (__bridge CFStringRef)writableDBPath;
    CFURLRef url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, false); // 1
    if (url != NULL) {
        pdfContext = CGPDFContextCreateWithURL (url, &frame, NULL); // 2
        CFRelease(url); // 3
    }
    
    // Надо разбить текст на страницы, желательно не разбивая параграф, но это еще подумать надо
    // array - набор атрибутированых строк, которые влазят во фрейм на печать
    NSArray *array = [self splitTextViewWithAttributedString:self.textViewOpisanie.attributedText toFrame:self.textViewOpisanie.frame];
    
    for (int i=0; i<array.count; i++) {
        if (i == 1 && self.imageDiagram) { // Если печатаем описание с диаграммой // Для остальных страниц не надо печатать диаграму

            CGRect frameTextView = self.textViewOpisanie.frame;
            frameTextView.size.height = frameTextView.size.height + self.constraintImageHigh.constant + self.constraintImageVerh.constant;
            frameTextView.origin.y = frameTextView.origin.y - self.constraintImageHigh.constant - self.constraintImageVerh.constant;
            self.textViewOpisanie.frame = frameTextView;

            self.imageViewDiagram.image = nil;

            self.constraintImageHigh.constant = 0;
            self.constraintImageVerh.constant = 0;
            [self.view layoutIfNeeded]; // !!! После всех манипуляций обновим элементы, размеры и резинки
        }
        self.textViewOpisanie.attributedText = array[i];
        
        // Генерируем страницу
        CGContextBeginPage (pdfContext,nil); // 6
        
        // turn PDF upsidedown
        CGAffineTransform transform = CGAffineTransformIdentity;
        transform = CGAffineTransformMakeTranslation(0, frame.size.height);
        transform = CGAffineTransformScale(transform, 1,-1);
        CGContextConcatCTM(pdfContext, transform);
        
        // Draw view into PDF
        [self.view.layer renderInContext:pdfContext];
        
        CGContextEndPage (pdfContext);// 8
#warning <Error>: replacing +/-infinity with 2147483647
//        Чтобы исправить это, ввести высоту строк в стиле абзаца:
//        paragraphStyle.minimumLineHeight = theFont.pointSize;
//        paragraphStyle.maximumLineHeight = theFont.pointSize;

        
    }

    CGContextRelease (pdfContext);
    
    DLog(@"Выходной файл тут:\n\n%@\n",writableDBPath);
    
    return writableDBPath;
}

-(NSArray*)splitTextViewWithAttributedString:(NSAttributedString*)str1 toFrame:(CGRect)frame {
    
    // В этом модуле разбиваем attributedString на блоки, чтобы каждый из них умещался в этом фреме
    // Возвращаем массив атрибутированых строк
    
    NSMutableArray *arrayAttributedString = [[NSMutableArray alloc] init];
    
    CGSize size = CGSizeMake(frame.size.width, frame.size.height - 20); // "20 - это добавка чтобы не пропустить половинчатую строку"

    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:size];

    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [layoutManager addTextContainer:textContainer];
    [layoutManager glyphRangeForTextContainer:textContainer];

    NSTextStorage *ts = [[NSTextStorage alloc] initWithAttributedString:str1];
    [ts addLayoutManager:layoutManager];
    
    long numberOfGlyphs = [layoutManager numberOfGlyphs];
    
    NSRange range = [layoutManager glyphRangeForTextContainer:textContainer];
    
    long maxIndex = NSMaxRange(range);
    
    NSAttributedString *atrStringCurrent;
    if (range.length>0) {
        // Добавим первый блок
        atrStringCurrent = [str1 attributedSubstringFromRange:range];
        if (atrStringCurrent) {
            [arrayAttributedString addObject:atrStringCurrent];
        }
    }

    while (maxIndex < numberOfGlyphs) {
        if (self.imageDiagram) {// Если печатаем описание с диаграммой // Для остальных страниц не надо печатать диаграму
            size = CGSizeMake(frame.size.width, frame.size.height + self.constraintImageHigh.constant + self.constraintImageVerh.constant - 20);
        }
        
        textContainer = [[NSTextContainer alloc] initWithSize:size];
        
        [layoutManager addTextContainer:textContainer];
        [layoutManager glyphRangeForTextContainer:textContainer];
        
        range = [layoutManager glyphRangeForTextContainer:textContainer];
        maxIndex = NSMaxRange(range);

        if (range.length>0) { // Добавим второй и все следующие блоки
            atrStringCurrent = [str1 attributedSubstringFromRange:range];
            if (atrStringCurrent) {
                [arrayAttributedString addObject:atrStringCurrent];
            }
        }
        
        //DLog(@"\n📕 range на %d pages: = %d %d",cnt, range.location, range.length);
    }

    return arrayAttributedString;
}

#pragma -


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
