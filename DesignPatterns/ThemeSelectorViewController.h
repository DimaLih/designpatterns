//
//  ThemeSelectorViewController.h
//  DesignPatterns
//
//  Created by Водолазкий В.В. on 05.02.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThemeSelectorViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

/**
 *  Reference to parent view controller. Used to simplify navigation
 */
@property (nonatomic, retain) UIViewController *parent;


@property (nonatomic, readwrite) BOOL isViewVisible;

- (void) showOnScreen;
- (void) hideFromScreen;


@end
