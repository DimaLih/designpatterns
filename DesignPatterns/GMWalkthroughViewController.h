//
//  GMWalkthroughViewController.h
//  iWish
//
//  Created by Водолазкий В.В. on 04/10/14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GMWalkthroughViewController : UIViewController


@property (retain, nonatomic) IBOutlet UIImageView *pageView;
@property (retain, nonatomic) IBOutlet UIPageControl *pageIndicator;
@property (retain, nonatomic) IBOutlet UIButton *regButton;
@property (retain, nonatomic) IBOutlet UILabel *pageTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *pageDescription;
@property (nonatomic, retain) IBOutlet UILabel *justRegistered;
@property (nonatomic, retain) IBOutlet UIButton *enterButton;

@property (nonatomic, retain) UINavigationController *nav;



- (IBAction)regButtonPressed:(id)sender;

@end
