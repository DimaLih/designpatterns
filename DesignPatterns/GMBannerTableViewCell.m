//
//  GMBannerTableViewCell.m
//  QRWallet
//
//  Created by Водолазкий В.В. on 09.02.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "GMBannerTableViewCell.h"
#import "AppDelegate.h"
#import "Preferences.h"
#import  <SDVersion.h>

#define OFFSET_Y	5.0

NSString * const GMBannerTableViewCellReusableID	=	@"GMBannerTableViewCellReusableID";

@interface GMBannerTableViewCell () <GADBannerViewDelegate>

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

// Для своей рекламы
@property (weak, nonatomic) IBOutlet UIView *viewMyAd;
@property (weak, nonatomic) IBOutlet UIImageView *imageMyBanner;
@property (weak, nonatomic) IBOutlet UIButton *btMyBanner;

@end


@implementation GMBannerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (GADAdSize) adSize
{
//    GADAdSize adSize = kGADAdSizeSmartBannerPortrait;
    GADAdSize adSize = kGADAdSizeBanner;
    DeviceVersion dev = [SDiOSVersion deviceVersion];
    if (dev >= iPad1 && dev < iPodTouch1Gen) {
        adSize = kGADAdSizeFullBanner;
    }
	return adSize;
}

-(void)cellBannerView:(UIViewController*)rootVC {
    
    // ************  Свои баннеры сначала покажем
    NSMutableArray *images = [NSMutableArray new];
    for (long i = 10; i < 19; i++) {
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"GMADS_%ld", i]];
        if (img) {
            [images addObject:img];
        }
    }
    // Анимируем свой баннер
    self.imageMyBanner.animationImages = images;
#ifdef DEBUG
    self.imageMyBanner.animationDuration = images.count * 0.5; // 0.5 секунд на статическую картинку
#else
    self.imageMyBanner.animationDuration = images.count * 10;
#endif
    [self.imageMyBanner startAnimating];
    // ************

    
    
	//self.bannerView = [[GADBannerView alloc] initWithAdSize:[GMBannerTableViewCell adSize]];
    self.bannerView.adSize = [GMBannerTableViewCell adSize];
	self.bannerView.adUnitID = GOOGLEAD_BANNER_ID;
	self.bannerView.delegate = self;
	//self.bannerView.translatesAutoresizingMaskIntoConstraints = NO;
	self.bannerView.rootViewController = rootVC;
	//[self.adView addSubview:self.bannerView];
	//[self setupBannerConstraints];
    self.bannerView.hidden = YES;
	[self.bannerView loadRequest:[GADRequest request]];
}


- (IBAction)btMyBannerPressed:(id)sender
{
    // По нажатии на свой баннер открыть эплмагаз
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.apple.com/us/developer/geomatix-laboratory-s-r-o/id1013472217"] options:@{} completionHandler:nil];
}


//- (void)setupBannerConstraints
//{
//    // canter ad banner in the cell
//    [self.adView addConstraint:[NSLayoutConstraint constraintWithItem:self.bannerView
//            attribute:NSLayoutAttributeCenterX
//            relatedBy:NSLayoutRelationEqual
//             toItem:self.adView
//            attribute:NSLayoutAttributeCenterX
//             multiplier:1.0
//              constant:0.0]];
//    [self.adView addConstraint:[NSLayoutConstraint constraintWithItem:self.bannerView
//        attribute:NSLayoutAttributeCenterY
//        relatedBy:NSLayoutRelationEqual
//        toItem:self.adView
//        attribute:NSLayoutAttributeCenterY
//        multiplier:1.0
//        constant:0.0]];
//}

#pragma mark - методы делегата рекламы

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    // Поступила реклама от гугла, покажем ее а свою скроем
    //DLog(@"🦀🦀🦀🦀🦀🦀🦀");
    self.bannerView.hidden = NO;
    self.viewMyAd.hidden = YES;
}


- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    // рекламы нет, покажем свой баннер
    //DLog(@"🔥🔥🔥🔥🔥🔥🔥");
    self.bannerView.hidden = YES;
    self.viewMyAd.hidden = NO;
}

#pragma mark -

+ (CGFloat) cellHeight
{
	NSInteger adSize = [GMBannerTableViewCell adSize].size.height;
	return  adSize + OFFSET_Y * 2.0;
}

+ (UINib *) cellNib
{
	return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}



@end
