//
//  SetupTVC.h
//  DesignPatterns
//
//  Created by Dmitry Likhtarov on 24.09.2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetupTVC : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWith;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (readwrite) BOOL cellLanguageExpanded;

@end

NS_ASSUME_NONNULL_END
