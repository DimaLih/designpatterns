//
//  main.m
//  ProxyDemo
//
//  Created by Водолазкий В.В. on 11.03.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O.
//  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject

@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *title;

- (instancetype) initWithAuthor:(NSString *)aAuthor andTitle:(NSString *)aTitle;

@end

@implementation Book

- (instancetype) initWithAuthor:(NSString *)aAuthor andTitle:(NSString *)aTitle
{
	if (self = [super init]) {
		self.author = aAuthor;
		self.title = aTitle;
	}
	return self;
}

- (NSString *) description {
	return [NSString stringWithFormat:@"Book: author - %@, title - %@",
			self.author, self.title];
}

//
// Used by removeBook method...
//
- (BOOL) isEqual:(id)other {
	if (other == self)
		return YES;
	if (![super isEqual:other])
		return NO;
	if ([other isKindOfClass:[Book class]]) {
		Book *o = other;
		return [self.author isEqualToString:o.author] &&
		[self.title isEqualToString:o.title];
	}
	return NO;
}

@end

@interface BookList : NSObject {
	NSMutableArray <Book *> *bookList;
}

@property (nonatomic, readonly) NSInteger bookCount;

- (void) addBook:(Book *)aBook;
- (void) removeBook:(Book *)aBook;
- (Book *) getBook:(NSInteger) bookNum;

@end

@implementation BookList

- (instancetype) init {
	if (self = [super init]) {
		bookList = [NSMutableArray new];
	}
	return self;
}

- (NSInteger) bookCount {
	return bookList.count;
}

- (void) addBook:(Book *)aBook {
	[bookList addObject:aBook];
}

- (void) removeBook:(Book *)aBook {
	[bookList removeObject:aBook];
}

- (Book *) getBook:(NSInteger)bookNum {
	if (bookNum >= 0 && bookNum < bookList.count) {
		return bookList[bookNum];
	}
	return nil;
}

@end

@interface ProxyBookList : NSObject {
	BookList *bookList;
}

@property (nonatomic, readonly) NSInteger bookCount;

- (void) addBook:(Book *)aBook;
- (void) removeBook:(Book *)aBook;
- (Book *) getBook:(NSInteger) bookNum;

@end

@implementation ProxyBookList

- (instancetype) init {
	if (self = [super init]) {
		// We do not initialize bookList here!
	}
	return self;
}

- (NSInteger) bookCount {
	if (!bookList) {
		[self makeBookList];
	}
	return bookList.bookCount;
}

- (void) addBook:(Book *)aBook {
	if (!bookList) {
		[self makeBookList];
	}
	[bookList addBook:aBook];
}

- (void) removeBook:(Book *)aBook {
	if (bookList) {
		[bookList removeBook:aBook];
	}
}

- (Book *) getBook:(NSInteger)bookNum {
	if (bookList) {
		return [bookList getBook:bookNum];
	}
	return nil;
}

- (void) makeBookList {
	bookList = [BookList new];
}

@end



int main(int argc, const char * argv[]) {
	@autoreleasepool {
		ProxyBookList *pbl = [[ProxyBookList alloc] init];
		Book *b1 = [[Book alloc] initWithAuthor:@"Terry Pratchet"
									   andTitle:@"Small Gods"];
		[pbl addBook:b1];
		NSLog(@"total books - %ld", (long)pbl.bookCount);
		
		Book *book = [pbl getBook:0];
		NSLog(@"book in proxy - %@",book);
		
		[pbl removeBook:book];
		
		NSLog(@"total books - %ld", (long)pbl.bookCount);
		
		
	}
    return 0;
}
