//
//  main.swift
//  InterpreterSwiftDemo
//
//  Created by Водолазкий В.В. on 07.08.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

import Foundation

/*
	romanNumeral ::= {thousands} {hundreds} {tens} {ones}
	thousands, hundreds, tens, ones ::= nine | four | {five} {one} {one} {one}
	nine ::= "CM" | "XC" | "IX"
	four ::= "CD" | "XL" | "IV"
	five ::= 'D' | 'L' | 'V'
	one  ::= 'M' | 'C' | 'X' | 'I'
*/

//
// Swift cannot handle simple one-byte strings out of the box.
//
extension String{
	func sub(start: Int, length: Int) -> String {
		assert(start >= 0, "Cannot extract from a negative starting index")
		assert(length >= 0, "Cannot extract a negative length string")
		assert(start <= self.utf8.count - 1, "cannot start beyond the end")
		assert(start + length <= self.utf8.count, "substring goes past the end of the original")
		
		let nsrange = NSRange(location: start, length: length)
		if let r = Range(nsrange) {
			let start = self.utf8.index(self.utf8.startIndex, offsetBy: r.lowerBound)
			let end = self.utf8.index(self.utf8.startIndex, offsetBy: r.upperBound)
			let substringRange = start..<end
			let v = self.utf8[substringRange]
			let outString =  String.init(v)
			return outString!
		}
		return String.init()
	}
}


class RNInterpeter {
	
	var baseUnit : Int = 0
	
	init(base : Int) {
		baseUnit = base
	}
	
	func process(input: inout String, total : inout Int)  {
		var index = 2	// amount of chars consumed by first didgit
		if input.hasPrefix(self.Nine()) {
			total += 9 * self.multiplier()
		} else if input.hasPrefix(self.Four())  {
			total += 4 * multiplier()
		} else {
			if input.hasPrefix(self.Five()) {
				total += 5 * multiplier()
				index = 1
			} else {
				index = 0
			}
		}
		let oneStart = index		// Up to three "One" digits in tetrade
		var end = oneStart + 3
		if end >= input.utf8.count {
			end = input.utf8.count-1
		}
		if oneStart > end {
			// string is over
			input = String.init()
			return
		}
		for i in oneStart ... end {
			let symbol = input.sub(start: i, length: 1)
			if (symbol == self.One()) {
				// consume one char
				total += multiplier()
				index += 1	// modern language does not support ++
			} else {
				// no proper "digit" was found. stop scan
				input = input.sub(start: index, length: input.utf8.count - index)
				return;
			}
		}
		// max amount of possible "One"s is processed
		input = input.sub(start: index, length: input.utf8.count - index)
	}
	
	// abstract methods to be overwritten in descendants
	func multiplier()->Int {
		return 0;
	}
	func One()->String {
		return "one"
	}
	func Four()->String {
		return "four"
	}
	func Five()->String {
		return "five"
	}
	func Nine()->String {
		return "nine"
	}
}

class LastNumber : RNInterpeter {
	convenience init() {
		self.init(base: 1)
	}
	override func multiplier() -> Int {
		return 1
	}
	override func One() -> String {
		return "I"
	}
	override func Four() -> String {
		return "IV"
	}
	override func Five() -> String {
		return "V"
	}
	override func Nine() -> String {
		return "IX"
	}
}

class Ten : RNInterpeter {
	convenience init() {
		self.init(base: 10)
	}
	override func multiplier() -> Int {
		return 10
	}
	override func One() -> String {
		return "X"
	}
	override func Four() -> String {
		return "XL"
	}
	override func Five() -> String {
		return "L"
	}
	override func Nine() -> String {
		return "XC"
	}
}

class Hundred : RNInterpeter {
	convenience init() {
		self.init(base: 100)
	}
	override func multiplier() -> Int {
		return 100
	}
	override func One() -> String {
		return "C"
	}
	override func Four() -> String {
		return "CD"
	}
	override func Five() -> String {
		return "D"
	}
	override func Nine() -> String {
		return "CM"
	}
}

class Thousand : RNInterpeter {
	convenience init() {
		self.init(base: 1000)
	}
	override func multiplier() -> Int {
		return 1000
	}
	override func One() -> String {
		return "M"
	}
	override func Four() -> String {
		return ""
	}
	override func Five() -> String {
		return ""
	}
	override func Nine() -> String {
		return ""
	}
}


class RomanConverter {
	let thousands = Thousand.init()
	let hundreds = Hundred.init()
	let tens = Ten.init()
	let low = LastNumber.init()

	// Main function - interpret string containing Roman number  as an integer
	func interpret(roman: String) -> Int {
		var total : Int = 0
		var romanString = roman
		thousands.process(input: &romanString, total : &total)
		hundreds.process(input: &romanString, total : &total)
		tens.process(input: &romanString, total : &total)
		low.process(input: &romanString, total : &total)
		return total
	}
}

let interpreter = RomanConverter.init()

var result = interpreter.interpret(roman: "CMXCIX")

print(result)



